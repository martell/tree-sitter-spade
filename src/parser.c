#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 446
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 172
#define ALIAS_COUNT 0
#define TOKEN_COUNT 71
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 5
#define MAX_ALIAS_SEQUENCE_LENGTH 10
#define PRODUCTION_ID_COUNT 5

enum {
  anon_sym_POUND = 1,
  anon_sym_LBRACK = 2,
  anon_sym_RBRACK = 3,
  anon_sym_fn = 4,
  anon_sym_entity = 5,
  anon_sym_DASH_GT = 6,
  anon_sym_pipeline = 7,
  anon_sym_LPAREN = 8,
  anon_sym_RPAREN = 9,
  anon_sym_struct = 10,
  anon_sym_port = 11,
  anon_sym_enum = 12,
  anon_sym_impl = 13,
  anon_sym_LBRACE = 14,
  anon_sym_RBRACE = 15,
  anon_sym_COMMA = 16,
  anon_sym_use = 17,
  anon_sym_SEMI = 18,
  anon_sym_DOLLARconfig = 19,
  anon_sym_EQ = 20,
  anon_sym_let = 21,
  anon_sym_COLON = 22,
  anon_sym_reg = 23,
  anon_sym_set = 24,
  anon_sym_decl = 25,
  anon_sym_STAR = 26,
  anon_sym_SQUOTE = 27,
  anon_sym_DOLLARif = 28,
  anon_sym_DOLLARelse = 29,
  anon_sym_assert = 30,
  anon_sym_reset = 31,
  anon_sym_PLUS = 32,
  anon_sym_DASH = 33,
  sym_op_equals = 34,
  anon_sym_LT = 35,
  anon_sym_GT = 36,
  sym_op_le = 37,
  sym_op_ge = 38,
  sym_op_lshift = 39,
  sym_op_rshift = 40,
  anon_sym_AMP = 41,
  sym_op_bitwise_xor = 42,
  sym_op_bitwise_or = 43,
  sym_op_logical_and = 44,
  sym_op_logical_or = 45,
  anon_sym_BQUOTE = 46,
  anon_sym_BANG = 47,
  anon_sym_if = 48,
  anon_sym_else = 49,
  anon_sym_match = 50,
  anon_sym_EQ_GT = 51,
  anon_sym_DOT = 52,
  anon_sym_inst = 53,
  anon_sym_stage = 54,
  anon_sym_RPAREN_DOT = 55,
  anon_sym_true = 56,
  anon_sym_false = 57,
  anon_sym___builtin__ = 58,
  anon_sym_DOLLAR_LPAREN = 59,
  sym_self = 60,
  anon_sym_mut = 61,
  anon_sym_int = 62,
  anon_sym_bool = 63,
  anon_sym_clock = 64,
  sym_identifier = 65,
  anon_sym_COLON_COLON = 66,
  aux_sym_int_literal_token1 = 67,
  aux_sym_int_literal_token2 = 68,
  aux_sym_int_literal_token3 = 69,
  sym_line_comment = 70,
  sym_source_file = 71,
  sym_attribute = 72,
  sym__item = 73,
  sym_unit_definition = 74,
  sym__pipeline_start = 75,
  sym_struct_definition = 76,
  sym_enum_definition = 77,
  sym_impl = 78,
  sym_enum_body = 79,
  sym_enum_member = 80,
  sym_use = 81,
  sym_comptime_config = 82,
  sym__body_or_builtin = 83,
  sym_block = 84,
  sym__statement = 85,
  sym_let_binding = 86,
  sym_reg_statement = 87,
  sym_set_statement = 88,
  sym_decl_statement = 89,
  sym_pipeline_reg_marker = 90,
  sym_pipeline_stage_name = 91,
  sym__comptime_operator = 92,
  sym_comptime_if = 93,
  sym_comptime_else = 94,
  sym_assert_statement = 95,
  sym_reg_reset = 96,
  sym__expression = 97,
  sym_op_add = 98,
  sym_op_sub = 99,
  sym_op_mul = 100,
  sym_op_lt = 101,
  sym_op_gt = 102,
  sym_op_bitwise_and = 103,
  sym__op_add_like = 104,
  sym__op_relational = 105,
  sym__op_shifty = 106,
  sym_op_custom_infix = 107,
  sym_binary_expression = 108,
  sym_unary_expression = 109,
  sym_if_expression = 110,
  sym_match_expression = 111,
  sym_match_block = 112,
  sym_match_arm = 113,
  sym_last_match_arm = 114,
  sym__base_expression = 115,
  sym__simple_base_expression = 116,
  sym_field_access = 117,
  sym_method_call = 118,
  sym_array_literal = 119,
  sym_tuple_literal = 120,
  sym_paren_expression = 121,
  sym_function_call = 122,
  sym_entity_instance = 123,
  sym_pipeline_instance = 124,
  sym_stage_reference = 125,
  sym_bool_literal = 126,
  sym_builtin_marker = 127,
  sym_argument_list = 128,
  sym__named_argument_list = 129,
  sym__named_argument = 130,
  sym__positional_argument_list = 131,
  sym_parameter_list = 132,
  sym_braced_parameter_list = 133,
  sym_typed_parameter = 134,
  sym_parameter = 135,
  sym__pattern = 136,
  sym_tuple_pattern = 137,
  sym_named_unpack = 138,
  sym_positional_unpack = 139,
  sym__named_pattern_list = 140,
  sym__positional_pattern_list = 141,
  sym_named_pattern_param = 142,
  sym_type = 143,
  sym_tuple_type = 144,
  sym_array_type = 145,
  sym_mut_wire = 146,
  sym_wire = 147,
  sym__generic_list = 148,
  sym_generic_parameters = 149,
  sym__generic_param = 150,
  sym__base_type = 151,
  sym_builtin_type = 152,
  sym_scoped_identifier = 153,
  sym__path = 154,
  sym__scoped_or_raw_ident = 155,
  sym_int_literal = 156,
  aux_sym_source_file_repeat1 = 157,
  aux_sym__item_repeat1 = 158,
  aux_sym_impl_repeat1 = 159,
  aux_sym_enum_body_repeat1 = 160,
  aux_sym_block_repeat1 = 161,
  aux_sym_decl_statement_repeat1 = 162,
  aux_sym_match_block_repeat1 = 163,
  aux_sym_array_literal_repeat1 = 164,
  aux_sym__named_argument_list_repeat1 = 165,
  aux_sym__positional_argument_list_repeat1 = 166,
  aux_sym_parameter_list_repeat1 = 167,
  aux_sym_tuple_pattern_repeat1 = 168,
  aux_sym__named_pattern_list_repeat1 = 169,
  aux_sym_tuple_type_repeat1 = 170,
  aux_sym_generic_parameters_repeat1 = 171,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_POUND] = "#",
  [anon_sym_LBRACK] = "[",
  [anon_sym_RBRACK] = "]",
  [anon_sym_fn] = "fn",
  [anon_sym_entity] = "entity",
  [anon_sym_DASH_GT] = "->",
  [anon_sym_pipeline] = "pipeline",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_struct] = "struct",
  [anon_sym_port] = "port",
  [anon_sym_enum] = "enum",
  [anon_sym_impl] = "impl",
  [anon_sym_LBRACE] = "{",
  [anon_sym_RBRACE] = "}",
  [anon_sym_COMMA] = ",",
  [anon_sym_use] = "use",
  [anon_sym_SEMI] = ";",
  [anon_sym_DOLLARconfig] = "$config",
  [anon_sym_EQ] = "=",
  [anon_sym_let] = "let",
  [anon_sym_COLON] = ":",
  [anon_sym_reg] = "reg",
  [anon_sym_set] = "set",
  [anon_sym_decl] = "decl",
  [anon_sym_STAR] = "*",
  [anon_sym_SQUOTE] = "'",
  [anon_sym_DOLLARif] = "$if",
  [anon_sym_DOLLARelse] = "$else",
  [anon_sym_assert] = "assert",
  [anon_sym_reset] = "reset",
  [anon_sym_PLUS] = "+",
  [anon_sym_DASH] = "-",
  [sym_op_equals] = "op_equals",
  [anon_sym_LT] = "<",
  [anon_sym_GT] = ">",
  [sym_op_le] = "op_le",
  [sym_op_ge] = "op_ge",
  [sym_op_lshift] = "op_lshift",
  [sym_op_rshift] = "op_rshift",
  [anon_sym_AMP] = "&",
  [sym_op_bitwise_xor] = "op_bitwise_xor",
  [sym_op_bitwise_or] = "op_bitwise_or",
  [sym_op_logical_and] = "op_logical_and",
  [sym_op_logical_or] = "op_logical_or",
  [anon_sym_BQUOTE] = "`",
  [anon_sym_BANG] = "!",
  [anon_sym_if] = "if",
  [anon_sym_else] = "else",
  [anon_sym_match] = "match",
  [anon_sym_EQ_GT] = "=>",
  [anon_sym_DOT] = ".",
  [anon_sym_inst] = "inst",
  [anon_sym_stage] = "stage",
  [anon_sym_RPAREN_DOT] = ").",
  [anon_sym_true] = "true",
  [anon_sym_false] = "false",
  [anon_sym___builtin__] = "__builtin__",
  [anon_sym_DOLLAR_LPAREN] = "$(",
  [sym_self] = "self",
  [anon_sym_mut] = "mut",
  [anon_sym_int] = "int",
  [anon_sym_bool] = "bool",
  [anon_sym_clock] = "clock",
  [sym_identifier] = "identifier",
  [anon_sym_COLON_COLON] = "::",
  [aux_sym_int_literal_token1] = "int_literal_token1",
  [aux_sym_int_literal_token2] = "int_literal_token2",
  [aux_sym_int_literal_token3] = "int_literal_token3",
  [sym_line_comment] = "line_comment",
  [sym_source_file] = "source_file",
  [sym_attribute] = "attribute",
  [sym__item] = "_item",
  [sym_unit_definition] = "unit_definition",
  [sym__pipeline_start] = "_pipeline_start",
  [sym_struct_definition] = "struct_definition",
  [sym_enum_definition] = "enum_definition",
  [sym_impl] = "impl",
  [sym_enum_body] = "enum_body",
  [sym_enum_member] = "enum_member",
  [sym_use] = "use",
  [sym_comptime_config] = "comptime_config",
  [sym__body_or_builtin] = "_body_or_builtin",
  [sym_block] = "block",
  [sym__statement] = "_statement",
  [sym_let_binding] = "let_binding",
  [sym_reg_statement] = "reg_statement",
  [sym_set_statement] = "set_statement",
  [sym_decl_statement] = "decl_statement",
  [sym_pipeline_reg_marker] = "pipeline_reg_marker",
  [sym_pipeline_stage_name] = "pipeline_stage_name",
  [sym__comptime_operator] = "_comptime_operator",
  [sym_comptime_if] = "comptime_if",
  [sym_comptime_else] = "comptime_else",
  [sym_assert_statement] = "assert_statement",
  [sym_reg_reset] = "reg_reset",
  [sym__expression] = "_expression",
  [sym_op_add] = "op_add",
  [sym_op_sub] = "op_sub",
  [sym_op_mul] = "op_mul",
  [sym_op_lt] = "op_lt",
  [sym_op_gt] = "op_gt",
  [sym_op_bitwise_and] = "op_bitwise_and",
  [sym__op_add_like] = "_op_add_like",
  [sym__op_relational] = "_op_relational",
  [sym__op_shifty] = "_op_shifty",
  [sym_op_custom_infix] = "op_custom_infix",
  [sym_binary_expression] = "binary_expression",
  [sym_unary_expression] = "unary_expression",
  [sym_if_expression] = "if_expression",
  [sym_match_expression] = "match_expression",
  [sym_match_block] = "match_block",
  [sym_match_arm] = "match_arm",
  [sym_last_match_arm] = "match_arm",
  [sym__base_expression] = "_base_expression",
  [sym__simple_base_expression] = "_simple_base_expression",
  [sym_field_access] = "field_access",
  [sym_method_call] = "method_call",
  [sym_array_literal] = "array_literal",
  [sym_tuple_literal] = "tuple_literal",
  [sym_paren_expression] = "paren_expression",
  [sym_function_call] = "function_call",
  [sym_entity_instance] = "entity_instance",
  [sym_pipeline_instance] = "pipeline_instance",
  [sym_stage_reference] = "stage_reference",
  [sym_bool_literal] = "bool_literal",
  [sym_builtin_marker] = "builtin_marker",
  [sym_argument_list] = "argument_list",
  [sym__named_argument_list] = "_named_argument_list",
  [sym__named_argument] = "_named_argument",
  [sym__positional_argument_list] = "_positional_argument_list",
  [sym_parameter_list] = "parameter_list",
  [sym_braced_parameter_list] = "braced_parameter_list",
  [sym_typed_parameter] = "typed_parameter",
  [sym_parameter] = "parameter",
  [sym__pattern] = "_pattern",
  [sym_tuple_pattern] = "tuple_pattern",
  [sym_named_unpack] = "named_unpack",
  [sym_positional_unpack] = "positional_unpack",
  [sym__named_pattern_list] = "_named_pattern_list",
  [sym__positional_pattern_list] = "_positional_pattern_list",
  [sym_named_pattern_param] = "named_pattern_param",
  [sym_type] = "type",
  [sym_tuple_type] = "tuple_type",
  [sym_array_type] = "array_type",
  [sym_mut_wire] = "mut_wire",
  [sym_wire] = "wire",
  [sym__generic_list] = "_generic_list",
  [sym_generic_parameters] = "generic_parameters",
  [sym__generic_param] = "_generic_param",
  [sym__base_type] = "_base_type",
  [sym_builtin_type] = "builtin_type",
  [sym_scoped_identifier] = "scoped_identifier",
  [sym__path] = "_path",
  [sym__scoped_or_raw_ident] = "_scoped_or_raw_ident",
  [sym_int_literal] = "int_literal",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym__item_repeat1] = "_item_repeat1",
  [aux_sym_impl_repeat1] = "impl_repeat1",
  [aux_sym_enum_body_repeat1] = "enum_body_repeat1",
  [aux_sym_block_repeat1] = "block_repeat1",
  [aux_sym_decl_statement_repeat1] = "decl_statement_repeat1",
  [aux_sym_match_block_repeat1] = "match_block_repeat1",
  [aux_sym_array_literal_repeat1] = "array_literal_repeat1",
  [aux_sym__named_argument_list_repeat1] = "_named_argument_list_repeat1",
  [aux_sym__positional_argument_list_repeat1] = "_positional_argument_list_repeat1",
  [aux_sym_parameter_list_repeat1] = "parameter_list_repeat1",
  [aux_sym_tuple_pattern_repeat1] = "tuple_pattern_repeat1",
  [aux_sym__named_pattern_list_repeat1] = "_named_pattern_list_repeat1",
  [aux_sym_tuple_type_repeat1] = "tuple_type_repeat1",
  [aux_sym_generic_parameters_repeat1] = "generic_parameters_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_POUND] = anon_sym_POUND,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_fn] = anon_sym_fn,
  [anon_sym_entity] = anon_sym_entity,
  [anon_sym_DASH_GT] = anon_sym_DASH_GT,
  [anon_sym_pipeline] = anon_sym_pipeline,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_struct] = anon_sym_struct,
  [anon_sym_port] = anon_sym_port,
  [anon_sym_enum] = anon_sym_enum,
  [anon_sym_impl] = anon_sym_impl,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [anon_sym_use] = anon_sym_use,
  [anon_sym_SEMI] = anon_sym_SEMI,
  [anon_sym_DOLLARconfig] = anon_sym_DOLLARconfig,
  [anon_sym_EQ] = anon_sym_EQ,
  [anon_sym_let] = anon_sym_let,
  [anon_sym_COLON] = anon_sym_COLON,
  [anon_sym_reg] = anon_sym_reg,
  [anon_sym_set] = anon_sym_set,
  [anon_sym_decl] = anon_sym_decl,
  [anon_sym_STAR] = anon_sym_STAR,
  [anon_sym_SQUOTE] = anon_sym_SQUOTE,
  [anon_sym_DOLLARif] = anon_sym_DOLLARif,
  [anon_sym_DOLLARelse] = anon_sym_DOLLARelse,
  [anon_sym_assert] = anon_sym_assert,
  [anon_sym_reset] = anon_sym_reset,
  [anon_sym_PLUS] = anon_sym_PLUS,
  [anon_sym_DASH] = anon_sym_DASH,
  [sym_op_equals] = sym_op_equals,
  [anon_sym_LT] = anon_sym_LT,
  [anon_sym_GT] = anon_sym_GT,
  [sym_op_le] = sym_op_le,
  [sym_op_ge] = sym_op_ge,
  [sym_op_lshift] = sym_op_lshift,
  [sym_op_rshift] = sym_op_rshift,
  [anon_sym_AMP] = anon_sym_AMP,
  [sym_op_bitwise_xor] = sym_op_bitwise_xor,
  [sym_op_bitwise_or] = sym_op_bitwise_or,
  [sym_op_logical_and] = sym_op_logical_and,
  [sym_op_logical_or] = sym_op_logical_or,
  [anon_sym_BQUOTE] = anon_sym_BQUOTE,
  [anon_sym_BANG] = anon_sym_BANG,
  [anon_sym_if] = anon_sym_if,
  [anon_sym_else] = anon_sym_else,
  [anon_sym_match] = anon_sym_match,
  [anon_sym_EQ_GT] = anon_sym_EQ_GT,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_inst] = anon_sym_inst,
  [anon_sym_stage] = anon_sym_stage,
  [anon_sym_RPAREN_DOT] = anon_sym_RPAREN_DOT,
  [anon_sym_true] = anon_sym_true,
  [anon_sym_false] = anon_sym_false,
  [anon_sym___builtin__] = anon_sym___builtin__,
  [anon_sym_DOLLAR_LPAREN] = anon_sym_DOLLAR_LPAREN,
  [sym_self] = sym_self,
  [anon_sym_mut] = anon_sym_mut,
  [anon_sym_int] = anon_sym_int,
  [anon_sym_bool] = anon_sym_bool,
  [anon_sym_clock] = anon_sym_clock,
  [sym_identifier] = sym_identifier,
  [anon_sym_COLON_COLON] = anon_sym_COLON_COLON,
  [aux_sym_int_literal_token1] = aux_sym_int_literal_token1,
  [aux_sym_int_literal_token2] = aux_sym_int_literal_token2,
  [aux_sym_int_literal_token3] = aux_sym_int_literal_token3,
  [sym_line_comment] = sym_line_comment,
  [sym_source_file] = sym_source_file,
  [sym_attribute] = sym_attribute,
  [sym__item] = sym__item,
  [sym_unit_definition] = sym_unit_definition,
  [sym__pipeline_start] = sym__pipeline_start,
  [sym_struct_definition] = sym_struct_definition,
  [sym_enum_definition] = sym_enum_definition,
  [sym_impl] = sym_impl,
  [sym_enum_body] = sym_enum_body,
  [sym_enum_member] = sym_enum_member,
  [sym_use] = sym_use,
  [sym_comptime_config] = sym_comptime_config,
  [sym__body_or_builtin] = sym__body_or_builtin,
  [sym_block] = sym_block,
  [sym__statement] = sym__statement,
  [sym_let_binding] = sym_let_binding,
  [sym_reg_statement] = sym_reg_statement,
  [sym_set_statement] = sym_set_statement,
  [sym_decl_statement] = sym_decl_statement,
  [sym_pipeline_reg_marker] = sym_pipeline_reg_marker,
  [sym_pipeline_stage_name] = sym_pipeline_stage_name,
  [sym__comptime_operator] = sym__comptime_operator,
  [sym_comptime_if] = sym_comptime_if,
  [sym_comptime_else] = sym_comptime_else,
  [sym_assert_statement] = sym_assert_statement,
  [sym_reg_reset] = sym_reg_reset,
  [sym__expression] = sym__expression,
  [sym_op_add] = sym_op_add,
  [sym_op_sub] = sym_op_sub,
  [sym_op_mul] = sym_op_mul,
  [sym_op_lt] = sym_op_lt,
  [sym_op_gt] = sym_op_gt,
  [sym_op_bitwise_and] = sym_op_bitwise_and,
  [sym__op_add_like] = sym__op_add_like,
  [sym__op_relational] = sym__op_relational,
  [sym__op_shifty] = sym__op_shifty,
  [sym_op_custom_infix] = sym_op_custom_infix,
  [sym_binary_expression] = sym_binary_expression,
  [sym_unary_expression] = sym_unary_expression,
  [sym_if_expression] = sym_if_expression,
  [sym_match_expression] = sym_match_expression,
  [sym_match_block] = sym_match_block,
  [sym_match_arm] = sym_match_arm,
  [sym_last_match_arm] = sym_match_arm,
  [sym__base_expression] = sym__base_expression,
  [sym__simple_base_expression] = sym__simple_base_expression,
  [sym_field_access] = sym_field_access,
  [sym_method_call] = sym_method_call,
  [sym_array_literal] = sym_array_literal,
  [sym_tuple_literal] = sym_tuple_literal,
  [sym_paren_expression] = sym_paren_expression,
  [sym_function_call] = sym_function_call,
  [sym_entity_instance] = sym_entity_instance,
  [sym_pipeline_instance] = sym_pipeline_instance,
  [sym_stage_reference] = sym_stage_reference,
  [sym_bool_literal] = sym_bool_literal,
  [sym_builtin_marker] = sym_builtin_marker,
  [sym_argument_list] = sym_argument_list,
  [sym__named_argument_list] = sym__named_argument_list,
  [sym__named_argument] = sym__named_argument,
  [sym__positional_argument_list] = sym__positional_argument_list,
  [sym_parameter_list] = sym_parameter_list,
  [sym_braced_parameter_list] = sym_braced_parameter_list,
  [sym_typed_parameter] = sym_typed_parameter,
  [sym_parameter] = sym_parameter,
  [sym__pattern] = sym__pattern,
  [sym_tuple_pattern] = sym_tuple_pattern,
  [sym_named_unpack] = sym_named_unpack,
  [sym_positional_unpack] = sym_positional_unpack,
  [sym__named_pattern_list] = sym__named_pattern_list,
  [sym__positional_pattern_list] = sym__positional_pattern_list,
  [sym_named_pattern_param] = sym_named_pattern_param,
  [sym_type] = sym_type,
  [sym_tuple_type] = sym_tuple_type,
  [sym_array_type] = sym_array_type,
  [sym_mut_wire] = sym_mut_wire,
  [sym_wire] = sym_wire,
  [sym__generic_list] = sym__generic_list,
  [sym_generic_parameters] = sym_generic_parameters,
  [sym__generic_param] = sym__generic_param,
  [sym__base_type] = sym__base_type,
  [sym_builtin_type] = sym_builtin_type,
  [sym_scoped_identifier] = sym_scoped_identifier,
  [sym__path] = sym__path,
  [sym__scoped_or_raw_ident] = sym__scoped_or_raw_ident,
  [sym_int_literal] = sym_int_literal,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym__item_repeat1] = aux_sym__item_repeat1,
  [aux_sym_impl_repeat1] = aux_sym_impl_repeat1,
  [aux_sym_enum_body_repeat1] = aux_sym_enum_body_repeat1,
  [aux_sym_block_repeat1] = aux_sym_block_repeat1,
  [aux_sym_decl_statement_repeat1] = aux_sym_decl_statement_repeat1,
  [aux_sym_match_block_repeat1] = aux_sym_match_block_repeat1,
  [aux_sym_array_literal_repeat1] = aux_sym_array_literal_repeat1,
  [aux_sym__named_argument_list_repeat1] = aux_sym__named_argument_list_repeat1,
  [aux_sym__positional_argument_list_repeat1] = aux_sym__positional_argument_list_repeat1,
  [aux_sym_parameter_list_repeat1] = aux_sym_parameter_list_repeat1,
  [aux_sym_tuple_pattern_repeat1] = aux_sym_tuple_pattern_repeat1,
  [aux_sym__named_pattern_list_repeat1] = aux_sym__named_pattern_list_repeat1,
  [aux_sym_tuple_type_repeat1] = aux_sym_tuple_type_repeat1,
  [aux_sym_generic_parameters_repeat1] = aux_sym_generic_parameters_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_POUND] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_fn] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_entity] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_pipeline] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_struct] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_port] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_enum] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_impl] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_use] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SEMI] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOLLARconfig] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_let] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_reg] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_set] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_decl] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_STAR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SQUOTE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOLLARif] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOLLARelse] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_assert] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_reset] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH] = {
    .visible = true,
    .named = false,
  },
  [sym_op_equals] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [sym_op_le] = {
    .visible = true,
    .named = true,
  },
  [sym_op_ge] = {
    .visible = true,
    .named = true,
  },
  [sym_op_lshift] = {
    .visible = true,
    .named = true,
  },
  [sym_op_rshift] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = false,
  },
  [sym_op_bitwise_xor] = {
    .visible = true,
    .named = true,
  },
  [sym_op_bitwise_or] = {
    .visible = true,
    .named = true,
  },
  [sym_op_logical_and] = {
    .visible = true,
    .named = true,
  },
  [sym_op_logical_or] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_BQUOTE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_if] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_else] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_match] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_inst] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_stage] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_true] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_false] = {
    .visible = true,
    .named = false,
  },
  [anon_sym___builtin__] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOLLAR_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [sym_self] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_mut] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_int] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_bool] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_clock] = {
    .visible = true,
    .named = false,
  },
  [sym_identifier] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_COLON_COLON] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_int_literal_token1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_int_literal_token2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_int_literal_token3] = {
    .visible = false,
    .named = false,
  },
  [sym_line_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym_attribute] = {
    .visible = true,
    .named = true,
  },
  [sym__item] = {
    .visible = false,
    .named = true,
  },
  [sym_unit_definition] = {
    .visible = true,
    .named = true,
  },
  [sym__pipeline_start] = {
    .visible = false,
    .named = true,
  },
  [sym_struct_definition] = {
    .visible = true,
    .named = true,
  },
  [sym_enum_definition] = {
    .visible = true,
    .named = true,
  },
  [sym_impl] = {
    .visible = true,
    .named = true,
  },
  [sym_enum_body] = {
    .visible = true,
    .named = true,
  },
  [sym_enum_member] = {
    .visible = true,
    .named = true,
  },
  [sym_use] = {
    .visible = true,
    .named = true,
  },
  [sym_comptime_config] = {
    .visible = true,
    .named = true,
  },
  [sym__body_or_builtin] = {
    .visible = false,
    .named = true,
  },
  [sym_block] = {
    .visible = true,
    .named = true,
  },
  [sym__statement] = {
    .visible = false,
    .named = true,
  },
  [sym_let_binding] = {
    .visible = true,
    .named = true,
  },
  [sym_reg_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_set_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_decl_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_pipeline_reg_marker] = {
    .visible = true,
    .named = true,
  },
  [sym_pipeline_stage_name] = {
    .visible = true,
    .named = true,
  },
  [sym__comptime_operator] = {
    .visible = false,
    .named = true,
  },
  [sym_comptime_if] = {
    .visible = true,
    .named = true,
  },
  [sym_comptime_else] = {
    .visible = true,
    .named = true,
  },
  [sym_assert_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_reg_reset] = {
    .visible = true,
    .named = true,
  },
  [sym__expression] = {
    .visible = false,
    .named = true,
  },
  [sym_op_add] = {
    .visible = true,
    .named = true,
  },
  [sym_op_sub] = {
    .visible = true,
    .named = true,
  },
  [sym_op_mul] = {
    .visible = true,
    .named = true,
  },
  [sym_op_lt] = {
    .visible = true,
    .named = true,
  },
  [sym_op_gt] = {
    .visible = true,
    .named = true,
  },
  [sym_op_bitwise_and] = {
    .visible = true,
    .named = true,
  },
  [sym__op_add_like] = {
    .visible = false,
    .named = true,
  },
  [sym__op_relational] = {
    .visible = false,
    .named = true,
  },
  [sym__op_shifty] = {
    .visible = false,
    .named = true,
  },
  [sym_op_custom_infix] = {
    .visible = true,
    .named = true,
  },
  [sym_binary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_unary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_if_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_match_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_match_block] = {
    .visible = true,
    .named = true,
  },
  [sym_match_arm] = {
    .visible = true,
    .named = true,
  },
  [sym_last_match_arm] = {
    .visible = true,
    .named = true,
  },
  [sym__base_expression] = {
    .visible = false,
    .named = true,
  },
  [sym__simple_base_expression] = {
    .visible = false,
    .named = true,
  },
  [sym_field_access] = {
    .visible = true,
    .named = true,
  },
  [sym_method_call] = {
    .visible = true,
    .named = true,
  },
  [sym_array_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_tuple_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_paren_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_function_call] = {
    .visible = true,
    .named = true,
  },
  [sym_entity_instance] = {
    .visible = true,
    .named = true,
  },
  [sym_pipeline_instance] = {
    .visible = true,
    .named = true,
  },
  [sym_stage_reference] = {
    .visible = true,
    .named = true,
  },
  [sym_bool_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_builtin_marker] = {
    .visible = true,
    .named = true,
  },
  [sym_argument_list] = {
    .visible = true,
    .named = true,
  },
  [sym__named_argument_list] = {
    .visible = false,
    .named = true,
  },
  [sym__named_argument] = {
    .visible = false,
    .named = true,
  },
  [sym__positional_argument_list] = {
    .visible = false,
    .named = true,
  },
  [sym_parameter_list] = {
    .visible = true,
    .named = true,
  },
  [sym_braced_parameter_list] = {
    .visible = true,
    .named = true,
  },
  [sym_typed_parameter] = {
    .visible = true,
    .named = true,
  },
  [sym_parameter] = {
    .visible = true,
    .named = true,
  },
  [sym__pattern] = {
    .visible = false,
    .named = true,
  },
  [sym_tuple_pattern] = {
    .visible = true,
    .named = true,
  },
  [sym_named_unpack] = {
    .visible = true,
    .named = true,
  },
  [sym_positional_unpack] = {
    .visible = true,
    .named = true,
  },
  [sym__named_pattern_list] = {
    .visible = false,
    .named = true,
  },
  [sym__positional_pattern_list] = {
    .visible = false,
    .named = true,
  },
  [sym_named_pattern_param] = {
    .visible = true,
    .named = true,
  },
  [sym_type] = {
    .visible = true,
    .named = true,
  },
  [sym_tuple_type] = {
    .visible = true,
    .named = true,
  },
  [sym_array_type] = {
    .visible = true,
    .named = true,
  },
  [sym_mut_wire] = {
    .visible = true,
    .named = true,
  },
  [sym_wire] = {
    .visible = true,
    .named = true,
  },
  [sym__generic_list] = {
    .visible = false,
    .named = true,
  },
  [sym_generic_parameters] = {
    .visible = true,
    .named = true,
  },
  [sym__generic_param] = {
    .visible = false,
    .named = true,
  },
  [sym__base_type] = {
    .visible = false,
    .named = true,
  },
  [sym_builtin_type] = {
    .visible = true,
    .named = true,
  },
  [sym_scoped_identifier] = {
    .visible = true,
    .named = true,
  },
  [sym__path] = {
    .visible = false,
    .named = true,
  },
  [sym__scoped_or_raw_ident] = {
    .visible = false,
    .named = true,
  },
  [sym_int_literal] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__item_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_impl_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_enum_body_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_block_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_decl_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_match_block_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_array_literal_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__named_argument_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__positional_argument_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_parameter_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_tuple_pattern_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__named_pattern_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_tuple_type_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_generic_parameters_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_name = 1,
  field_path = 2,
  field_pattern = 3,
  field_stage = 4,
  field_value = 5,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_name] = "name",
  [field_path] = "path",
  [field_pattern] = "pattern",
  [field_stage] = "stage",
  [field_value] = "value",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 1},
  [2] = {.index = 1, .length = 2},
  [3] = {.index = 3, .length = 1},
  [4] = {.index = 4, .length = 2},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_name, 1},
  [1] =
    {field_name, 2},
    {field_path, 0},
  [3] =
    {field_stage, 2},
  [4] =
    {field_pattern, 0},
    {field_value, 2},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 28,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 41,
  [42] = 42,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
  [48] = 48,
  [49] = 49,
  [50] = 50,
  [51] = 51,
  [52] = 52,
  [53] = 53,
  [54] = 54,
  [55] = 55,
  [56] = 56,
  [57] = 57,
  [58] = 58,
  [59] = 59,
  [60] = 60,
  [61] = 61,
  [62] = 62,
  [63] = 63,
  [64] = 64,
  [65] = 65,
  [66] = 66,
  [67] = 67,
  [68] = 68,
  [69] = 69,
  [70] = 70,
  [71] = 71,
  [72] = 72,
  [73] = 73,
  [74] = 74,
  [75] = 75,
  [76] = 76,
  [77] = 77,
  [78] = 78,
  [79] = 79,
  [80] = 80,
  [81] = 81,
  [82] = 82,
  [83] = 83,
  [84] = 84,
  [85] = 85,
  [86] = 86,
  [87] = 87,
  [88] = 88,
  [89] = 89,
  [90] = 90,
  [91] = 91,
  [92] = 92,
  [93] = 93,
  [94] = 94,
  [95] = 95,
  [96] = 96,
  [97] = 97,
  [98] = 98,
  [99] = 99,
  [100] = 100,
  [101] = 101,
  [102] = 102,
  [103] = 103,
  [104] = 104,
  [105] = 105,
  [106] = 106,
  [107] = 107,
  [108] = 108,
  [109] = 109,
  [110] = 110,
  [111] = 111,
  [112] = 112,
  [113] = 113,
  [114] = 114,
  [115] = 115,
  [116] = 116,
  [117] = 117,
  [118] = 118,
  [119] = 119,
  [120] = 120,
  [121] = 121,
  [122] = 122,
  [123] = 123,
  [124] = 124,
  [125] = 125,
  [126] = 126,
  [127] = 127,
  [128] = 128,
  [129] = 129,
  [130] = 130,
  [131] = 131,
  [132] = 132,
  [133] = 133,
  [134] = 131,
  [135] = 135,
  [136] = 136,
  [137] = 137,
  [138] = 44,
  [139] = 139,
  [140] = 140,
  [141] = 141,
  [142] = 142,
  [143] = 143,
  [144] = 144,
  [145] = 139,
  [146] = 146,
  [147] = 147,
  [148] = 148,
  [149] = 149,
  [150] = 150,
  [151] = 151,
  [152] = 141,
  [153] = 153,
  [154] = 154,
  [155] = 155,
  [156] = 142,
  [157] = 155,
  [158] = 158,
  [159] = 159,
  [160] = 147,
  [161] = 144,
  [162] = 162,
  [163] = 163,
  [164] = 164,
  [165] = 165,
  [166] = 166,
  [167] = 167,
  [168] = 168,
  [169] = 169,
  [170] = 170,
  [171] = 171,
  [172] = 172,
  [173] = 173,
  [174] = 174,
  [175] = 175,
  [176] = 176,
  [177] = 177,
  [178] = 178,
  [179] = 179,
  [180] = 180,
  [181] = 181,
  [182] = 182,
  [183] = 183,
  [184] = 184,
  [185] = 185,
  [186] = 186,
  [187] = 187,
  [188] = 188,
  [189] = 189,
  [190] = 190,
  [191] = 191,
  [192] = 192,
  [193] = 193,
  [194] = 194,
  [195] = 195,
  [196] = 196,
  [197] = 70,
  [198] = 198,
  [199] = 199,
  [200] = 200,
  [201] = 201,
  [202] = 202,
  [203] = 203,
  [204] = 204,
  [205] = 205,
  [206] = 206,
  [207] = 78,
  [208] = 208,
  [209] = 209,
  [210] = 210,
  [211] = 211,
  [212] = 212,
  [213] = 213,
  [214] = 214,
  [215] = 215,
  [216] = 216,
  [217] = 217,
  [218] = 218,
  [219] = 219,
  [220] = 220,
  [221] = 221,
  [222] = 222,
  [223] = 223,
  [224] = 87,
  [225] = 225,
  [226] = 92,
  [227] = 223,
  [228] = 89,
  [229] = 88,
  [230] = 230,
  [231] = 231,
  [232] = 91,
  [233] = 90,
  [234] = 234,
  [235] = 235,
  [236] = 236,
  [237] = 237,
  [238] = 238,
  [239] = 239,
  [240] = 240,
  [241] = 241,
  [242] = 242,
  [243] = 243,
  [244] = 244,
  [245] = 245,
  [246] = 246,
  [247] = 247,
  [248] = 248,
  [249] = 249,
  [250] = 250,
  [251] = 251,
  [252] = 252,
  [253] = 253,
  [254] = 254,
  [255] = 255,
  [256] = 256,
  [257] = 257,
  [258] = 258,
  [259] = 259,
  [260] = 260,
  [261] = 261,
  [262] = 262,
  [263] = 263,
  [264] = 264,
  [265] = 265,
  [266] = 266,
  [267] = 267,
  [268] = 268,
  [269] = 269,
  [270] = 270,
  [271] = 271,
  [272] = 272,
  [273] = 273,
  [274] = 274,
  [275] = 274,
  [276] = 276,
  [277] = 277,
  [278] = 278,
  [279] = 279,
  [280] = 280,
  [281] = 281,
  [282] = 282,
  [283] = 283,
  [284] = 284,
  [285] = 285,
  [286] = 286,
  [287] = 287,
  [288] = 288,
  [289] = 43,
  [290] = 290,
  [291] = 291,
  [292] = 292,
  [293] = 293,
  [294] = 294,
  [295] = 295,
  [296] = 296,
  [297] = 297,
  [298] = 189,
  [299] = 299,
  [300] = 300,
  [301] = 301,
  [302] = 302,
  [303] = 303,
  [304] = 304,
  [305] = 305,
  [306] = 306,
  [307] = 307,
  [308] = 308,
  [309] = 309,
  [310] = 310,
  [311] = 311,
  [312] = 312,
  [313] = 313,
  [314] = 314,
  [315] = 315,
  [316] = 316,
  [317] = 317,
  [318] = 318,
  [319] = 319,
  [320] = 320,
  [321] = 321,
  [322] = 313,
  [323] = 323,
  [324] = 324,
  [325] = 325,
  [326] = 326,
  [327] = 327,
  [328] = 328,
  [329] = 329,
  [330] = 330,
  [331] = 331,
  [332] = 332,
  [333] = 333,
  [334] = 334,
  [335] = 335,
  [336] = 336,
  [337] = 337,
  [338] = 338,
  [339] = 339,
  [340] = 340,
  [341] = 341,
  [342] = 342,
  [343] = 343,
  [344] = 344,
  [345] = 345,
  [346] = 346,
  [347] = 347,
  [348] = 348,
  [349] = 349,
  [350] = 350,
  [351] = 351,
  [352] = 352,
  [353] = 353,
  [354] = 354,
  [355] = 355,
  [356] = 356,
  [357] = 357,
  [358] = 358,
  [359] = 359,
  [360] = 360,
  [361] = 361,
  [362] = 362,
  [363] = 363,
  [364] = 364,
  [365] = 365,
  [366] = 366,
  [367] = 367,
  [368] = 368,
  [369] = 369,
  [370] = 370,
  [371] = 371,
  [372] = 222,
  [373] = 373,
  [374] = 374,
  [375] = 375,
  [376] = 376,
  [377] = 377,
  [378] = 378,
  [379] = 379,
  [380] = 380,
  [381] = 381,
  [382] = 382,
  [383] = 383,
  [384] = 384,
  [385] = 385,
  [386] = 386,
  [387] = 387,
  [388] = 388,
  [389] = 389,
  [390] = 390,
  [391] = 391,
  [392] = 392,
  [393] = 393,
  [394] = 394,
  [395] = 395,
  [396] = 396,
  [397] = 397,
  [398] = 398,
  [399] = 399,
  [400] = 400,
  [401] = 401,
  [402] = 402,
  [403] = 403,
  [404] = 404,
  [405] = 405,
  [406] = 406,
  [407] = 407,
  [408] = 408,
  [409] = 409,
  [410] = 410,
  [411] = 411,
  [412] = 412,
  [413] = 413,
  [414] = 414,
  [415] = 415,
  [416] = 416,
  [417] = 417,
  [418] = 418,
  [419] = 419,
  [420] = 420,
  [421] = 421,
  [422] = 422,
  [423] = 423,
  [424] = 424,
  [425] = 425,
  [426] = 426,
  [427] = 427,
  [428] = 428,
  [429] = 429,
  [430] = 430,
  [431] = 412,
  [432] = 386,
  [433] = 390,
  [434] = 434,
  [435] = 435,
  [436] = 436,
  [437] = 437,
  [438] = 438,
  [439] = 427,
  [440] = 417,
  [441] = 394,
  [442] = 405,
  [443] = 443,
  [444] = 444,
  [445] = 391,
};

static inline bool sym_identifier_character_set_1(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 931
            ? (c < 748
              ? (c < 192
                ? (c < 170
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= 'z')
                  : (c <= 170 || (c < 186
                    ? c == 181
                    : c <= 186)))
                : (c <= 214 || (c < 710
                  ? (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)
                  : (c <= 721 || (c >= 736 && c <= 740)))))
              : (c <= 748 || (c < 895
                ? (c < 886
                  ? (c < 880
                    ? c == 750
                    : c <= 884)
                  : (c <= 887 || (c >= 891 && c <= 893)))
                : (c <= 895 || (c < 908
                  ? (c < 904
                    ? c == 902
                    : c <= 906)
                  : (c <= 908 || (c >= 910 && c <= 929)))))))
            : (c <= 1013 || (c < 1649
              ? (c < 1376
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1015 && c <= 1153)
                    : c <= 1327)
                  : (c <= 1366 || c == 1369))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_identifier_character_set_2(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 910
            ? (c < 736
              ? (c < 186
                ? (c < 'a'
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= '_')
                  : (c <= 'z' || (c < 181
                    ? c == 170
                    : c <= 181)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c >= 710 && c <= 721)))))
              : (c <= 740 || (c < 891
                ? (c < 880
                  ? (c < 750
                    ? c == 748
                    : c <= 750)
                  : (c <= 884 || (c >= 886 && c <= 887)))
                : (c <= 893 || (c < 904
                  ? (c < 902
                    ? c == 895
                    : c <= 902)
                  : (c <= 906 || c == 908))))))
            : (c <= 929 || (c < 1649
              ? (c < 1376
                ? (c < 1162
                  ? (c < 1015
                    ? (c >= 931 && c <= 1013)
                    : c <= 1153)
                  : (c <= 1327 || (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_identifier_character_set_3(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'a' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_identifier_character_set_4(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'b' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(117);
      if (lookahead == '!') ADVANCE(179);
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '$') ADVANCE(9);
      if (lookahead == '&') ADVANCE(173);
      if (lookahead == '\'') ADVANCE(152);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '*') ADVANCE(151);
      if (lookahead == '+') ADVANCE(158);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(160);
      if (lookahead == '.') ADVANCE(186);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == ':') ADVANCE(144);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '<') ADVANCE(163);
      if (lookahead == '=') ADVANCE(140);
      if (lookahead == '>') ADVANCE(167);
      if (lookahead == '[') ADVANCE(119);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == '^') ADVANCE(174);
      if (lookahead == '_') ADVANCE(19);
      if (lookahead == '`') ADVANCE(178);
      if (lookahead == 'a') ADVANCE(96);
      if (lookahead == 'b') ADVANCE(81);
      if (lookahead == 'c') ADVANCE(66);
      if (lookahead == 'd') ADVANCE(31);
      if (lookahead == 'e') ADVANCE(69);
      if (lookahead == 'f') ADVANCE(24);
      if (lookahead == 'i') ADVANCE(46);
      if (lookahead == 'l') ADVANCE(32);
      if (lookahead == 'm') ADVANCE(23);
      if (lookahead == 'p') ADVANCE(55);
      if (lookahead == 'r') ADVANCE(33);
      if (lookahead == 's') ADVANCE(34);
      if (lookahead == 't') ADVANCE(85);
      if (lookahead == 'u') ADVANCE(89);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '|') ADVANCE(175);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      END_STATE();
    case 1:
      if (lookahead == '!') ADVANCE(179);
      if (lookahead == '$') ADVANCE(10);
      if (lookahead == '&') ADVANCE(173);
      if (lookahead == '\'') ADVANCE(152);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '*') ADVANCE(151);
      if (lookahead == '+') ADVANCE(158);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(159);
      if (lookahead == '.') ADVANCE(186);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == ':') ADVANCE(16);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '<') ADVANCE(163);
      if (lookahead == '=') ADVANCE(139);
      if (lookahead == '>') ADVANCE(167);
      if (lookahead == '[') ADVANCE(119);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == '^') ADVANCE(174);
      if (lookahead == '`') ADVANCE(178);
      if (lookahead == 'a') ADVANCE(245);
      if (lookahead == 'd') ADVANCE(214);
      if (lookahead == 'f') ADVANCE(208);
      if (lookahead == 'i') ADVANCE(224);
      if (lookahead == 'l') ADVANCE(215);
      if (lookahead == 'm') ADVANCE(209);
      if (lookahead == 'r') ADVANCE(216);
      if (lookahead == 's') ADVANCE(217);
      if (lookahead == 't') ADVANCE(242);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '|') ADVANCE(175);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(1)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(258);
      END_STATE();
    case 2:
      if (lookahead == '!') ADVANCE(179);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '*') ADVANCE(151);
      if (lookahead == '-') ADVANCE(159);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == ':') ADVANCE(16);
      if (lookahead == '<') ADVANCE(164);
      if (lookahead == '=') ADVANCE(17);
      if (lookahead == '>') ADVANCE(166);
      if (lookahead == '[') ADVANCE(119);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == 'f') ADVANCE(208);
      if (lookahead == 'i') ADVANCE(224);
      if (lookahead == 'm') ADVANCE(209);
      if (lookahead == 's') ADVANCE(222);
      if (lookahead == 't') ADVANCE(242);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(2)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 3:
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '+') ADVANCE(158);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(159);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == ':') ADVANCE(16);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '>') ADVANCE(165);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(3)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 4:
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == 's') ADVANCE(223);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(4)
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 5:
      if (lookahead == '&') ADVANCE(172);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == '>') ADVANCE(165);
      if (lookahead == '[') ADVANCE(119);
      if (lookahead == 'b') ADVANCE(238);
      if (lookahead == 'c') ADVANCE(234);
      if (lookahead == 'i') ADVANCE(237);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(5)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 6:
      if (lookahead == '&') ADVANCE(172);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(12);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == '<') ADVANCE(162);
      if (lookahead == '>') ADVANCE(165);
      if (lookahead == '[') ADVANCE(119);
      if (lookahead == 'b') ADVANCE(238);
      if (lookahead == 'c') ADVANCE(234);
      if (lookahead == 'i') ADVANCE(237);
      if (lookahead == 'm') ADVANCE(257);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(6)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 7:
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == '0') ADVANCE(260);
      if (lookahead == ':') ADVANCE(16);
      if (lookahead == '>') ADVANCE(165);
      if (lookahead == 'f') ADVANCE(208);
      if (lookahead == 't') ADVANCE(242);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(7)
      if (('1' <= lookahead && lookahead <= '9')) ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 8:
      if (lookahead == '(') ADVANCE(197);
      if (lookahead == 'c') ADVANCE(79);
      END_STATE();
    case 9:
      if (lookahead == '(') ADVANCE(197);
      if (lookahead == 'c') ADVANCE(79);
      if (lookahead == 'e') ADVANCE(70);
      if (lookahead == 'i') ADVANCE(47);
      END_STATE();
    case 10:
      if (lookahead == '(') ADVANCE(197);
      if (lookahead == 'e') ADVANCE(70);
      if (lookahead == 'i') ADVANCE(47);
      END_STATE();
    case 11:
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(18);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '<') ADVANCE(162);
      if (lookahead == '=') ADVANCE(138);
      if (lookahead == '>') ADVANCE(165);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == '_') ADVANCE(19);
      if (lookahead == 'r') ADVANCE(45);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(11)
      END_STATE();
    case 12:
      if (lookahead == '.') ADVANCE(191);
      END_STATE();
    case 13:
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == 'i') ADVANCE(236);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(13)
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 14:
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == 'p') ADVANCE(241);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(14)
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(258);
      END_STATE();
    case 15:
      if (lookahead == '/') ADVANCE(264);
      END_STATE();
    case 16:
      if (lookahead == ':') ADVANCE(259);
      END_STATE();
    case 17:
      if (lookahead == '=') ADVANCE(161);
      if (lookahead == '>') ADVANCE(185);
      END_STATE();
    case 18:
      if (lookahead == '>') ADVANCE(123);
      END_STATE();
    case 19:
      if (lookahead == '_') ADVANCE(25);
      END_STATE();
    case 20:
      if (lookahead == '_') ADVANCE(196);
      END_STATE();
    case 21:
      if (lookahead == '_') ADVANCE(20);
      END_STATE();
    case 22:
      if (lookahead == 'a') ADVANCE(52);
      if (lookahead == 'r') ADVANCE(110);
      END_STATE();
    case 23:
      if (lookahead == 'a') ADVANCE(106);
      if (lookahead == 'u') ADVANCE(99);
      END_STATE();
    case 24:
      if (lookahead == 'a') ADVANCE(71);
      if (lookahead == 'n') ADVANCE(121);
      END_STATE();
    case 25:
      if (lookahead == 'b') ADVANCE(109);
      END_STATE();
    case 26:
      if (lookahead == 'c') ADVANCE(61);
      END_STATE();
    case 27:
      if (lookahead == 'c') ADVANCE(53);
      END_STATE();
    case 28:
      if (lookahead == 'c') ADVANCE(79);
      END_STATE();
    case 29:
      if (lookahead == 'c') ADVANCE(63);
      END_STATE();
    case 30:
      if (lookahead == 'c') ADVANCE(105);
      END_STATE();
    case 31:
      if (lookahead == 'e') ADVANCE(29);
      END_STATE();
    case 32:
      if (lookahead == 'e') ADVANCE(98);
      END_STATE();
    case 33:
      if (lookahead == 'e') ADVANCE(50);
      END_STATE();
    case 34:
      if (lookahead == 'e') ADVANCE(65);
      if (lookahead == 't') ADVANCE(22);
      END_STATE();
    case 35:
      if (lookahead == 'e') ADVANCE(135);
      END_STATE();
    case 36:
      if (lookahead == 'e') ADVANCE(182);
      END_STATE();
    case 37:
      if (lookahead == 'e') ADVANCE(192);
      END_STATE();
    case 38:
      if (lookahead == 'e') ADVANCE(154);
      END_STATE();
    case 39:
      if (lookahead == 'e') ADVANCE(194);
      END_STATE();
    case 40:
      if (lookahead == 'e') ADVANCE(189);
      END_STATE();
    case 41:
      if (lookahead == 'e') ADVANCE(124);
      END_STATE();
    case 42:
      if (lookahead == 'e') ADVANCE(67);
      END_STATE();
    case 43:
      if (lookahead == 'e') ADVANCE(103);
      END_STATE();
    case 44:
      if (lookahead == 'e') ADVANCE(88);
      END_STATE();
    case 45:
      if (lookahead == 'e') ADVANCE(93);
      END_STATE();
    case 46:
      if (lookahead == 'f') ADVANCE(180);
      if (lookahead == 'm') ADVANCE(83);
      if (lookahead == 'n') ADVANCE(90);
      END_STATE();
    case 47:
      if (lookahead == 'f') ADVANCE(153);
      END_STATE();
    case 48:
      if (lookahead == 'f') ADVANCE(198);
      END_STATE();
    case 49:
      if (lookahead == 'f') ADVANCE(57);
      END_STATE();
    case 50:
      if (lookahead == 'g') ADVANCE(145);
      if (lookahead == 's') ADVANCE(43);
      END_STATE();
    case 51:
      if (lookahead == 'g') ADVANCE(137);
      END_STATE();
    case 52:
      if (lookahead == 'g') ADVANCE(40);
      END_STATE();
    case 53:
      if (lookahead == 'h') ADVANCE(183);
      END_STATE();
    case 54:
      if (lookahead == 'i') ADVANCE(84);
      END_STATE();
    case 55:
      if (lookahead == 'i') ADVANCE(84);
      if (lookahead == 'o') ADVANCE(87);
      END_STATE();
    case 56:
      if (lookahead == 'i') ADVANCE(78);
      END_STATE();
    case 57:
      if (lookahead == 'i') ADVANCE(51);
      END_STATE();
    case 58:
      if (lookahead == 'i') ADVANCE(76);
      END_STATE();
    case 59:
      if (lookahead == 'i') ADVANCE(102);
      END_STATE();
    case 60:
      if (lookahead == 'i') ADVANCE(68);
      END_STATE();
    case 61:
      if (lookahead == 'k') ADVANCE(206);
      END_STATE();
    case 62:
      if (lookahead == 'l') ADVANCE(204);
      END_STATE();
    case 63:
      if (lookahead == 'l') ADVANCE(149);
      END_STATE();
    case 64:
      if (lookahead == 'l') ADVANCE(131);
      END_STATE();
    case 65:
      if (lookahead == 'l') ADVANCE(48);
      if (lookahead == 't') ADVANCE(147);
      END_STATE();
    case 66:
      if (lookahead == 'l') ADVANCE(80);
      END_STATE();
    case 67:
      if (lookahead == 'l') ADVANCE(56);
      END_STATE();
    case 68:
      if (lookahead == 'l') ADVANCE(107);
      END_STATE();
    case 69:
      if (lookahead == 'l') ADVANCE(92);
      if (lookahead == 'n') ADVANCE(97);
      END_STATE();
    case 70:
      if (lookahead == 'l') ADVANCE(94);
      END_STATE();
    case 71:
      if (lookahead == 'l') ADVANCE(95);
      END_STATE();
    case 72:
      if (lookahead == 'm') ADVANCE(83);
      END_STATE();
    case 73:
      if (lookahead == 'm') ADVANCE(130);
      END_STATE();
    case 74:
      if (lookahead == 'n') ADVANCE(97);
      END_STATE();
    case 75:
      if (lookahead == 'n') ADVANCE(121);
      END_STATE();
    case 76:
      if (lookahead == 'n') ADVANCE(21);
      END_STATE();
    case 77:
      if (lookahead == 'n') ADVANCE(49);
      END_STATE();
    case 78:
      if (lookahead == 'n') ADVANCE(41);
      END_STATE();
    case 79:
      if (lookahead == 'o') ADVANCE(77);
      END_STATE();
    case 80:
      if (lookahead == 'o') ADVANCE(26);
      END_STATE();
    case 81:
      if (lookahead == 'o') ADVANCE(82);
      END_STATE();
    case 82:
      if (lookahead == 'o') ADVANCE(62);
      END_STATE();
    case 83:
      if (lookahead == 'p') ADVANCE(64);
      END_STATE();
    case 84:
      if (lookahead == 'p') ADVANCE(42);
      END_STATE();
    case 85:
      if (lookahead == 'r') ADVANCE(111);
      END_STATE();
    case 86:
      if (lookahead == 'r') ADVANCE(110);
      END_STATE();
    case 87:
      if (lookahead == 'r') ADVANCE(101);
      END_STATE();
    case 88:
      if (lookahead == 'r') ADVANCE(104);
      END_STATE();
    case 89:
      if (lookahead == 's') ADVANCE(35);
      END_STATE();
    case 90:
      if (lookahead == 's') ADVANCE(100);
      if (lookahead == 't') ADVANCE(202);
      END_STATE();
    case 91:
      if (lookahead == 's') ADVANCE(44);
      END_STATE();
    case 92:
      if (lookahead == 's') ADVANCE(36);
      END_STATE();
    case 93:
      if (lookahead == 's') ADVANCE(43);
      END_STATE();
    case 94:
      if (lookahead == 's') ADVANCE(38);
      END_STATE();
    case 95:
      if (lookahead == 's') ADVANCE(39);
      END_STATE();
    case 96:
      if (lookahead == 's') ADVANCE(91);
      END_STATE();
    case 97:
      if (lookahead == 't') ADVANCE(59);
      if (lookahead == 'u') ADVANCE(73);
      END_STATE();
    case 98:
      if (lookahead == 't') ADVANCE(141);
      END_STATE();
    case 99:
      if (lookahead == 't') ADVANCE(200);
      END_STATE();
    case 100:
      if (lookahead == 't') ADVANCE(187);
      END_STATE();
    case 101:
      if (lookahead == 't') ADVANCE(128);
      END_STATE();
    case 102:
      if (lookahead == 't') ADVANCE(112);
      END_STATE();
    case 103:
      if (lookahead == 't') ADVANCE(157);
      END_STATE();
    case 104:
      if (lookahead == 't') ADVANCE(155);
      END_STATE();
    case 105:
      if (lookahead == 't') ADVANCE(127);
      END_STATE();
    case 106:
      if (lookahead == 't') ADVANCE(27);
      END_STATE();
    case 107:
      if (lookahead == 't') ADVANCE(58);
      END_STATE();
    case 108:
      if (lookahead == 't') ADVANCE(86);
      END_STATE();
    case 109:
      if (lookahead == 'u') ADVANCE(60);
      END_STATE();
    case 110:
      if (lookahead == 'u') ADVANCE(30);
      END_STATE();
    case 111:
      if (lookahead == 'u') ADVANCE(37);
      END_STATE();
    case 112:
      if (lookahead == 'y') ADVANCE(122);
      END_STATE();
    case 113:
      if (lookahead == '0' ||
          lookahead == '1') ADVANCE(263);
      END_STATE();
    case 114:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(262);
      END_STATE();
    case 115:
      if (eof) ADVANCE(117);
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '$') ADVANCE(8);
      if (lookahead == '&') ADVANCE(173);
      if (lookahead == '(') ADVANCE(125);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '*') ADVANCE(151);
      if (lookahead == '+') ADVANCE(158);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(159);
      if (lookahead == '.') ADVANCE(186);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == ':') ADVANCE(143);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '<') ADVANCE(163);
      if (lookahead == '=') ADVANCE(140);
      if (lookahead == '>') ADVANCE(167);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == '^') ADVANCE(174);
      if (lookahead == '_') ADVANCE(19);
      if (lookahead == '`') ADVANCE(178);
      if (lookahead == 'e') ADVANCE(74);
      if (lookahead == 'f') ADVANCE(75);
      if (lookahead == 'i') ADVANCE(72);
      if (lookahead == 'p') ADVANCE(54);
      if (lookahead == 'r') ADVANCE(45);
      if (lookahead == 's') ADVANCE(108);
      if (lookahead == 'u') ADVANCE(89);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '|') ADVANCE(175);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(115)
      END_STATE();
    case 116:
      if (eof) ADVANCE(117);
      if (lookahead == '#') ADVANCE(118);
      if (lookahead == '$') ADVANCE(28);
      if (lookahead == '&') ADVANCE(173);
      if (lookahead == ')') ADVANCE(126);
      if (lookahead == '*') ADVANCE(151);
      if (lookahead == '+') ADVANCE(158);
      if (lookahead == ',') ADVANCE(134);
      if (lookahead == '-') ADVANCE(159);
      if (lookahead == '.') ADVANCE(186);
      if (lookahead == '/') ADVANCE(15);
      if (lookahead == ':') ADVANCE(143);
      if (lookahead == ';') ADVANCE(136);
      if (lookahead == '<') ADVANCE(163);
      if (lookahead == '=') ADVANCE(139);
      if (lookahead == '>') ADVANCE(167);
      if (lookahead == ']') ADVANCE(120);
      if (lookahead == '^') ADVANCE(174);
      if (lookahead == '`') ADVANCE(178);
      if (lookahead == 'e') ADVANCE(69);
      if (lookahead == 'f') ADVANCE(75);
      if (lookahead == 'i') ADVANCE(72);
      if (lookahead == 'p') ADVANCE(54);
      if (lookahead == 'r') ADVANCE(45);
      if (lookahead == 's') ADVANCE(108);
      if (lookahead == 'u') ADVANCE(89);
      if (lookahead == '{') ADVANCE(132);
      if (lookahead == '|') ADVANCE(175);
      if (lookahead == '}') ADVANCE(133);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(116)
      END_STATE();
    case 117:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 118:
      ACCEPT_TOKEN(anon_sym_POUND);
      END_STATE();
    case 119:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 120:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 121:
      ACCEPT_TOKEN(anon_sym_fn);
      END_STATE();
    case 122:
      ACCEPT_TOKEN(anon_sym_entity);
      END_STATE();
    case 123:
      ACCEPT_TOKEN(anon_sym_DASH_GT);
      END_STATE();
    case 124:
      ACCEPT_TOKEN(anon_sym_pipeline);
      END_STATE();
    case 125:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 126:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 127:
      ACCEPT_TOKEN(anon_sym_struct);
      END_STATE();
    case 128:
      ACCEPT_TOKEN(anon_sym_port);
      END_STATE();
    case 129:
      ACCEPT_TOKEN(anon_sym_port);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 130:
      ACCEPT_TOKEN(anon_sym_enum);
      END_STATE();
    case 131:
      ACCEPT_TOKEN(anon_sym_impl);
      END_STATE();
    case 132:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 133:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 134:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 135:
      ACCEPT_TOKEN(anon_sym_use);
      END_STATE();
    case 136:
      ACCEPT_TOKEN(anon_sym_SEMI);
      END_STATE();
    case 137:
      ACCEPT_TOKEN(anon_sym_DOLLARconfig);
      END_STATE();
    case 138:
      ACCEPT_TOKEN(anon_sym_EQ);
      END_STATE();
    case 139:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '=') ADVANCE(161);
      END_STATE();
    case 140:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '=') ADVANCE(161);
      if (lookahead == '>') ADVANCE(185);
      END_STATE();
    case 141:
      ACCEPT_TOKEN(anon_sym_let);
      END_STATE();
    case 142:
      ACCEPT_TOKEN(anon_sym_let);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 143:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 144:
      ACCEPT_TOKEN(anon_sym_COLON);
      if (lookahead == ':') ADVANCE(259);
      END_STATE();
    case 145:
      ACCEPT_TOKEN(anon_sym_reg);
      END_STATE();
    case 146:
      ACCEPT_TOKEN(anon_sym_reg);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 147:
      ACCEPT_TOKEN(anon_sym_set);
      END_STATE();
    case 148:
      ACCEPT_TOKEN(anon_sym_set);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 149:
      ACCEPT_TOKEN(anon_sym_decl);
      END_STATE();
    case 150:
      ACCEPT_TOKEN(anon_sym_decl);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 151:
      ACCEPT_TOKEN(anon_sym_STAR);
      END_STATE();
    case 152:
      ACCEPT_TOKEN(anon_sym_SQUOTE);
      END_STATE();
    case 153:
      ACCEPT_TOKEN(anon_sym_DOLLARif);
      END_STATE();
    case 154:
      ACCEPT_TOKEN(anon_sym_DOLLARelse);
      END_STATE();
    case 155:
      ACCEPT_TOKEN(anon_sym_assert);
      END_STATE();
    case 156:
      ACCEPT_TOKEN(anon_sym_assert);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 157:
      ACCEPT_TOKEN(anon_sym_reset);
      END_STATE();
    case 158:
      ACCEPT_TOKEN(anon_sym_PLUS);
      END_STATE();
    case 159:
      ACCEPT_TOKEN(anon_sym_DASH);
      END_STATE();
    case 160:
      ACCEPT_TOKEN(anon_sym_DASH);
      if (lookahead == '>') ADVANCE(123);
      END_STATE();
    case 161:
      ACCEPT_TOKEN(sym_op_equals);
      END_STATE();
    case 162:
      ACCEPT_TOKEN(anon_sym_LT);
      END_STATE();
    case 163:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '<') ADVANCE(170);
      if (lookahead == '=') ADVANCE(168);
      END_STATE();
    case 164:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '=') ADVANCE(168);
      END_STATE();
    case 165:
      ACCEPT_TOKEN(anon_sym_GT);
      END_STATE();
    case 166:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead == '=') ADVANCE(169);
      END_STATE();
    case 167:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead == '=') ADVANCE(169);
      if (lookahead == '>') ADVANCE(171);
      END_STATE();
    case 168:
      ACCEPT_TOKEN(sym_op_le);
      END_STATE();
    case 169:
      ACCEPT_TOKEN(sym_op_ge);
      END_STATE();
    case 170:
      ACCEPT_TOKEN(sym_op_lshift);
      END_STATE();
    case 171:
      ACCEPT_TOKEN(sym_op_rshift);
      END_STATE();
    case 172:
      ACCEPT_TOKEN(anon_sym_AMP);
      END_STATE();
    case 173:
      ACCEPT_TOKEN(anon_sym_AMP);
      if (lookahead == '&') ADVANCE(176);
      END_STATE();
    case 174:
      ACCEPT_TOKEN(sym_op_bitwise_xor);
      END_STATE();
    case 175:
      ACCEPT_TOKEN(sym_op_bitwise_or);
      if (lookahead == '|') ADVANCE(177);
      END_STATE();
    case 176:
      ACCEPT_TOKEN(sym_op_logical_and);
      END_STATE();
    case 177:
      ACCEPT_TOKEN(sym_op_logical_or);
      END_STATE();
    case 178:
      ACCEPT_TOKEN(anon_sym_BQUOTE);
      END_STATE();
    case 179:
      ACCEPT_TOKEN(anon_sym_BANG);
      END_STATE();
    case 180:
      ACCEPT_TOKEN(anon_sym_if);
      END_STATE();
    case 181:
      ACCEPT_TOKEN(anon_sym_if);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 182:
      ACCEPT_TOKEN(anon_sym_else);
      END_STATE();
    case 183:
      ACCEPT_TOKEN(anon_sym_match);
      END_STATE();
    case 184:
      ACCEPT_TOKEN(anon_sym_match);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 185:
      ACCEPT_TOKEN(anon_sym_EQ_GT);
      END_STATE();
    case 186:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 187:
      ACCEPT_TOKEN(anon_sym_inst);
      END_STATE();
    case 188:
      ACCEPT_TOKEN(anon_sym_inst);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 189:
      ACCEPT_TOKEN(anon_sym_stage);
      END_STATE();
    case 190:
      ACCEPT_TOKEN(anon_sym_stage);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 191:
      ACCEPT_TOKEN(anon_sym_RPAREN_DOT);
      END_STATE();
    case 192:
      ACCEPT_TOKEN(anon_sym_true);
      END_STATE();
    case 193:
      ACCEPT_TOKEN(anon_sym_true);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 194:
      ACCEPT_TOKEN(anon_sym_false);
      END_STATE();
    case 195:
      ACCEPT_TOKEN(anon_sym_false);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 196:
      ACCEPT_TOKEN(anon_sym___builtin__);
      END_STATE();
    case 197:
      ACCEPT_TOKEN(anon_sym_DOLLAR_LPAREN);
      END_STATE();
    case 198:
      ACCEPT_TOKEN(sym_self);
      END_STATE();
    case 199:
      ACCEPT_TOKEN(sym_self);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 200:
      ACCEPT_TOKEN(anon_sym_mut);
      END_STATE();
    case 201:
      ACCEPT_TOKEN(anon_sym_mut);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 202:
      ACCEPT_TOKEN(anon_sym_int);
      END_STATE();
    case 203:
      ACCEPT_TOKEN(anon_sym_int);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 204:
      ACCEPT_TOKEN(anon_sym_bool);
      END_STATE();
    case 205:
      ACCEPT_TOKEN(anon_sym_bool);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 206:
      ACCEPT_TOKEN(anon_sym_clock);
      END_STATE();
    case 207:
      ACCEPT_TOKEN(anon_sym_clock);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 208:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(235);
      if (sym_identifier_character_set_4(lookahead)) ADVANCE(258);
      END_STATE();
    case 209:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(255);
      if (sym_identifier_character_set_4(lookahead)) ADVANCE(258);
      END_STATE();
    case 210:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(227);
      if (sym_identifier_character_set_4(lookahead)) ADVANCE(258);
      END_STATE();
    case 211:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(228);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 212:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(229);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 213:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(232);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 214:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(213);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 215:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(249);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 216:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(226);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 217:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(230);
      if (lookahead == 't') ADVANCE(210);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 218:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(193);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 219:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(195);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 220:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(190);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 221:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(243);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 222:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(231);
      if (lookahead == 't') ADVANCE(210);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 223:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(231);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 224:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(181);
      if (lookahead == 'n') ADVANCE(246);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 225:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(199);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 226:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(146);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 227:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(220);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 228:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'h') ADVANCE(184);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 229:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'k') ADVANCE(207);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 230:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(225);
      if (lookahead == 't') ADVANCE(148);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 231:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(225);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 232:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(150);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 233:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(205);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 234:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(239);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 235:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(248);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 236:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(246);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 237:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(252);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 238:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(240);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 239:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(212);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 240:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(233);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 241:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(244);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 242:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(256);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 243:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(251);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 244:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(254);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 245:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(247);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 246:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(250);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 247:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(221);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 248:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(219);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 249:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(142);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 250:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(188);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 251:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(156);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 252:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(203);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 253:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(201);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 254:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(129);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 255:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(211);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 256:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(218);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 257:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(253);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 258:
      ACCEPT_TOKEN(sym_identifier);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(258);
      END_STATE();
    case 259:
      ACCEPT_TOKEN(anon_sym_COLON_COLON);
      END_STATE();
    case 260:
      ACCEPT_TOKEN(aux_sym_int_literal_token1);
      if (lookahead == 'b') ADVANCE(113);
      if (lookahead == 'x') ADVANCE(114);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(261);
      END_STATE();
    case 261:
      ACCEPT_TOKEN(aux_sym_int_literal_token1);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(261);
      END_STATE();
    case 262:
      ACCEPT_TOKEN(aux_sym_int_literal_token2);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(262);
      END_STATE();
    case 263:
      ACCEPT_TOKEN(aux_sym_int_literal_token3);
      if (lookahead == '0' ||
          lookahead == '1' ||
          lookahead == '_') ADVANCE(263);
      END_STATE();
    case 264:
      ACCEPT_TOKEN(sym_line_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(264);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 1},
  [3] = {.lex_state = 1},
  [4] = {.lex_state = 2},
  [5] = {.lex_state = 2},
  [6] = {.lex_state = 2},
  [7] = {.lex_state = 2},
  [8] = {.lex_state = 2},
  [9] = {.lex_state = 2},
  [10] = {.lex_state = 2},
  [11] = {.lex_state = 2},
  [12] = {.lex_state = 2},
  [13] = {.lex_state = 2},
  [14] = {.lex_state = 2},
  [15] = {.lex_state = 2},
  [16] = {.lex_state = 2},
  [17] = {.lex_state = 2},
  [18] = {.lex_state = 2},
  [19] = {.lex_state = 2},
  [20] = {.lex_state = 2},
  [21] = {.lex_state = 2},
  [22] = {.lex_state = 2},
  [23] = {.lex_state = 2},
  [24] = {.lex_state = 2},
  [25] = {.lex_state = 2},
  [26] = {.lex_state = 2},
  [27] = {.lex_state = 2},
  [28] = {.lex_state = 2},
  [29] = {.lex_state = 2},
  [30] = {.lex_state = 2},
  [31] = {.lex_state = 2},
  [32] = {.lex_state = 2},
  [33] = {.lex_state = 2},
  [34] = {.lex_state = 2},
  [35] = {.lex_state = 2},
  [36] = {.lex_state = 2},
  [37] = {.lex_state = 2},
  [38] = {.lex_state = 2},
  [39] = {.lex_state = 2},
  [40] = {.lex_state = 2},
  [41] = {.lex_state = 2},
  [42] = {.lex_state = 2},
  [43] = {.lex_state = 115},
  [44] = {.lex_state = 1},
  [45] = {.lex_state = 116},
  [46] = {.lex_state = 116},
  [47] = {.lex_state = 1},
  [48] = {.lex_state = 1},
  [49] = {.lex_state = 1},
  [50] = {.lex_state = 1},
  [51] = {.lex_state = 1},
  [52] = {.lex_state = 1},
  [53] = {.lex_state = 1},
  [54] = {.lex_state = 1},
  [55] = {.lex_state = 1},
  [56] = {.lex_state = 1},
  [57] = {.lex_state = 1},
  [58] = {.lex_state = 1},
  [59] = {.lex_state = 1},
  [60] = {.lex_state = 1},
  [61] = {.lex_state = 1},
  [62] = {.lex_state = 1},
  [63] = {.lex_state = 1},
  [64] = {.lex_state = 1},
  [65] = {.lex_state = 1},
  [66] = {.lex_state = 1},
  [67] = {.lex_state = 1},
  [68] = {.lex_state = 1},
  [69] = {.lex_state = 1},
  [70] = {.lex_state = 1},
  [71] = {.lex_state = 1},
  [72] = {.lex_state = 1},
  [73] = {.lex_state = 1},
  [74] = {.lex_state = 1},
  [75] = {.lex_state = 115},
  [76] = {.lex_state = 1},
  [77] = {.lex_state = 1},
  [78] = {.lex_state = 1},
  [79] = {.lex_state = 1},
  [80] = {.lex_state = 1},
  [81] = {.lex_state = 1},
  [82] = {.lex_state = 1},
  [83] = {.lex_state = 1},
  [84] = {.lex_state = 1},
  [85] = {.lex_state = 1},
  [86] = {.lex_state = 1},
  [87] = {.lex_state = 1},
  [88] = {.lex_state = 1},
  [89] = {.lex_state = 1},
  [90] = {.lex_state = 1},
  [91] = {.lex_state = 1},
  [92] = {.lex_state = 1},
  [93] = {.lex_state = 1},
  [94] = {.lex_state = 1},
  [95] = {.lex_state = 1},
  [96] = {.lex_state = 1},
  [97] = {.lex_state = 1},
  [98] = {.lex_state = 1},
  [99] = {.lex_state = 1},
  [100] = {.lex_state = 1},
  [101] = {.lex_state = 1},
  [102] = {.lex_state = 1},
  [103] = {.lex_state = 1},
  [104] = {.lex_state = 1},
  [105] = {.lex_state = 1},
  [106] = {.lex_state = 1},
  [107] = {.lex_state = 1},
  [108] = {.lex_state = 1},
  [109] = {.lex_state = 1},
  [110] = {.lex_state = 1},
  [111] = {.lex_state = 1},
  [112] = {.lex_state = 1},
  [113] = {.lex_state = 1},
  [114] = {.lex_state = 1},
  [115] = {.lex_state = 1},
  [116] = {.lex_state = 1},
  [117] = {.lex_state = 1},
  [118] = {.lex_state = 1},
  [119] = {.lex_state = 1},
  [120] = {.lex_state = 1},
  [121] = {.lex_state = 1},
  [122] = {.lex_state = 1},
  [123] = {.lex_state = 1},
  [124] = {.lex_state = 1},
  [125] = {.lex_state = 0},
  [126] = {.lex_state = 0},
  [127] = {.lex_state = 7},
  [128] = {.lex_state = 7},
  [129] = {.lex_state = 7},
  [130] = {.lex_state = 5},
  [131] = {.lex_state = 6},
  [132] = {.lex_state = 5},
  [133] = {.lex_state = 5},
  [134] = {.lex_state = 6},
  [135] = {.lex_state = 5},
  [136] = {.lex_state = 5},
  [137] = {.lex_state = 5},
  [138] = {.lex_state = 0},
  [139] = {.lex_state = 0},
  [140] = {.lex_state = 7},
  [141] = {.lex_state = 0},
  [142] = {.lex_state = 0},
  [143] = {.lex_state = 7},
  [144] = {.lex_state = 0},
  [145] = {.lex_state = 0},
  [146] = {.lex_state = 5},
  [147] = {.lex_state = 5},
  [148] = {.lex_state = 5},
  [149] = {.lex_state = 7},
  [150] = {.lex_state = 5},
  [151] = {.lex_state = 0},
  [152] = {.lex_state = 0},
  [153] = {.lex_state = 5},
  [154] = {.lex_state = 7},
  [155] = {.lex_state = 5},
  [156] = {.lex_state = 0},
  [157] = {.lex_state = 5},
  [158] = {.lex_state = 7},
  [159] = {.lex_state = 5},
  [160] = {.lex_state = 5},
  [161] = {.lex_state = 0},
  [162] = {.lex_state = 2},
  [163] = {.lex_state = 5},
  [164] = {.lex_state = 5},
  [165] = {.lex_state = 7},
  [166] = {.lex_state = 5},
  [167] = {.lex_state = 2},
  [168] = {.lex_state = 2},
  [169] = {.lex_state = 2},
  [170] = {.lex_state = 2},
  [171] = {.lex_state = 2},
  [172] = {.lex_state = 2},
  [173] = {.lex_state = 7},
  [174] = {.lex_state = 7},
  [175] = {.lex_state = 7},
  [176] = {.lex_state = 7},
  [177] = {.lex_state = 0},
  [178] = {.lex_state = 0},
  [179] = {.lex_state = 11},
  [180] = {.lex_state = 0},
  [181] = {.lex_state = 0},
  [182] = {.lex_state = 0},
  [183] = {.lex_state = 0},
  [184] = {.lex_state = 0},
  [185] = {.lex_state = 0},
  [186] = {.lex_state = 11},
  [187] = {.lex_state = 0},
  [188] = {.lex_state = 0},
  [189] = {.lex_state = 0},
  [190] = {.lex_state = 0},
  [191] = {.lex_state = 11},
  [192] = {.lex_state = 11},
  [193] = {.lex_state = 0},
  [194] = {.lex_state = 0},
  [195] = {.lex_state = 0},
  [196] = {.lex_state = 11},
  [197] = {.lex_state = 0},
  [198] = {.lex_state = 0},
  [199] = {.lex_state = 11},
  [200] = {.lex_state = 11},
  [201] = {.lex_state = 0},
  [202] = {.lex_state = 11},
  [203] = {.lex_state = 0},
  [204] = {.lex_state = 0},
  [205] = {.lex_state = 0},
  [206] = {.lex_state = 11},
  [207] = {.lex_state = 0},
  [208] = {.lex_state = 115},
  [209] = {.lex_state = 11},
  [210] = {.lex_state = 11},
  [211] = {.lex_state = 0},
  [212] = {.lex_state = 0},
  [213] = {.lex_state = 11},
  [214] = {.lex_state = 0},
  [215] = {.lex_state = 0},
  [216] = {.lex_state = 11},
  [217] = {.lex_state = 0},
  [218] = {.lex_state = 0},
  [219] = {.lex_state = 11},
  [220] = {.lex_state = 0},
  [221] = {.lex_state = 7},
  [222] = {.lex_state = 0},
  [223] = {.lex_state = 2},
  [224] = {.lex_state = 0},
  [225] = {.lex_state = 4},
  [226] = {.lex_state = 0},
  [227] = {.lex_state = 2},
  [228] = {.lex_state = 0},
  [229] = {.lex_state = 0},
  [230] = {.lex_state = 3},
  [231] = {.lex_state = 7},
  [232] = {.lex_state = 0},
  [233] = {.lex_state = 0},
  [234] = {.lex_state = 3},
  [235] = {.lex_state = 3},
  [236] = {.lex_state = 3},
  [237] = {.lex_state = 3},
  [238] = {.lex_state = 0},
  [239] = {.lex_state = 3},
  [240] = {.lex_state = 3},
  [241] = {.lex_state = 3},
  [242] = {.lex_state = 0},
  [243] = {.lex_state = 3},
  [244] = {.lex_state = 0},
  [245] = {.lex_state = 3},
  [246] = {.lex_state = 115},
  [247] = {.lex_state = 115},
  [248] = {.lex_state = 3},
  [249] = {.lex_state = 115},
  [250] = {.lex_state = 115},
  [251] = {.lex_state = 11},
  [252] = {.lex_state = 115},
  [253] = {.lex_state = 115},
  [254] = {.lex_state = 115},
  [255] = {.lex_state = 115},
  [256] = {.lex_state = 115},
  [257] = {.lex_state = 3},
  [258] = {.lex_state = 11},
  [259] = {.lex_state = 115},
  [260] = {.lex_state = 115},
  [261] = {.lex_state = 115},
  [262] = {.lex_state = 115},
  [263] = {.lex_state = 115},
  [264] = {.lex_state = 0},
  [265] = {.lex_state = 3},
  [266] = {.lex_state = 0},
  [267] = {.lex_state = 0},
  [268] = {.lex_state = 0},
  [269] = {.lex_state = 0},
  [270] = {.lex_state = 3},
  [271] = {.lex_state = 0},
  [272] = {.lex_state = 3},
  [273] = {.lex_state = 0},
  [274] = {.lex_state = 0},
  [275] = {.lex_state = 0},
  [276] = {.lex_state = 0},
  [277] = {.lex_state = 3},
  [278] = {.lex_state = 0},
  [279] = {.lex_state = 3},
  [280] = {.lex_state = 3},
  [281] = {.lex_state = 3},
  [282] = {.lex_state = 0},
  [283] = {.lex_state = 3},
  [284] = {.lex_state = 0},
  [285] = {.lex_state = 0},
  [286] = {.lex_state = 0},
  [287] = {.lex_state = 0},
  [288] = {.lex_state = 116},
  [289] = {.lex_state = 6},
  [290] = {.lex_state = 3},
  [291] = {.lex_state = 0},
  [292] = {.lex_state = 0},
  [293] = {.lex_state = 3},
  [294] = {.lex_state = 3},
  [295] = {.lex_state = 0},
  [296] = {.lex_state = 0},
  [297] = {.lex_state = 3},
  [298] = {.lex_state = 3},
  [299] = {.lex_state = 0},
  [300] = {.lex_state = 11},
  [301] = {.lex_state = 0},
  [302] = {.lex_state = 11},
  [303] = {.lex_state = 0},
  [304] = {.lex_state = 7},
  [305] = {.lex_state = 0},
  [306] = {.lex_state = 0},
  [307] = {.lex_state = 0},
  [308] = {.lex_state = 116},
  [309] = {.lex_state = 3},
  [310] = {.lex_state = 3},
  [311] = {.lex_state = 7},
  [312] = {.lex_state = 3},
  [313] = {.lex_state = 0},
  [314] = {.lex_state = 0},
  [315] = {.lex_state = 0},
  [316] = {.lex_state = 11},
  [317] = {.lex_state = 0},
  [318] = {.lex_state = 115},
  [319] = {.lex_state = 0},
  [320] = {.lex_state = 0},
  [321] = {.lex_state = 0},
  [322] = {.lex_state = 7},
  [323] = {.lex_state = 7},
  [324] = {.lex_state = 0},
  [325] = {.lex_state = 3},
  [326] = {.lex_state = 0},
  [327] = {.lex_state = 0},
  [328] = {.lex_state = 0},
  [329] = {.lex_state = 0},
  [330] = {.lex_state = 0},
  [331] = {.lex_state = 0},
  [332] = {.lex_state = 0},
  [333] = {.lex_state = 11},
  [334] = {.lex_state = 115},
  [335] = {.lex_state = 0},
  [336] = {.lex_state = 0},
  [337] = {.lex_state = 3},
  [338] = {.lex_state = 0},
  [339] = {.lex_state = 0},
  [340] = {.lex_state = 0},
  [341] = {.lex_state = 11},
  [342] = {.lex_state = 0},
  [343] = {.lex_state = 0},
  [344] = {.lex_state = 0},
  [345] = {.lex_state = 11},
  [346] = {.lex_state = 3},
  [347] = {.lex_state = 0},
  [348] = {.lex_state = 0},
  [349] = {.lex_state = 3},
  [350] = {.lex_state = 0},
  [351] = {.lex_state = 0},
  [352] = {.lex_state = 115},
  [353] = {.lex_state = 0},
  [354] = {.lex_state = 7},
  [355] = {.lex_state = 0},
  [356] = {.lex_state = 7},
  [357] = {.lex_state = 0},
  [358] = {.lex_state = 0},
  [359] = {.lex_state = 7},
  [360] = {.lex_state = 116},
  [361] = {.lex_state = 0},
  [362] = {.lex_state = 0},
  [363] = {.lex_state = 0},
  [364] = {.lex_state = 3},
  [365] = {.lex_state = 0},
  [366] = {.lex_state = 0},
  [367] = {.lex_state = 7},
  [368] = {.lex_state = 0},
  [369] = {.lex_state = 0},
  [370] = {.lex_state = 0},
  [371] = {.lex_state = 3},
  [372] = {.lex_state = 3},
  [373] = {.lex_state = 0},
  [374] = {.lex_state = 0},
  [375] = {.lex_state = 7},
  [376] = {.lex_state = 0},
  [377] = {.lex_state = 0},
  [378] = {.lex_state = 14},
  [379] = {.lex_state = 13},
  [380] = {.lex_state = 0},
  [381] = {.lex_state = 0},
  [382] = {.lex_state = 0},
  [383] = {.lex_state = 3},
  [384] = {.lex_state = 0},
  [385] = {.lex_state = 115},
  [386] = {.lex_state = 3},
  [387] = {.lex_state = 3},
  [388] = {.lex_state = 0},
  [389] = {.lex_state = 2},
  [390] = {.lex_state = 0},
  [391] = {.lex_state = 3},
  [392] = {.lex_state = 3},
  [393] = {.lex_state = 3},
  [394] = {.lex_state = 0},
  [395] = {.lex_state = 0},
  [396] = {.lex_state = 115},
  [397] = {.lex_state = 1},
  [398] = {.lex_state = 3},
  [399] = {.lex_state = 0},
  [400] = {.lex_state = 1},
  [401] = {.lex_state = 115},
  [402] = {.lex_state = 3},
  [403] = {.lex_state = 3},
  [404] = {.lex_state = 0},
  [405] = {.lex_state = 0},
  [406] = {.lex_state = 3},
  [407] = {.lex_state = 0},
  [408] = {.lex_state = 0},
  [409] = {.lex_state = 3},
  [410] = {.lex_state = 3},
  [411] = {.lex_state = 3},
  [412] = {.lex_state = 0},
  [413] = {.lex_state = 1},
  [414] = {.lex_state = 6},
  [415] = {.lex_state = 1},
  [416] = {.lex_state = 2},
  [417] = {.lex_state = 0},
  [418] = {.lex_state = 1},
  [419] = {.lex_state = 0},
  [420] = {.lex_state = 0},
  [421] = {.lex_state = 3},
  [422] = {.lex_state = 0},
  [423] = {.lex_state = 0},
  [424] = {.lex_state = 3},
  [425] = {.lex_state = 0},
  [426] = {.lex_state = 3},
  [427] = {.lex_state = 3},
  [428] = {.lex_state = 0},
  [429] = {.lex_state = 3},
  [430] = {.lex_state = 3},
  [431] = {.lex_state = 0},
  [432] = {.lex_state = 3},
  [433] = {.lex_state = 0},
  [434] = {.lex_state = 0},
  [435] = {.lex_state = 6},
  [436] = {.lex_state = 3},
  [437] = {.lex_state = 6},
  [438] = {.lex_state = 0},
  [439] = {.lex_state = 3},
  [440] = {.lex_state = 0},
  [441] = {.lex_state = 0},
  [442] = {.lex_state = 0},
  [443] = {.lex_state = 0},
  [444] = {.lex_state = 3},
  [445] = {.lex_state = 3},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_POUND] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_fn] = ACTIONS(1),
    [anon_sym_entity] = ACTIONS(1),
    [anon_sym_DASH_GT] = ACTIONS(1),
    [anon_sym_pipeline] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_struct] = ACTIONS(1),
    [anon_sym_port] = ACTIONS(1),
    [anon_sym_enum] = ACTIONS(1),
    [anon_sym_impl] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [anon_sym_use] = ACTIONS(1),
    [anon_sym_SEMI] = ACTIONS(1),
    [anon_sym_DOLLARconfig] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [anon_sym_let] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [anon_sym_reg] = ACTIONS(1),
    [anon_sym_set] = ACTIONS(1),
    [anon_sym_decl] = ACTIONS(1),
    [anon_sym_STAR] = ACTIONS(1),
    [anon_sym_SQUOTE] = ACTIONS(1),
    [anon_sym_DOLLARif] = ACTIONS(1),
    [anon_sym_DOLLARelse] = ACTIONS(1),
    [anon_sym_assert] = ACTIONS(1),
    [anon_sym_reset] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [anon_sym_DASH] = ACTIONS(1),
    [sym_op_equals] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [sym_op_le] = ACTIONS(1),
    [sym_op_ge] = ACTIONS(1),
    [sym_op_lshift] = ACTIONS(1),
    [sym_op_rshift] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
    [sym_op_bitwise_xor] = ACTIONS(1),
    [sym_op_bitwise_or] = ACTIONS(1),
    [sym_op_logical_and] = ACTIONS(1),
    [sym_op_logical_or] = ACTIONS(1),
    [anon_sym_BQUOTE] = ACTIONS(1),
    [anon_sym_BANG] = ACTIONS(1),
    [anon_sym_if] = ACTIONS(1),
    [anon_sym_else] = ACTIONS(1),
    [anon_sym_match] = ACTIONS(1),
    [anon_sym_EQ_GT] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
    [anon_sym_inst] = ACTIONS(1),
    [anon_sym_stage] = ACTIONS(1),
    [anon_sym_true] = ACTIONS(1),
    [anon_sym_false] = ACTIONS(1),
    [anon_sym___builtin__] = ACTIONS(1),
    [anon_sym_DOLLAR_LPAREN] = ACTIONS(1),
    [sym_self] = ACTIONS(1),
    [anon_sym_mut] = ACTIONS(1),
    [anon_sym_int] = ACTIONS(1),
    [anon_sym_bool] = ACTIONS(1),
    [anon_sym_clock] = ACTIONS(1),
    [anon_sym_COLON_COLON] = ACTIONS(1),
    [aux_sym_int_literal_token1] = ACTIONS(1),
    [aux_sym_int_literal_token2] = ACTIONS(1),
    [aux_sym_int_literal_token3] = ACTIONS(1),
    [sym_line_comment] = ACTIONS(3),
  },
  [1] = {
    [sym_source_file] = STATE(428),
    [sym_attribute] = STATE(151),
    [sym__item] = STATE(125),
    [sym_unit_definition] = STATE(125),
    [sym__pipeline_start] = STATE(444),
    [sym_struct_definition] = STATE(125),
    [sym_enum_definition] = STATE(125),
    [sym_impl] = STATE(125),
    [sym_use] = STATE(125),
    [sym_comptime_config] = STATE(125),
    [aux_sym_source_file_repeat1] = STATE(125),
    [aux_sym__item_repeat1] = STATE(151),
    [ts_builtin_sym_end] = ACTIONS(5),
    [anon_sym_POUND] = ACTIONS(7),
    [anon_sym_fn] = ACTIONS(9),
    [anon_sym_entity] = ACTIONS(9),
    [anon_sym_pipeline] = ACTIONS(11),
    [anon_sym_struct] = ACTIONS(13),
    [anon_sym_enum] = ACTIONS(15),
    [anon_sym_impl] = ACTIONS(17),
    [anon_sym_use] = ACTIONS(19),
    [anon_sym_DOLLARconfig] = ACTIONS(21),
    [sym_line_comment] = ACTIONS(3),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 31,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      anon_sym_let,
    ACTIONS(31), 1,
      anon_sym_reg,
    ACTIONS(33), 1,
      anon_sym_set,
    ACTIONS(35), 1,
      anon_sym_decl,
    ACTIONS(39), 1,
      anon_sym_SQUOTE,
    ACTIONS(41), 1,
      anon_sym_DOLLARif,
    ACTIONS(43), 1,
      anon_sym_assert,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(44), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(390), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
    STATE(84), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [122] = 31,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      anon_sym_let,
    ACTIONS(31), 1,
      anon_sym_reg,
    ACTIONS(33), 1,
      anon_sym_set,
    ACTIONS(35), 1,
      anon_sym_decl,
    ACTIONS(39), 1,
      anon_sym_SQUOTE,
    ACTIONS(41), 1,
      anon_sym_DOLLARif,
    ACTIONS(43), 1,
      anon_sym_assert,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(2), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(390), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
    STATE(83), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [244] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(67), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(66), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [334] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(69), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(61), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [424] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(71), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(60), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [514] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(73), 1,
      anon_sym_RBRACK,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(60), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [604] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(75), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(66), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [694] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(77), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(60), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [784] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(79), 1,
      anon_sym_RBRACK,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(62), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [874] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(81), 1,
      anon_sym_RPAREN,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(59), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [964] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(83), 1,
      anon_sym_RBRACK,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(60), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1054] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(73), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1141] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(50), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1228] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(60), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1315] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(63), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1402] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(65), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1489] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(66), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1576] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(57), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1663] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(58), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1750] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(77), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1837] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(85), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [1924] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(86), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2011] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(47), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2098] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(64), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2185] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(79), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2272] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(69), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2359] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(48), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2446] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(80), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2533] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(54), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2620] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(72), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2707] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(81), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2794] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(82), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2881] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(71), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [2968] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(56), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3055] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(53), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3142] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(52), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3229] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(67), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3316] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(74), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3403] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(51), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3490] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(55), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3577] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(47), 1,
      anon_sym_if,
    ACTIONS(49), 1,
      anon_sym_match,
    ACTIONS(51), 1,
      anon_sym_inst,
    ACTIONS(53), 1,
      anon_sym_stage,
    ACTIONS(57), 1,
      sym_self,
    ACTIONS(59), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(35), 1,
      sym_op_sub,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(267), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
    ACTIONS(37), 2,
      anon_sym_STAR,
      anon_sym_BANG,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(49), 7,
      sym_block,
      sym__expression,
      sym_binary_expression,
      sym_unary_expression,
      sym__base_expression,
      sym_field_access,
      sym_method_call,
    STATE(113), 12,
      sym_if_expression,
      sym_match_expression,
      sym__simple_base_expression,
      sym_array_literal,
      sym_tuple_literal,
      sym_paren_expression,
      sym_function_call,
      sym_entity_instance,
      sym_pipeline_instance,
      sym_stage_reference,
      sym_bool_literal,
      sym_int_literal,
  [3664] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(87), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(85), 33,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_RPAREN,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_SEMI,
      anon_sym_DOLLARconfig,
      anon_sym_COLON,
      anon_sym_STAR,
      anon_sym_reset,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_EQ_GT,
      anon_sym_DOT,
      anon_sym___builtin__,
  [3710] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(91), 1,
      anon_sym_let,
    ACTIONS(94), 1,
      anon_sym_reg,
    ACTIONS(97), 1,
      anon_sym_set,
    ACTIONS(100), 1,
      anon_sym_decl,
    ACTIONS(103), 1,
      anon_sym_SQUOTE,
    ACTIONS(106), 1,
      anon_sym_DOLLARif,
    ACTIONS(109), 1,
      anon_sym_assert,
    STATE(44), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(390), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
    ACTIONS(89), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(112), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [3771] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(116), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(114), 30,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_RPAREN,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_SEMI,
      anon_sym_DOLLARconfig,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_else,
      anon_sym_DOT,
  [3814] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(120), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(118), 30,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_RPAREN,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_SEMI,
      anon_sym_DOLLARconfig,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_else,
      anon_sym_DOT,
  [3857] = 20,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(124), 1,
      anon_sym_EQ,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 9,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [3932] = 21,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(124), 1,
      anon_sym_EQ,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 8,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4009] = 17,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    ACTIONS(124), 3,
      anon_sym_EQ,
      anon_sym_AMP,
      sym_op_bitwise_or,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4078] = 19,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(124), 2,
      anon_sym_EQ,
      sym_op_bitwise_or,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 9,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4151] = 14,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(124), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(122), 12,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_le,
      sym_op_ge,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4214] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(124), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(122), 15,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4273] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(124), 1,
      anon_sym_EQ,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 7,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_BQUOTE,
  [4352] = 9,
    ACTIONS(3), 1,
      sym_line_comment,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(124), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(122), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4405] = 10,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(126), 1,
      anon_sym_STAR,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(124), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(122), 17,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4460] = 9,
    ACTIONS(3), 1,
      sym_line_comment,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(152), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(150), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4513] = 13,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(124), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(122), 13,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4574] = 18,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(124), 2,
      anon_sym_EQ,
      sym_op_bitwise_or,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
    ACTIONS(122), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [4645] = 24,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(154), 1,
      anon_sym_RPAREN,
    ACTIONS(156), 1,
      anon_sym_COMMA,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(324), 1,
      aux_sym_array_literal_repeat1,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [4724] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    ACTIONS(160), 3,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_COMMA,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [4799] = 24,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(162), 1,
      anon_sym_RPAREN,
    ACTIONS(164), 1,
      anon_sym_COMMA,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(314), 1,
      aux_sym__positional_argument_list_repeat1,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [4878] = 24,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(166), 1,
      anon_sym_RBRACK,
    ACTIONS(168), 1,
      anon_sym_COMMA,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(357), 1,
      aux_sym_array_literal_repeat1,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [4957] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(170), 1,
      anon_sym_RPAREN,
    ACTIONS(172), 1,
      anon_sym_COMMA,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5033] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(99), 1,
      sym_match_block,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5109] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    ACTIONS(176), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5183] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    ACTIONS(178), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5257] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(180), 1,
      anon_sym_RBRACE,
    ACTIONS(182), 1,
      anon_sym_COMMA,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5333] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(186), 1,
      anon_sym_LPAREN,
    ACTIONS(190), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(123), 1,
      sym_argument_list,
    STATE(118), 2,
      sym__named_argument_list,
      sym__positional_argument_list,
    ACTIONS(188), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(184), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [5377] = 23,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    STATE(399), 1,
      sym_block,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5453] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(196), 1,
      anon_sym_DOLLARelse,
    STATE(88), 1,
      sym_comptime_else,
    ACTIONS(192), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(194), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [5492] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(198), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5565] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(200), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5638] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(202), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5711] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(204), 1,
      anon_sym_EQ,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5784] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(208), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(206), 22,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_COLON,
      anon_sym_STAR,
      anon_sym_reset,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_EQ_GT,
      anon_sym_DOT,
  [5819] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(216), 1,
      anon_sym_COLON_COLON,
    ACTIONS(212), 2,
      anon_sym_LPAREN,
      anon_sym_DOLLAR_LPAREN,
    ACTIONS(214), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(210), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [5858] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(218), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [5931] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(196), 1,
      anon_sym_DOLLARelse,
    STATE(87), 1,
      sym_comptime_else,
    ACTIONS(220), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(222), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [5970] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(224), 1,
      anon_sym_COMMA,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6043] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(226), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6116] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(228), 1,
      anon_sym_RPAREN,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6189] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(230), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6262] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(232), 1,
      anon_sym_RBRACE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6335] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(234), 1,
      anon_sym_RBRACE,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6408] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(236), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6481] = 22,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(45), 1,
      anon_sym_DASH,
    ACTIONS(126), 1,
      anon_sym_STAR,
    ACTIONS(128), 1,
      anon_sym_PLUS,
    ACTIONS(130), 1,
      sym_op_equals,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(140), 1,
      anon_sym_AMP,
    ACTIONS(142), 1,
      sym_op_bitwise_xor,
    ACTIONS(144), 1,
      sym_op_bitwise_or,
    ACTIONS(146), 1,
      sym_op_logical_and,
    ACTIONS(148), 1,
      sym_op_logical_or,
    ACTIONS(158), 1,
      anon_sym_BQUOTE,
    ACTIONS(238), 1,
      anon_sym_SEMI,
    STATE(30), 1,
      sym_op_mul,
    STATE(36), 1,
      sym_op_custom_infix,
    STATE(37), 1,
      sym__op_shifty,
    STATE(42), 1,
      sym_op_bitwise_and,
    ACTIONS(136), 2,
      sym_op_le,
      sym_op_ge,
    ACTIONS(138), 2,
      sym_op_lshift,
      sym_op_rshift,
    STATE(40), 3,
      sym_op_lt,
      sym_op_gt,
      sym__op_relational,
    STATE(41), 3,
      sym_op_add,
      sym_op_sub,
      sym__op_add_like,
  [6554] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(192), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(194), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6587] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(240), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(242), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6620] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(244), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(246), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6653] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(248), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(250), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6686] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(252), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(254), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6719] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(256), 11,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(258), 14,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_assert,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [6752] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(262), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(260), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6784] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(266), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(264), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6816] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(270), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(268), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6848] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(274), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(272), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6880] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(278), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(276), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6912] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(282), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(280), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6944] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(286), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(284), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [6976] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(290), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(288), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7008] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(294), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(292), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7040] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(298), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(296), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7072] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(302), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(300), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7104] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(306), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(304), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7136] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(310), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(308), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7168] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(314), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(312), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7200] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(318), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(316), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7232] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(322), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(320), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7264] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(326), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(324), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7296] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(330), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(328), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7328] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(334), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(332), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7360] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(338), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(336), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7392] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(344), 1,
      anon_sym_DOT,
    ACTIONS(342), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(340), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [7426] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(348), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(346), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7458] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(352), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(350), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7490] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(356), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(354), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7522] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(360), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(358), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7554] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(364), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(362), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7586] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(368), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(366), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7618] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(372), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(370), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7650] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(376), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(374), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7682] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(380), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(378), 19,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
      anon_sym_DOT,
  [7714] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(384), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(382), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [7745] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(388), 5,
      anon_sym_EQ,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_AMP,
      sym_op_bitwise_or,
    ACTIONS(386), 18,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_DASH,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
      sym_op_lshift,
      sym_op_rshift,
      sym_op_bitwise_xor,
      sym_op_logical_and,
      sym_op_logical_or,
      anon_sym_BQUOTE,
  [7776] = 13,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(7), 1,
      anon_sym_POUND,
    ACTIONS(11), 1,
      anon_sym_pipeline,
    ACTIONS(13), 1,
      anon_sym_struct,
    ACTIONS(15), 1,
      anon_sym_enum,
    ACTIONS(17), 1,
      anon_sym_impl,
    ACTIONS(19), 1,
      anon_sym_use,
    ACTIONS(21), 1,
      anon_sym_DOLLARconfig,
    ACTIONS(390), 1,
      ts_builtin_sym_end,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(9), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(151), 2,
      sym_attribute,
      aux_sym__item_repeat1,
    STATE(126), 8,
      sym__item,
      sym_unit_definition,
      sym_struct_definition,
      sym_enum_definition,
      sym_impl,
      sym_use,
      sym_comptime_config,
      aux_sym_source_file_repeat1,
  [7825] = 13,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(392), 1,
      ts_builtin_sym_end,
    ACTIONS(394), 1,
      anon_sym_POUND,
    ACTIONS(400), 1,
      anon_sym_pipeline,
    ACTIONS(403), 1,
      anon_sym_struct,
    ACTIONS(406), 1,
      anon_sym_enum,
    ACTIONS(409), 1,
      anon_sym_impl,
    ACTIONS(412), 1,
      anon_sym_use,
    ACTIONS(415), 1,
      anon_sym_DOLLARconfig,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(397), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(151), 2,
      sym_attribute,
      aux_sym__item_repeat1,
    STATE(126), 8,
      sym__item,
      sym_unit_definition,
      sym_struct_definition,
      sym_enum_definition,
      sym_impl,
      sym_use,
      sym_comptime_config,
      aux_sym_source_file_repeat1,
  [7874] = 14,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(420), 1,
      anon_sym_RBRACE,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(384), 1,
      sym_last_match_arm,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(128), 2,
      sym_match_arm,
      aux_sym_match_block_repeat1,
    STATE(416), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [7925] = 13,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(404), 1,
      sym_last_match_arm,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(129), 2,
      sym_match_arm,
      aux_sym_match_block_repeat1,
    STATE(416), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [7973] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(424), 1,
      anon_sym_LPAREN,
    ACTIONS(430), 1,
      sym_identifier,
    ACTIONS(433), 1,
      anon_sym_COLON_COLON,
    ACTIONS(436), 1,
      aux_sym_int_literal_token1,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(427), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(439), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(129), 2,
      sym_match_arm,
      aux_sym_match_block_repeat1,
    STATE(389), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [8018] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(446), 1,
      anon_sym_RPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(304), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8063] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(456), 1,
      anon_sym_mut,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    STATE(199), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8108] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    ACTIONS(462), 1,
      anon_sym_GT,
    STATE(304), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8153] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    ACTIONS(464), 1,
      anon_sym_GT,
    STATE(304), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8198] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(466), 1,
      anon_sym_mut,
    STATE(199), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8243] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(468), 1,
      anon_sym_RPAREN,
    STATE(321), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8288] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(470), 1,
      anon_sym_RPAREN,
    STATE(304), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8333] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(344), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8375] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(89), 1,
      anon_sym_RBRACE,
    ACTIONS(472), 1,
      anon_sym_let,
    ACTIONS(475), 1,
      anon_sym_reg,
    ACTIONS(478), 1,
      anon_sym_set,
    ACTIONS(481), 1,
      anon_sym_decl,
    ACTIONS(484), 1,
      anon_sym_SQUOTE,
    ACTIONS(487), 1,
      anon_sym_DOLLARif,
    ACTIONS(490), 1,
      anon_sym_assert,
    STATE(138), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8417] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(493), 1,
      anon_sym_RBRACE,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    STATE(161), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8459] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(509), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(376), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [8503] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(511), 1,
      anon_sym_RBRACE,
    STATE(138), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8545] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(513), 1,
      anon_sym_RBRACE,
    STATE(141), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8587] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(515), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(376), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [8631] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(517), 1,
      anon_sym_RBRACE,
    STATE(138), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8673] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(519), 1,
      anon_sym_RBRACE,
    STATE(144), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [8715] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(330), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8757] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(209), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8799] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(308), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8841] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(521), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(376), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [8885] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    STATE(356), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [8927] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(7), 1,
      anon_sym_POUND,
    ACTIONS(11), 1,
      anon_sym_pipeline,
    ACTIONS(13), 1,
      anon_sym_struct,
    ACTIONS(15), 1,
      anon_sym_enum,
    ACTIONS(17), 1,
      anon_sym_impl,
    ACTIONS(19), 1,
      anon_sym_use,
    ACTIONS(21), 1,
      anon_sym_DOLLARconfig,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(9), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(189), 2,
      sym_attribute,
      aux_sym__item_repeat1,
    STATE(203), 6,
      sym_unit_definition,
      sym_struct_definition,
      sym_enum_definition,
      sym_impl,
      sym_use,
      sym_comptime_config,
  [8971] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(523), 1,
      anon_sym_RBRACE,
    STATE(138), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [9013] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(407), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9055] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(525), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(328), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9099] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    STATE(304), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9141] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(527), 1,
      anon_sym_RBRACE,
    STATE(152), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [9183] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(304), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9225] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(529), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(336), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9269] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(418), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9311] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(452), 1,
      sym_identifier,
    ACTIONS(454), 1,
      anon_sym_AMP,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    STATE(209), 1,
      sym_type,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9353] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(495), 1,
      anon_sym_let,
    ACTIONS(497), 1,
      anon_sym_reg,
    ACTIONS(499), 1,
      anon_sym_set,
    ACTIONS(501), 1,
      anon_sym_decl,
    ACTIONS(503), 1,
      anon_sym_SQUOTE,
    ACTIONS(505), 1,
      anon_sym_DOLLARif,
    ACTIONS(507), 1,
      anon_sym_assert,
    ACTIONS(531), 1,
      anon_sym_RBRACE,
    STATE(138), 4,
      sym__statement,
      sym_pipeline_stage_name,
      sym_comptime_if,
      aux_sym_block_repeat1,
    STATE(433), 6,
      sym_let_binding,
      sym_reg_statement,
      sym_set_statement,
      sym_decl_statement,
      sym_pipeline_reg_marker,
      sym_assert_statement,
  [9395] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(533), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(535), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9421] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(269), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9463] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(422), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9505] = 12,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    ACTIONS(537), 1,
      anon_sym_RPAREN,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(376), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9549] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(442), 1,
      anon_sym_LBRACK,
    ACTIONS(444), 1,
      anon_sym_LPAREN,
    ACTIONS(448), 1,
      anon_sym_AMP,
    ACTIONS(452), 1,
      sym_identifier,
    STATE(268), 1,
      sym_type,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(450), 3,
      anon_sym_int,
      anon_sym_bool,
      anon_sym_clock,
    STATE(179), 3,
      sym__base_type,
      sym_builtin_type,
      sym_int_literal,
    STATE(216), 4,
      sym_tuple_type,
      sym_array_type,
      sym_mut_wire,
      sym_wire,
  [9591] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(539), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(541), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9617] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(543), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(545), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9643] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(547), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(549), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9669] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(551), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(553), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9695] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(555), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(557), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9721] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(559), 9,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_STAR,
      anon_sym_DASH,
      anon_sym_BANG,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(561), 9,
      anon_sym_if,
      anon_sym_match,
      anon_sym_inst,
      anon_sym_stage,
      anon_sym_true,
      anon_sym_false,
      sym_self,
      sym_identifier,
      aux_sym_int_literal_token1,
  [9747] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(288), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9788] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(360), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9829] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(376), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9870] = 11,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    ACTIONS(418), 1,
      anon_sym_LPAREN,
    ACTIONS(422), 1,
      sym_identifier,
    STATE(208), 1,
      sym__scoped_or_raw_ident,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
    ACTIONS(55), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    STATE(361), 6,
      sym_bool_literal,
      sym__pattern,
      sym_tuple_pattern,
      sym_named_unpack,
      sym_positional_unpack,
      sym_int_literal,
  [9911] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(563), 12,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [9929] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(565), 12,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [9947] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(569), 1,
      anon_sym_LT,
    STATE(200), 1,
      sym__generic_list,
    ACTIONS(567), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [9969] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(571), 12,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [9987] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(573), 12,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10005] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(575), 11,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10022] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(577), 11,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10039] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(581), 2,
      anon_sym_EQ,
      anon_sym_COLON,
    ACTIONS(579), 9,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_reset,
      anon_sym_EQ_GT,
      anon_sym_DOLLAR_LPAREN,
      anon_sym_COLON_COLON,
  [10058] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(583), 11,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10075] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(585), 11,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym___builtin__,
  [10092] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(589), 2,
      anon_sym_EQ,
      anon_sym_COLON,
    ACTIONS(587), 9,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_reset,
      anon_sym_EQ_GT,
      anon_sym_DOLLAR_LPAREN,
      anon_sym_COLON_COLON,
  [10111] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(591), 11,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10128] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(593), 1,
      anon_sym_POUND,
    STATE(189), 2,
      sym_attribute,
      aux_sym__item_repeat1,
    ACTIONS(596), 8,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10149] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(598), 11,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_RBRACE,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10166] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(600), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10182] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(602), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10198] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(604), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10214] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(606), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10230] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(608), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10246] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(610), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10262] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(612), 1,
      anon_sym_DOLLARelse,
    STATE(229), 1,
      sym_comptime_else,
    ACTIONS(192), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10282] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(614), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10298] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(616), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10314] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(618), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10330] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(620), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10346] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(622), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10362] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(624), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10378] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(626), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10394] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(628), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10410] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(630), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10426] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(612), 1,
      anon_sym_DOLLARelse,
    STATE(224), 1,
      sym_comptime_else,
    ACTIONS(220), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10446] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(632), 1,
      anon_sym_LPAREN,
    ACTIONS(636), 1,
      anon_sym_EQ,
    ACTIONS(638), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(246), 1,
      sym__positional_pattern_list,
    STATE(247), 1,
      sym__named_pattern_list,
    ACTIONS(634), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [10472] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(640), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10488] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(642), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10504] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(644), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10520] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(646), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10536] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(648), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10552] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(650), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10568] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(216), 1,
      anon_sym_COLON_COLON,
    ACTIONS(652), 2,
      anon_sym_EQ,
      anon_sym_COLON,
    ACTIONS(212), 7,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_COMMA,
      anon_sym_reset,
      anon_sym_EQ_GT,
      anon_sym_DOLLAR_LPAREN,
  [10588] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(567), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10604] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(654), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10620] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(656), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10636] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(658), 10,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_COMMA,
      anon_sym_SEMI,
      anon_sym_EQ,
      anon_sym_reset,
      anon_sym_GT,
      anon_sym___builtin__,
  [10652] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(660), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10668] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(664), 1,
      anon_sym_RBRACE,
    ACTIONS(662), 4,
      anon_sym_LPAREN,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(666), 4,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
      aux_sym_int_literal_token1,
  [10687] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(668), 9,
      anon_sym_POUND,
      anon_sym_fn,
      anon_sym_entity,
      anon_sym_pipeline,
      anon_sym_struct,
      anon_sym_enum,
      anon_sym_impl,
      anon_sym_use,
      anon_sym_DOLLARconfig,
  [10702] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(670), 3,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
    STATE(275), 3,
      sym__comptime_operator,
      sym_op_lt,
      sym_op_gt,
  [10722] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(192), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10736] = 8,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(674), 1,
      anon_sym_RPAREN,
    ACTIONS(676), 1,
      sym_self,
    ACTIONS(678), 1,
      sym_identifier,
    STATE(303), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [10762] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(256), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10776] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(132), 1,
      anon_sym_LT,
    ACTIONS(134), 1,
      anon_sym_GT,
    ACTIONS(680), 3,
      sym_op_equals,
      sym_op_le,
      sym_op_ge,
    STATE(274), 3,
      sym__comptime_operator,
      sym_op_lt,
      sym_op_gt,
  [10796] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(244), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10810] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(240), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10824] = 8,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(682), 1,
      anon_sym_RPAREN,
    ACTIONS(684), 1,
      anon_sym_COMMA,
    ACTIONS(686), 1,
      sym_identifier,
    STATE(338), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [10850] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(662), 4,
      anon_sym_LPAREN,
      anon_sym_COLON_COLON,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(666), 4,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
      aux_sym_int_literal_token1,
  [10866] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(252), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10880] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(248), 8,
      anon_sym_RBRACE,
      anon_sym_let,
      anon_sym_reg,
      anon_sym_set,
      anon_sym_decl,
      anon_sym_SQUOTE,
      anon_sym_DOLLARif,
      anon_sym_assert,
  [10894] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    ACTIONS(690), 1,
      sym_identifier,
    STATE(435), 1,
      sym_int_literal,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
    ACTIONS(688), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
  [10915] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(692), 1,
      anon_sym_RPAREN,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [10938] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(694), 1,
      anon_sym_RBRACE,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [10961] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(696), 1,
      anon_sym_RBRACE,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [10984] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(11), 1,
      anon_sym_pipeline,
    ACTIONS(698), 1,
      anon_sym_RBRACE,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(9), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(242), 2,
      sym_unit_definition,
      aux_sym_impl_repeat1,
  [11005] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(700), 1,
      anon_sym_RBRACE,
    STATE(326), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11028] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(702), 1,
      anon_sym_RPAREN,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11051] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(704), 1,
      anon_sym_RPAREN,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11074] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(11), 1,
      anon_sym_pipeline,
    ACTIONS(706), 1,
      anon_sym_RBRACE,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(9), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(244), 2,
      sym_unit_definition,
      aux_sym_impl_repeat1,
  [11095] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(708), 1,
      anon_sym_RPAREN,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11118] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(713), 1,
      anon_sym_pipeline,
    ACTIONS(716), 1,
      anon_sym_RBRACE,
    STATE(444), 1,
      sym__pipeline_start,
    ACTIONS(710), 2,
      anon_sym_fn,
      anon_sym_entity,
    STATE(244), 2,
      sym_unit_definition,
      aux_sym_impl_repeat1,
  [11139] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(708), 1,
      anon_sym_RPAREN,
    STATE(340), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11162] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(720), 1,
      anon_sym_EQ,
    ACTIONS(718), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11176] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(724), 1,
      anon_sym_EQ,
    ACTIONS(722), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11190] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    STATE(319), 1,
      sym_typed_parameter,
    STATE(396), 1,
      sym_parameter,
    STATE(272), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11210] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(728), 1,
      anon_sym_EQ,
    ACTIONS(726), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11224] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(732), 1,
      anon_sym_EQ,
    ACTIONS(730), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11238] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(734), 1,
      anon_sym_DASH_GT,
    ACTIONS(736), 1,
      anon_sym___builtin__,
    STATE(182), 3,
      sym__body_or_builtin,
      sym_block,
      sym_builtin_marker,
  [11256] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(740), 1,
      anon_sym_EQ,
    ACTIONS(738), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11270] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(744), 1,
      anon_sym_EQ,
    ACTIONS(742), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11284] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(748), 1,
      anon_sym_EQ,
    ACTIONS(746), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11298] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(752), 1,
      anon_sym_EQ,
    ACTIONS(750), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11312] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(756), 1,
      anon_sym_EQ,
    ACTIONS(754), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11326] = 7,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(758), 1,
      anon_sym_LPAREN,
    ACTIONS(760), 1,
      sym_identifier,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(271), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
  [11348] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(736), 1,
      anon_sym___builtin__,
    ACTIONS(762), 1,
      anon_sym_DASH_GT,
    STATE(190), 3,
      sym__body_or_builtin,
      sym_block,
      sym_builtin_marker,
  [11366] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(766), 1,
      anon_sym_EQ,
    ACTIONS(764), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11380] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(770), 1,
      anon_sym_EQ,
    ACTIONS(768), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11394] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(774), 1,
      anon_sym_EQ,
    ACTIONS(772), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11408] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(778), 1,
      anon_sym_EQ,
    ACTIONS(776), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11422] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(782), 1,
      anon_sym_EQ,
    ACTIONS(780), 5,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
      anon_sym_reset,
      anon_sym_EQ_GT,
  [11436] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(186), 1,
      anon_sym_LPAREN,
    ACTIONS(190), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(124), 1,
      sym_argument_list,
    STATE(118), 2,
      sym__named_argument_list,
      sym__positional_argument_list,
  [11453] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(760), 1,
      sym_identifier,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(266), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
  [11472] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(186), 1,
      anon_sym_LPAREN,
    ACTIONS(190), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(103), 1,
      sym_argument_list,
    STATE(118), 2,
      sym__named_argument_list,
      sym__positional_argument_list,
  [11489] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(186), 1,
      anon_sym_LPAREN,
    ACTIONS(190), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(121), 1,
      sym_argument_list,
    STATE(118), 2,
      sym__named_argument_list,
      sym__positional_argument_list,
  [11506] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(736), 1,
      anon_sym___builtin__,
    STATE(183), 3,
      sym__body_or_builtin,
      sym_block,
      sym_builtin_marker,
  [11521] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(736), 1,
      anon_sym___builtin__,
    STATE(185), 3,
      sym__body_or_builtin,
      sym_block,
      sym_builtin_marker,
  [11536] = 6,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(760), 1,
      sym_identifier,
    STATE(215), 1,
      sym_scoped_identifier,
    STATE(419), 1,
      sym__scoped_or_raw_ident,
    STATE(420), 1,
      sym__path,
  [11555] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(186), 1,
      anon_sym_LPAREN,
    ACTIONS(190), 1,
      anon_sym_DOLLAR_LPAREN,
    STATE(98), 1,
      sym_argument_list,
    STATE(118), 2,
      sym__named_argument_list,
      sym__positional_argument_list,
  [11572] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(672), 1,
      anon_sym_POUND,
    ACTIONS(686), 1,
      sym_identifier,
    STATE(385), 1,
      sym_parameter,
    STATE(298), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11589] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(425), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11603] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(417), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11617] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(440), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11631] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(458), 1,
      aux_sym_int_literal_token1,
    STATE(414), 1,
      sym_int_literal,
    ACTIONS(460), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11645] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(784), 1,
      anon_sym_RPAREN,
    STATE(332), 1,
      sym_named_pattern_param,
    STATE(334), 1,
      sym_parameter,
  [11661] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(786), 1,
      anon_sym_LBRACE,
    STATE(365), 1,
      sym_braced_parameter_list,
    ACTIONS(788), 2,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [11675] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(790), 1,
      anon_sym_POUND,
    ACTIONS(792), 1,
      anon_sym_GT,
    ACTIONS(794), 1,
      sym_identifier,
    STATE(375), 1,
      sym__generic_param,
  [11691] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(796), 1,
      anon_sym_RPAREN,
    STATE(334), 1,
      sym_parameter,
    STATE(363), 1,
      sym_named_pattern_param,
  [11707] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(61), 1,
      anon_sym_COLON_COLON,
    ACTIONS(798), 1,
      sym_identifier,
    STATE(382), 1,
      sym_scoped_identifier,
    STATE(420), 1,
      sym__path,
  [11723] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(800), 1,
      anon_sym_LPAREN,
    ACTIONS(802), 1,
      anon_sym_LT,
    STATE(258), 1,
      sym_parameter_list,
    STATE(381), 1,
      sym_generic_parameters,
  [11739] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(804), 1,
      anon_sym_RPAREN,
    STATE(352), 1,
      sym_parameter,
    STATE(362), 1,
      sym__named_argument,
  [11755] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(806), 1,
      anon_sym_COMMA,
    STATE(284), 1,
      aux_sym_array_literal_repeat1,
    ACTIONS(160), 2,
      anon_sym_RBRACK,
      anon_sym_RPAREN,
  [11769] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(408), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11783] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(786), 1,
      anon_sym_LBRACE,
    ACTIONS(802), 1,
      anon_sym_LT,
    STATE(214), 1,
      sym_braced_parameter_list,
    STATE(377), 1,
      sym_generic_parameters,
  [11799] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(193), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11813] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(809), 1,
      anon_sym_EQ,
    ACTIONS(811), 1,
      anon_sym_COLON,
    ACTIONS(813), 1,
      anon_sym_reset,
    STATE(397), 1,
      sym_reg_reset,
  [11829] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(85), 4,
      anon_sym_COMMA,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_RPAREN_DOT,
  [11839] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(815), 1,
      anon_sym_RPAREN,
    STATE(334), 1,
      sym_parameter,
    STATE(363), 1,
      sym_named_pattern_param,
  [11855] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(819), 1,
      anon_sym_COMMA,
    STATE(291), 1,
      aux_sym_parameter_list_repeat1,
    ACTIONS(817), 2,
      anon_sym_RPAREN,
      anon_sym_RBRACE,
  [11869] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(802), 1,
      anon_sym_LT,
    ACTIONS(822), 1,
      anon_sym_LBRACE,
    STATE(217), 1,
      sym_enum_body,
    STATE(370), 1,
      sym_generic_parameters,
  [11885] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(824), 1,
      anon_sym_RPAREN,
    STATE(352), 1,
      sym_parameter,
    STATE(353), 1,
      sym__named_argument,
  [11901] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(790), 1,
      anon_sym_POUND,
    ACTIONS(794), 1,
      sym_identifier,
    ACTIONS(826), 1,
      anon_sym_GT,
    STATE(375), 1,
      sym__generic_param,
  [11917] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(63), 1,
      aux_sym_int_literal_token1,
    STATE(443), 1,
      sym_int_literal,
    ACTIONS(65), 2,
      aux_sym_int_literal_token2,
      aux_sym_int_literal_token3,
  [11931] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(786), 1,
      anon_sym_LBRACE,
    ACTIONS(802), 1,
      anon_sym_LT,
    STATE(205), 1,
      sym_braced_parameter_list,
    STATE(374), 1,
      sym_generic_parameters,
  [11947] = 5,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    ACTIONS(828), 1,
      anon_sym_RPAREN,
    STATE(352), 1,
      sym_parameter,
    STATE(362), 1,
      sym__named_argument,
  [11963] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(596), 1,
      sym_identifier,
    ACTIONS(830), 1,
      anon_sym_POUND,
    STATE(298), 2,
      sym_attribute,
      aux_sym__item_repeat1,
  [11977] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(833), 1,
      anon_sym_if,
    STATE(114), 2,
      sym_block,
      sym_if_expression,
  [11991] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(835), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12000] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(837), 1,
      anon_sym_RBRACE,
    ACTIONS(839), 1,
      anon_sym_COMMA,
    STATE(343), 1,
      aux_sym_enum_body_repeat1,
  [12013] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(841), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12022] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(682), 1,
      anon_sym_RPAREN,
    ACTIONS(843), 1,
      anon_sym_COMMA,
    STATE(329), 1,
      aux_sym_parameter_list_repeat1,
  [12035] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(845), 3,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_GT,
  [12044] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(847), 1,
      anon_sym_COMMA,
    ACTIONS(850), 1,
      anon_sym_SEMI,
    STATE(305), 1,
      aux_sym_decl_statement_repeat1,
  [12057] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(852), 1,
      anon_sym_RBRACE,
    ACTIONS(854), 1,
      anon_sym_COMMA,
    STATE(306), 1,
      aux_sym_enum_body_repeat1,
  [12070] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(857), 1,
      anon_sym_COMMA,
    ACTIONS(859), 1,
      anon_sym_SEMI,
    STATE(305), 1,
      aux_sym_decl_statement_repeat1,
  [12083] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(813), 1,
      anon_sym_reset,
    ACTIONS(861), 1,
      anon_sym_EQ,
    STATE(400), 1,
      sym_reg_reset,
  [12096] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(863), 1,
      anon_sym_RBRACE,
    ACTIONS(865), 1,
      sym_identifier,
    STATE(380), 1,
      sym_enum_member,
  [12109] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(790), 1,
      anon_sym_POUND,
    ACTIONS(867), 1,
      sym_identifier,
    STATE(311), 1,
      sym__generic_param,
  [12122] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(869), 1,
      anon_sym_COMMA,
    ACTIONS(871), 1,
      anon_sym_GT,
    STATE(354), 1,
      aux_sym_generic_parameters_repeat1,
  [12135] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    STATE(334), 1,
      sym_parameter,
    STATE(363), 1,
      sym_named_pattern_param,
  [12148] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(845), 1,
      anon_sym_RPAREN,
    ACTIONS(873), 1,
      anon_sym_COMMA,
    STATE(313), 1,
      aux_sym_tuple_type_repeat1,
  [12161] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(67), 1,
      anon_sym_RPAREN,
    ACTIONS(876), 1,
      anon_sym_COMMA,
    STATE(348), 1,
      aux_sym__positional_argument_list_repeat1,
  [12174] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(828), 1,
      anon_sym_RPAREN,
    ACTIONS(878), 1,
      anon_sym_COMMA,
    STATE(350), 1,
      aux_sym__named_argument_list_repeat1,
  [12187] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(880), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12196] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(882), 1,
      anon_sym_COMMA,
    ACTIONS(884), 1,
      anon_sym_SEMI,
    STATE(307), 1,
      aux_sym_decl_statement_repeat1,
  [12209] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(886), 3,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_COLON,
  [12218] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(817), 3,
      anon_sym_RPAREN,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [12227] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(692), 1,
      anon_sym_RPAREN,
    ACTIONS(888), 1,
      anon_sym_COMMA,
    STATE(291), 1,
      aux_sym_parameter_list_repeat1,
  [12240] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(890), 1,
      anon_sym_RPAREN,
    ACTIONS(892), 1,
      anon_sym_COMMA,
    STATE(355), 1,
      aux_sym_tuple_type_repeat1,
  [12253] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(845), 1,
      anon_sym_GT,
    ACTIONS(894), 1,
      anon_sym_COMMA,
    STATE(322), 1,
      aux_sym_tuple_type_repeat1,
  [12266] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(897), 1,
      anon_sym_COMMA,
    ACTIONS(900), 1,
      anon_sym_GT,
    STATE(323), 1,
      aux_sym_generic_parameters_repeat1,
  [12279] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(71), 1,
      anon_sym_RPAREN,
    ACTIONS(902), 1,
      anon_sym_COMMA,
    STATE(284), 1,
      aux_sym_array_literal_repeat1,
  [12292] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(686), 1,
      sym_identifier,
    STATE(352), 1,
      sym_parameter,
    STATE(362), 1,
      sym__named_argument,
  [12305] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(904), 1,
      anon_sym_RBRACE,
    ACTIONS(906), 1,
      anon_sym_COMMA,
    STATE(347), 1,
      aux_sym_parameter_list_repeat1,
  [12318] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(908), 1,
      anon_sym_RPAREN,
    ACTIONS(910), 1,
      anon_sym_COMMA,
    STATE(327), 1,
      aux_sym_tuple_pattern_repeat1,
  [12331] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(913), 1,
      anon_sym_RPAREN,
    ACTIONS(915), 1,
      anon_sym_COMMA,
    STATE(342), 1,
      aux_sym_tuple_pattern_repeat1,
  [12344] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(708), 1,
      anon_sym_RPAREN,
    ACTIONS(917), 1,
      anon_sym_COMMA,
    STATE(291), 1,
      aux_sym_parameter_list_repeat1,
  [12357] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(919), 3,
      anon_sym_RPAREN,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [12366] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(509), 1,
      anon_sym_RPAREN,
    ACTIONS(921), 1,
      anon_sym_COMMA,
    STATE(327), 1,
      aux_sym_tuple_pattern_repeat1,
  [12379] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(923), 1,
      anon_sym_RPAREN,
    ACTIONS(925), 1,
      anon_sym_COMMA,
    STATE(335), 1,
      aux_sym__named_pattern_list_repeat1,
  [12392] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(927), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12401] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(931), 1,
      anon_sym_COLON,
    ACTIONS(929), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12412] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(815), 1,
      anon_sym_RPAREN,
    ACTIONS(933), 1,
      anon_sym_COMMA,
    STATE(358), 1,
      aux_sym__named_pattern_list_repeat1,
  [12425] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(935), 1,
      anon_sym_RPAREN,
    ACTIONS(937), 1,
      anon_sym_COMMA,
    STATE(331), 1,
      aux_sym_tuple_pattern_repeat1,
  [12438] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(865), 1,
      sym_identifier,
    ACTIONS(939), 1,
      anon_sym_RBRACE,
    STATE(301), 1,
      sym_enum_member,
  [12451] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(708), 1,
      anon_sym_RPAREN,
    ACTIONS(917), 1,
      anon_sym_COMMA,
    STATE(339), 1,
      aux_sym_parameter_list_repeat1,
  [12464] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(704), 1,
      anon_sym_RPAREN,
    ACTIONS(941), 1,
      anon_sym_COMMA,
    STATE(291), 1,
      aux_sym_parameter_list_repeat1,
  [12477] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(704), 1,
      anon_sym_RPAREN,
    ACTIONS(941), 1,
      anon_sym_COMMA,
    STATE(320), 1,
      aux_sym_parameter_list_repeat1,
  [12490] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(943), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12499] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(537), 1,
      anon_sym_RPAREN,
    ACTIONS(945), 1,
      anon_sym_COMMA,
    STATE(327), 1,
      aux_sym_tuple_pattern_repeat1,
  [12512] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(947), 1,
      anon_sym_RBRACE,
    ACTIONS(949), 1,
      anon_sym_COMMA,
    STATE(306), 1,
      aux_sym_enum_body_repeat1,
  [12525] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(951), 3,
      anon_sym_RPAREN,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [12534] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(953), 3,
      anon_sym_DASH_GT,
      anon_sym_LBRACE,
      anon_sym___builtin__,
  [12543] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(865), 1,
      sym_identifier,
    ACTIONS(947), 1,
      anon_sym_RBRACE,
    STATE(380), 1,
      sym_enum_member,
  [12556] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(694), 1,
      anon_sym_RBRACE,
    ACTIONS(955), 1,
      anon_sym_COMMA,
    STATE(291), 1,
      aux_sym_parameter_list_repeat1,
  [12569] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(178), 1,
      anon_sym_RPAREN,
    ACTIONS(957), 1,
      anon_sym_COMMA,
    STATE(348), 1,
      aux_sym__positional_argument_list_repeat1,
  [12582] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(790), 1,
      anon_sym_POUND,
    ACTIONS(794), 1,
      sym_identifier,
    STATE(375), 1,
      sym__generic_param,
  [12595] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(960), 1,
      anon_sym_RPAREN,
    ACTIONS(962), 1,
      anon_sym_COMMA,
    STATE(350), 1,
      aux_sym__named_argument_list_repeat1,
  [12608] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(965), 1,
      anon_sym_LPAREN,
    ACTIONS(967), 1,
      anon_sym_SEMI,
    ACTIONS(969), 1,
      anon_sym_STAR,
  [12621] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(973), 1,
      anon_sym_COLON,
    ACTIONS(971), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12632] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(975), 1,
      anon_sym_RPAREN,
    ACTIONS(977), 1,
      anon_sym_COMMA,
    STATE(315), 1,
      aux_sym__named_argument_list_repeat1,
  [12645] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(792), 1,
      anon_sym_GT,
    ACTIONS(979), 1,
      anon_sym_COMMA,
    STATE(323), 1,
      aux_sym_generic_parameters_repeat1,
  [12658] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(446), 1,
      anon_sym_RPAREN,
    ACTIONS(981), 1,
      anon_sym_COMMA,
    STATE(313), 1,
      aux_sym_tuple_type_repeat1,
  [12671] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(983), 1,
      anon_sym_COMMA,
    ACTIONS(985), 1,
      anon_sym_GT,
    STATE(359), 1,
      aux_sym_tuple_type_repeat1,
  [12684] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(83), 1,
      anon_sym_RBRACK,
    ACTIONS(987), 1,
      anon_sym_COMMA,
    STATE(284), 1,
      aux_sym_array_literal_repeat1,
  [12697] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(989), 1,
      anon_sym_RPAREN,
    ACTIONS(991), 1,
      anon_sym_COMMA,
    STATE(358), 1,
      aux_sym__named_pattern_list_repeat1,
  [12710] = 4,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(464), 1,
      anon_sym_GT,
    ACTIONS(994), 1,
      anon_sym_COMMA,
    STATE(322), 1,
      aux_sym_tuple_type_repeat1,
  [12723] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(996), 1,
      anon_sym_EQ,
    ACTIONS(998), 1,
      anon_sym_COLON,
  [12733] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1000), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12741] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(960), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12749] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(989), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12757] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(859), 1,
      anon_sym_SEMI,
    ACTIONS(1002), 1,
      sym_identifier,
  [12767] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1004), 2,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [12775] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1006), 2,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
  [12783] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1008), 2,
      anon_sym_COMMA,
      anon_sym_GT,
  [12791] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1010), 2,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
  [12799] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(850), 2,
      anon_sym_COMMA,
      anon_sym_SEMI,
  [12807] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(822), 1,
      anon_sym_LBRACE,
    STATE(220), 1,
      sym_enum_body,
  [12817] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(865), 1,
      sym_identifier,
    STATE(380), 1,
      sym_enum_member,
  [12827] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(668), 2,
      anon_sym_POUND,
      sym_identifier,
  [12835] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1012), 2,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
  [12843] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(786), 1,
      anon_sym_LBRACE,
    STATE(204), 1,
      sym_braced_parameter_list,
  [12853] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(900), 2,
      anon_sym_COMMA,
      anon_sym_GT,
  [12861] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(908), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [12869] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(786), 1,
      anon_sym_LBRACE,
    STATE(205), 1,
      sym_braced_parameter_list,
  [12879] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1014), 1,
      anon_sym_port,
    ACTIONS(1016), 1,
      sym_identifier,
  [12889] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1018), 1,
      anon_sym_inst,
    ACTIONS(1020), 1,
      sym_identifier,
  [12899] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(852), 2,
      anon_sym_RBRACE,
      anon_sym_COMMA,
  [12907] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(800), 1,
      anon_sym_LPAREN,
    STATE(251), 1,
      sym_parameter_list,
  [12917] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(216), 1,
      anon_sym_COLON_COLON,
    ACTIONS(1022), 1,
      anon_sym_SEMI,
  [12927] = 3,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1002), 1,
      sym_identifier,
    ACTIONS(1024), 1,
      anon_sym_SEMI,
  [12937] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1026), 1,
      anon_sym_RBRACE,
  [12944] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1028), 1,
      anon_sym_COLON,
  [12951] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1030), 1,
      sym_identifier,
  [12958] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1032), 1,
      sym_identifier,
  [12965] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1034), 1,
      anon_sym_LPAREN,
  [12972] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1036), 1,
      anon_sym_EQ_GT,
  [12979] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1038), 1,
      anon_sym_SEMI,
  [12986] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1040), 1,
      sym_identifier,
  [12993] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1042), 1,
      sym_identifier,
  [13000] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1044), 1,
      sym_identifier,
  [13007] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1046), 1,
      anon_sym_LBRACE,
  [13014] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1048), 1,
      anon_sym_LPAREN,
  [13021] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1050), 1,
      anon_sym_COLON,
  [13028] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1052), 1,
      anon_sym_EQ,
  [13035] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1054), 1,
      sym_identifier,
  [13042] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1056), 1,
      anon_sym_else,
  [13049] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1058), 1,
      anon_sym_EQ,
  [13056] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1060), 1,
      anon_sym_COLON,
  [13063] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1062), 1,
      sym_identifier,
  [13070] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1064), 1,
      sym_identifier,
  [13077] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1066), 1,
      anon_sym_RBRACE,
  [13084] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1068), 1,
      anon_sym_LBRACK,
  [13091] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1002), 1,
      sym_identifier,
  [13098] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1070), 1,
      anon_sym_SEMI,
  [13105] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1072), 1,
      anon_sym_RPAREN,
  [13112] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1074), 1,
      sym_identifier,
  [13119] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1076), 1,
      sym_identifier,
  [13126] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1078), 1,
      sym_identifier,
  [13133] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1080), 1,
      anon_sym_RBRACK,
  [13140] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1082), 1,
      anon_sym_EQ,
  [13147] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1084), 1,
      anon_sym_RPAREN_DOT,
  [13154] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1086), 1,
      anon_sym_EQ,
  [13161] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1088), 1,
      anon_sym_EQ_GT,
  [13168] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1090), 1,
      anon_sym_LBRACE,
  [13175] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1092), 1,
      anon_sym_EQ,
  [13182] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1094), 1,
      anon_sym_LBRACE,
  [13189] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1096), 1,
      anon_sym_COLON_COLON,
  [13196] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1098), 1,
      sym_identifier,
  [13203] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1100), 1,
      anon_sym_RBRACK,
  [13210] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1102), 1,
      anon_sym_RPAREN,
  [13217] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1104), 1,
      sym_identifier,
  [13224] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1106), 1,
      anon_sym_SEMI,
  [13231] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1108), 1,
      sym_identifier,
  [13238] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1110), 1,
      sym_identifier,
  [13245] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1112), 1,
      ts_builtin_sym_end,
  [13252] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1114), 1,
      sym_identifier,
  [13259] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1116), 1,
      sym_identifier,
  [13266] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1118), 1,
      anon_sym_RBRACK,
  [13273] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1120), 1,
      sym_identifier,
  [13280] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1122), 1,
      anon_sym_SEMI,
  [13287] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1124), 1,
      anon_sym_BQUOTE,
  [13294] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1126), 1,
      anon_sym_RPAREN_DOT,
  [13301] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1128), 1,
      sym_identifier,
  [13308] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1130), 1,
      anon_sym_RPAREN_DOT,
  [13315] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1132), 1,
      anon_sym_LPAREN,
  [13322] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1134), 1,
      sym_identifier,
  [13329] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1136), 1,
      anon_sym_LBRACE,
  [13336] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1138), 1,
      anon_sym_LBRACE,
  [13343] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1140), 1,
      anon_sym_LBRACK,
  [13350] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1142), 1,
      anon_sym_RPAREN,
  [13357] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1144), 1,
      sym_identifier,
  [13364] = 2,
    ACTIONS(3), 1,
      sym_line_comment,
    ACTIONS(1146), 1,
      sym_identifier,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 122,
  [SMALL_STATE(4)] = 244,
  [SMALL_STATE(5)] = 334,
  [SMALL_STATE(6)] = 424,
  [SMALL_STATE(7)] = 514,
  [SMALL_STATE(8)] = 604,
  [SMALL_STATE(9)] = 694,
  [SMALL_STATE(10)] = 784,
  [SMALL_STATE(11)] = 874,
  [SMALL_STATE(12)] = 964,
  [SMALL_STATE(13)] = 1054,
  [SMALL_STATE(14)] = 1141,
  [SMALL_STATE(15)] = 1228,
  [SMALL_STATE(16)] = 1315,
  [SMALL_STATE(17)] = 1402,
  [SMALL_STATE(18)] = 1489,
  [SMALL_STATE(19)] = 1576,
  [SMALL_STATE(20)] = 1663,
  [SMALL_STATE(21)] = 1750,
  [SMALL_STATE(22)] = 1837,
  [SMALL_STATE(23)] = 1924,
  [SMALL_STATE(24)] = 2011,
  [SMALL_STATE(25)] = 2098,
  [SMALL_STATE(26)] = 2185,
  [SMALL_STATE(27)] = 2272,
  [SMALL_STATE(28)] = 2359,
  [SMALL_STATE(29)] = 2446,
  [SMALL_STATE(30)] = 2533,
  [SMALL_STATE(31)] = 2620,
  [SMALL_STATE(32)] = 2707,
  [SMALL_STATE(33)] = 2794,
  [SMALL_STATE(34)] = 2881,
  [SMALL_STATE(35)] = 2968,
  [SMALL_STATE(36)] = 3055,
  [SMALL_STATE(37)] = 3142,
  [SMALL_STATE(38)] = 3229,
  [SMALL_STATE(39)] = 3316,
  [SMALL_STATE(40)] = 3403,
  [SMALL_STATE(41)] = 3490,
  [SMALL_STATE(42)] = 3577,
  [SMALL_STATE(43)] = 3664,
  [SMALL_STATE(44)] = 3710,
  [SMALL_STATE(45)] = 3771,
  [SMALL_STATE(46)] = 3814,
  [SMALL_STATE(47)] = 3857,
  [SMALL_STATE(48)] = 3932,
  [SMALL_STATE(49)] = 4009,
  [SMALL_STATE(50)] = 4078,
  [SMALL_STATE(51)] = 4151,
  [SMALL_STATE(52)] = 4214,
  [SMALL_STATE(53)] = 4273,
  [SMALL_STATE(54)] = 4352,
  [SMALL_STATE(55)] = 4405,
  [SMALL_STATE(56)] = 4460,
  [SMALL_STATE(57)] = 4513,
  [SMALL_STATE(58)] = 4574,
  [SMALL_STATE(59)] = 4645,
  [SMALL_STATE(60)] = 4724,
  [SMALL_STATE(61)] = 4799,
  [SMALL_STATE(62)] = 4878,
  [SMALL_STATE(63)] = 4957,
  [SMALL_STATE(64)] = 5033,
  [SMALL_STATE(65)] = 5109,
  [SMALL_STATE(66)] = 5183,
  [SMALL_STATE(67)] = 5257,
  [SMALL_STATE(68)] = 5333,
  [SMALL_STATE(69)] = 5377,
  [SMALL_STATE(70)] = 5453,
  [SMALL_STATE(71)] = 5492,
  [SMALL_STATE(72)] = 5565,
  [SMALL_STATE(73)] = 5638,
  [SMALL_STATE(74)] = 5711,
  [SMALL_STATE(75)] = 5784,
  [SMALL_STATE(76)] = 5819,
  [SMALL_STATE(77)] = 5858,
  [SMALL_STATE(78)] = 5931,
  [SMALL_STATE(79)] = 5970,
  [SMALL_STATE(80)] = 6043,
  [SMALL_STATE(81)] = 6116,
  [SMALL_STATE(82)] = 6189,
  [SMALL_STATE(83)] = 6262,
  [SMALL_STATE(84)] = 6335,
  [SMALL_STATE(85)] = 6408,
  [SMALL_STATE(86)] = 6481,
  [SMALL_STATE(87)] = 6554,
  [SMALL_STATE(88)] = 6587,
  [SMALL_STATE(89)] = 6620,
  [SMALL_STATE(90)] = 6653,
  [SMALL_STATE(91)] = 6686,
  [SMALL_STATE(92)] = 6719,
  [SMALL_STATE(93)] = 6752,
  [SMALL_STATE(94)] = 6784,
  [SMALL_STATE(95)] = 6816,
  [SMALL_STATE(96)] = 6848,
  [SMALL_STATE(97)] = 6880,
  [SMALL_STATE(98)] = 6912,
  [SMALL_STATE(99)] = 6944,
  [SMALL_STATE(100)] = 6976,
  [SMALL_STATE(101)] = 7008,
  [SMALL_STATE(102)] = 7040,
  [SMALL_STATE(103)] = 7072,
  [SMALL_STATE(104)] = 7104,
  [SMALL_STATE(105)] = 7136,
  [SMALL_STATE(106)] = 7168,
  [SMALL_STATE(107)] = 7200,
  [SMALL_STATE(108)] = 7232,
  [SMALL_STATE(109)] = 7264,
  [SMALL_STATE(110)] = 7296,
  [SMALL_STATE(111)] = 7328,
  [SMALL_STATE(112)] = 7360,
  [SMALL_STATE(113)] = 7392,
  [SMALL_STATE(114)] = 7426,
  [SMALL_STATE(115)] = 7458,
  [SMALL_STATE(116)] = 7490,
  [SMALL_STATE(117)] = 7522,
  [SMALL_STATE(118)] = 7554,
  [SMALL_STATE(119)] = 7586,
  [SMALL_STATE(120)] = 7618,
  [SMALL_STATE(121)] = 7650,
  [SMALL_STATE(122)] = 7682,
  [SMALL_STATE(123)] = 7714,
  [SMALL_STATE(124)] = 7745,
  [SMALL_STATE(125)] = 7776,
  [SMALL_STATE(126)] = 7825,
  [SMALL_STATE(127)] = 7874,
  [SMALL_STATE(128)] = 7925,
  [SMALL_STATE(129)] = 7973,
  [SMALL_STATE(130)] = 8018,
  [SMALL_STATE(131)] = 8063,
  [SMALL_STATE(132)] = 8108,
  [SMALL_STATE(133)] = 8153,
  [SMALL_STATE(134)] = 8198,
  [SMALL_STATE(135)] = 8243,
  [SMALL_STATE(136)] = 8288,
  [SMALL_STATE(137)] = 8333,
  [SMALL_STATE(138)] = 8375,
  [SMALL_STATE(139)] = 8417,
  [SMALL_STATE(140)] = 8459,
  [SMALL_STATE(141)] = 8503,
  [SMALL_STATE(142)] = 8545,
  [SMALL_STATE(143)] = 8587,
  [SMALL_STATE(144)] = 8631,
  [SMALL_STATE(145)] = 8673,
  [SMALL_STATE(146)] = 8715,
  [SMALL_STATE(147)] = 8757,
  [SMALL_STATE(148)] = 8799,
  [SMALL_STATE(149)] = 8841,
  [SMALL_STATE(150)] = 8885,
  [SMALL_STATE(151)] = 8927,
  [SMALL_STATE(152)] = 8971,
  [SMALL_STATE(153)] = 9013,
  [SMALL_STATE(154)] = 9055,
  [SMALL_STATE(155)] = 9099,
  [SMALL_STATE(156)] = 9141,
  [SMALL_STATE(157)] = 9183,
  [SMALL_STATE(158)] = 9225,
  [SMALL_STATE(159)] = 9269,
  [SMALL_STATE(160)] = 9311,
  [SMALL_STATE(161)] = 9353,
  [SMALL_STATE(162)] = 9395,
  [SMALL_STATE(163)] = 9421,
  [SMALL_STATE(164)] = 9463,
  [SMALL_STATE(165)] = 9505,
  [SMALL_STATE(166)] = 9549,
  [SMALL_STATE(167)] = 9591,
  [SMALL_STATE(168)] = 9617,
  [SMALL_STATE(169)] = 9643,
  [SMALL_STATE(170)] = 9669,
  [SMALL_STATE(171)] = 9695,
  [SMALL_STATE(172)] = 9721,
  [SMALL_STATE(173)] = 9747,
  [SMALL_STATE(174)] = 9788,
  [SMALL_STATE(175)] = 9829,
  [SMALL_STATE(176)] = 9870,
  [SMALL_STATE(177)] = 9911,
  [SMALL_STATE(178)] = 9929,
  [SMALL_STATE(179)] = 9947,
  [SMALL_STATE(180)] = 9969,
  [SMALL_STATE(181)] = 9987,
  [SMALL_STATE(182)] = 10005,
  [SMALL_STATE(183)] = 10022,
  [SMALL_STATE(184)] = 10039,
  [SMALL_STATE(185)] = 10058,
  [SMALL_STATE(186)] = 10075,
  [SMALL_STATE(187)] = 10092,
  [SMALL_STATE(188)] = 10111,
  [SMALL_STATE(189)] = 10128,
  [SMALL_STATE(190)] = 10149,
  [SMALL_STATE(191)] = 10166,
  [SMALL_STATE(192)] = 10182,
  [SMALL_STATE(193)] = 10198,
  [SMALL_STATE(194)] = 10214,
  [SMALL_STATE(195)] = 10230,
  [SMALL_STATE(196)] = 10246,
  [SMALL_STATE(197)] = 10262,
  [SMALL_STATE(198)] = 10282,
  [SMALL_STATE(199)] = 10298,
  [SMALL_STATE(200)] = 10314,
  [SMALL_STATE(201)] = 10330,
  [SMALL_STATE(202)] = 10346,
  [SMALL_STATE(203)] = 10362,
  [SMALL_STATE(204)] = 10378,
  [SMALL_STATE(205)] = 10394,
  [SMALL_STATE(206)] = 10410,
  [SMALL_STATE(207)] = 10426,
  [SMALL_STATE(208)] = 10446,
  [SMALL_STATE(209)] = 10472,
  [SMALL_STATE(210)] = 10488,
  [SMALL_STATE(211)] = 10504,
  [SMALL_STATE(212)] = 10520,
  [SMALL_STATE(213)] = 10536,
  [SMALL_STATE(214)] = 10552,
  [SMALL_STATE(215)] = 10568,
  [SMALL_STATE(216)] = 10588,
  [SMALL_STATE(217)] = 10604,
  [SMALL_STATE(218)] = 10620,
  [SMALL_STATE(219)] = 10636,
  [SMALL_STATE(220)] = 10652,
  [SMALL_STATE(221)] = 10668,
  [SMALL_STATE(222)] = 10687,
  [SMALL_STATE(223)] = 10702,
  [SMALL_STATE(224)] = 10722,
  [SMALL_STATE(225)] = 10736,
  [SMALL_STATE(226)] = 10762,
  [SMALL_STATE(227)] = 10776,
  [SMALL_STATE(228)] = 10796,
  [SMALL_STATE(229)] = 10810,
  [SMALL_STATE(230)] = 10824,
  [SMALL_STATE(231)] = 10850,
  [SMALL_STATE(232)] = 10866,
  [SMALL_STATE(233)] = 10880,
  [SMALL_STATE(234)] = 10894,
  [SMALL_STATE(235)] = 10915,
  [SMALL_STATE(236)] = 10938,
  [SMALL_STATE(237)] = 10961,
  [SMALL_STATE(238)] = 10984,
  [SMALL_STATE(239)] = 11005,
  [SMALL_STATE(240)] = 11028,
  [SMALL_STATE(241)] = 11051,
  [SMALL_STATE(242)] = 11074,
  [SMALL_STATE(243)] = 11095,
  [SMALL_STATE(244)] = 11118,
  [SMALL_STATE(245)] = 11139,
  [SMALL_STATE(246)] = 11162,
  [SMALL_STATE(247)] = 11176,
  [SMALL_STATE(248)] = 11190,
  [SMALL_STATE(249)] = 11210,
  [SMALL_STATE(250)] = 11224,
  [SMALL_STATE(251)] = 11238,
  [SMALL_STATE(252)] = 11256,
  [SMALL_STATE(253)] = 11270,
  [SMALL_STATE(254)] = 11284,
  [SMALL_STATE(255)] = 11298,
  [SMALL_STATE(256)] = 11312,
  [SMALL_STATE(257)] = 11326,
  [SMALL_STATE(258)] = 11348,
  [SMALL_STATE(259)] = 11366,
  [SMALL_STATE(260)] = 11380,
  [SMALL_STATE(261)] = 11394,
  [SMALL_STATE(262)] = 11408,
  [SMALL_STATE(263)] = 11422,
  [SMALL_STATE(264)] = 11436,
  [SMALL_STATE(265)] = 11453,
  [SMALL_STATE(266)] = 11472,
  [SMALL_STATE(267)] = 11489,
  [SMALL_STATE(268)] = 11506,
  [SMALL_STATE(269)] = 11521,
  [SMALL_STATE(270)] = 11536,
  [SMALL_STATE(271)] = 11555,
  [SMALL_STATE(272)] = 11572,
  [SMALL_STATE(273)] = 11589,
  [SMALL_STATE(274)] = 11603,
  [SMALL_STATE(275)] = 11617,
  [SMALL_STATE(276)] = 11631,
  [SMALL_STATE(277)] = 11645,
  [SMALL_STATE(278)] = 11661,
  [SMALL_STATE(279)] = 11675,
  [SMALL_STATE(280)] = 11691,
  [SMALL_STATE(281)] = 11707,
  [SMALL_STATE(282)] = 11723,
  [SMALL_STATE(283)] = 11739,
  [SMALL_STATE(284)] = 11755,
  [SMALL_STATE(285)] = 11769,
  [SMALL_STATE(286)] = 11783,
  [SMALL_STATE(287)] = 11799,
  [SMALL_STATE(288)] = 11813,
  [SMALL_STATE(289)] = 11829,
  [SMALL_STATE(290)] = 11839,
  [SMALL_STATE(291)] = 11855,
  [SMALL_STATE(292)] = 11869,
  [SMALL_STATE(293)] = 11885,
  [SMALL_STATE(294)] = 11901,
  [SMALL_STATE(295)] = 11917,
  [SMALL_STATE(296)] = 11931,
  [SMALL_STATE(297)] = 11947,
  [SMALL_STATE(298)] = 11963,
  [SMALL_STATE(299)] = 11977,
  [SMALL_STATE(300)] = 11991,
  [SMALL_STATE(301)] = 12000,
  [SMALL_STATE(302)] = 12013,
  [SMALL_STATE(303)] = 12022,
  [SMALL_STATE(304)] = 12035,
  [SMALL_STATE(305)] = 12044,
  [SMALL_STATE(306)] = 12057,
  [SMALL_STATE(307)] = 12070,
  [SMALL_STATE(308)] = 12083,
  [SMALL_STATE(309)] = 12096,
  [SMALL_STATE(310)] = 12109,
  [SMALL_STATE(311)] = 12122,
  [SMALL_STATE(312)] = 12135,
  [SMALL_STATE(313)] = 12148,
  [SMALL_STATE(314)] = 12161,
  [SMALL_STATE(315)] = 12174,
  [SMALL_STATE(316)] = 12187,
  [SMALL_STATE(317)] = 12196,
  [SMALL_STATE(318)] = 12209,
  [SMALL_STATE(319)] = 12218,
  [SMALL_STATE(320)] = 12227,
  [SMALL_STATE(321)] = 12240,
  [SMALL_STATE(322)] = 12253,
  [SMALL_STATE(323)] = 12266,
  [SMALL_STATE(324)] = 12279,
  [SMALL_STATE(325)] = 12292,
  [SMALL_STATE(326)] = 12305,
  [SMALL_STATE(327)] = 12318,
  [SMALL_STATE(328)] = 12331,
  [SMALL_STATE(329)] = 12344,
  [SMALL_STATE(330)] = 12357,
  [SMALL_STATE(331)] = 12366,
  [SMALL_STATE(332)] = 12379,
  [SMALL_STATE(333)] = 12392,
  [SMALL_STATE(334)] = 12401,
  [SMALL_STATE(335)] = 12412,
  [SMALL_STATE(336)] = 12425,
  [SMALL_STATE(337)] = 12438,
  [SMALL_STATE(338)] = 12451,
  [SMALL_STATE(339)] = 12464,
  [SMALL_STATE(340)] = 12477,
  [SMALL_STATE(341)] = 12490,
  [SMALL_STATE(342)] = 12499,
  [SMALL_STATE(343)] = 12512,
  [SMALL_STATE(344)] = 12525,
  [SMALL_STATE(345)] = 12534,
  [SMALL_STATE(346)] = 12543,
  [SMALL_STATE(347)] = 12556,
  [SMALL_STATE(348)] = 12569,
  [SMALL_STATE(349)] = 12582,
  [SMALL_STATE(350)] = 12595,
  [SMALL_STATE(351)] = 12608,
  [SMALL_STATE(352)] = 12621,
  [SMALL_STATE(353)] = 12632,
  [SMALL_STATE(354)] = 12645,
  [SMALL_STATE(355)] = 12658,
  [SMALL_STATE(356)] = 12671,
  [SMALL_STATE(357)] = 12684,
  [SMALL_STATE(358)] = 12697,
  [SMALL_STATE(359)] = 12710,
  [SMALL_STATE(360)] = 12723,
  [SMALL_STATE(361)] = 12733,
  [SMALL_STATE(362)] = 12741,
  [SMALL_STATE(363)] = 12749,
  [SMALL_STATE(364)] = 12757,
  [SMALL_STATE(365)] = 12767,
  [SMALL_STATE(366)] = 12775,
  [SMALL_STATE(367)] = 12783,
  [SMALL_STATE(368)] = 12791,
  [SMALL_STATE(369)] = 12799,
  [SMALL_STATE(370)] = 12807,
  [SMALL_STATE(371)] = 12817,
  [SMALL_STATE(372)] = 12827,
  [SMALL_STATE(373)] = 12835,
  [SMALL_STATE(374)] = 12843,
  [SMALL_STATE(375)] = 12853,
  [SMALL_STATE(376)] = 12861,
  [SMALL_STATE(377)] = 12869,
  [SMALL_STATE(378)] = 12879,
  [SMALL_STATE(379)] = 12889,
  [SMALL_STATE(380)] = 12899,
  [SMALL_STATE(381)] = 12907,
  [SMALL_STATE(382)] = 12917,
  [SMALL_STATE(383)] = 12927,
  [SMALL_STATE(384)] = 12937,
  [SMALL_STATE(385)] = 12944,
  [SMALL_STATE(386)] = 12951,
  [SMALL_STATE(387)] = 12958,
  [SMALL_STATE(388)] = 12965,
  [SMALL_STATE(389)] = 12972,
  [SMALL_STATE(390)] = 12979,
  [SMALL_STATE(391)] = 12986,
  [SMALL_STATE(392)] = 12993,
  [SMALL_STATE(393)] = 13000,
  [SMALL_STATE(394)] = 13007,
  [SMALL_STATE(395)] = 13014,
  [SMALL_STATE(396)] = 13021,
  [SMALL_STATE(397)] = 13028,
  [SMALL_STATE(398)] = 13035,
  [SMALL_STATE(399)] = 13042,
  [SMALL_STATE(400)] = 13049,
  [SMALL_STATE(401)] = 13056,
  [SMALL_STATE(402)] = 13063,
  [SMALL_STATE(403)] = 13070,
  [SMALL_STATE(404)] = 13077,
  [SMALL_STATE(405)] = 13084,
  [SMALL_STATE(406)] = 13091,
  [SMALL_STATE(407)] = 13098,
  [SMALL_STATE(408)] = 13105,
  [SMALL_STATE(409)] = 13112,
  [SMALL_STATE(410)] = 13119,
  [SMALL_STATE(411)] = 13126,
  [SMALL_STATE(412)] = 13133,
  [SMALL_STATE(413)] = 13140,
  [SMALL_STATE(414)] = 13147,
  [SMALL_STATE(415)] = 13154,
  [SMALL_STATE(416)] = 13161,
  [SMALL_STATE(417)] = 13168,
  [SMALL_STATE(418)] = 13175,
  [SMALL_STATE(419)] = 13182,
  [SMALL_STATE(420)] = 13189,
  [SMALL_STATE(421)] = 13196,
  [SMALL_STATE(422)] = 13203,
  [SMALL_STATE(423)] = 13210,
  [SMALL_STATE(424)] = 13217,
  [SMALL_STATE(425)] = 13224,
  [SMALL_STATE(426)] = 13231,
  [SMALL_STATE(427)] = 13238,
  [SMALL_STATE(428)] = 13245,
  [SMALL_STATE(429)] = 13252,
  [SMALL_STATE(430)] = 13259,
  [SMALL_STATE(431)] = 13266,
  [SMALL_STATE(432)] = 13273,
  [SMALL_STATE(433)] = 13280,
  [SMALL_STATE(434)] = 13287,
  [SMALL_STATE(435)] = 13294,
  [SMALL_STATE(436)] = 13301,
  [SMALL_STATE(437)] = 13308,
  [SMALL_STATE(438)] = 13315,
  [SMALL_STATE(439)] = 13322,
  [SMALL_STATE(440)] = 13329,
  [SMALL_STATE(441)] = 13336,
  [SMALL_STATE(442)] = 13343,
  [SMALL_STATE(443)] = 13350,
  [SMALL_STATE(444)] = 13357,
  [SMALL_STATE(445)] = 13364,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(405),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(444),
  [11] = {.entry = {.count = 1, .reusable = true}}, SHIFT(438),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(378),
  [15] = {.entry = {.count = 1, .reusable = true}}, SHIFT(436),
  [17] = {.entry = {.count = 1, .reusable = true}}, SHIFT(270),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(281),
  [21] = {.entry = {.count = 1, .reusable = true}}, SHIFT(429),
  [23] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [25] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [27] = {.entry = {.count = 1, .reusable = true}}, SHIFT(3),
  [29] = {.entry = {.count = 1, .reusable = false}}, SHIFT(174),
  [31] = {.entry = {.count = 1, .reusable = false}}, SHIFT(351),
  [33] = {.entry = {.count = 1, .reusable = false}}, SHIFT(39),
  [35] = {.entry = {.count = 1, .reusable = false}}, SHIFT(387),
  [37] = {.entry = {.count = 1, .reusable = true}}, SHIFT(35),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(386),
  [41] = {.entry = {.count = 1, .reusable = true}}, SHIFT(391),
  [43] = {.entry = {.count = 1, .reusable = false}}, SHIFT(13),
  [45] = {.entry = {.count = 1, .reusable = true}}, SHIFT(162),
  [47] = {.entry = {.count = 1, .reusable = false}}, SHIFT(27),
  [49] = {.entry = {.count = 1, .reusable = false}}, SHIFT(25),
  [51] = {.entry = {.count = 1, .reusable = false}}, SHIFT(257),
  [53] = {.entry = {.count = 1, .reusable = false}}, SHIFT(388),
  [55] = {.entry = {.count = 1, .reusable = false}}, SHIFT(75),
  [57] = {.entry = {.count = 1, .reusable = false}}, SHIFT(113),
  [59] = {.entry = {.count = 1, .reusable = false}}, SHIFT(76),
  [61] = {.entry = {.count = 1, .reusable = true}}, SHIFT(421),
  [63] = {.entry = {.count = 1, .reusable = false}}, SHIFT(43),
  [65] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
  [67] = {.entry = {.count = 1, .reusable = true}}, SHIFT(112),
  [69] = {.entry = {.count = 1, .reusable = true}}, SHIFT(96),
  [71] = {.entry = {.count = 1, .reusable = true}}, SHIFT(94),
  [73] = {.entry = {.count = 1, .reusable = true}}, SHIFT(109),
  [75] = {.entry = {.count = 1, .reusable = true}}, SHIFT(101),
  [77] = {.entry = {.count = 1, .reusable = true}}, SHIFT(111),
  [79] = {.entry = {.count = 1, .reusable = true}}, SHIFT(104),
  [81] = {.entry = {.count = 1, .reusable = true}}, SHIFT(116),
  [83] = {.entry = {.count = 1, .reusable = true}}, SHIFT(107),
  [85] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_int_literal, 1),
  [87] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_int_literal, 1),
  [89] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2),
  [91] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(174),
  [94] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(351),
  [97] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(39),
  [100] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(387),
  [103] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(386),
  [106] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(391),
  [109] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(13),
  [112] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2),
  [114] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 3),
  [116] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 3),
  [118] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 4),
  [120] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 4),
  [122] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_binary_expression, 3),
  [124] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_binary_expression, 3),
  [126] = {.entry = {.count = 1, .reusable = true}}, SHIFT(167),
  [128] = {.entry = {.count = 1, .reusable = true}}, SHIFT(168),
  [130] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [132] = {.entry = {.count = 1, .reusable = false}}, SHIFT(170),
  [134] = {.entry = {.count = 1, .reusable = false}}, SHIFT(171),
  [136] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [138] = {.entry = {.count = 1, .reusable = true}}, SHIFT(37),
  [140] = {.entry = {.count = 1, .reusable = false}}, SHIFT(172),
  [142] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
  [144] = {.entry = {.count = 1, .reusable = false}}, SHIFT(14),
  [146] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [148] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [150] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unary_expression, 2),
  [152] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_unary_expression, 2),
  [154] = {.entry = {.count = 1, .reusable = true}}, SHIFT(97),
  [156] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [158] = {.entry = {.count = 1, .reusable = true}}, SHIFT(409),
  [160] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_array_literal_repeat1, 2),
  [162] = {.entry = {.count = 1, .reusable = true}}, SHIFT(108),
  [164] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [166] = {.entry = {.count = 1, .reusable = true}}, SHIFT(106),
  [168] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [170] = {.entry = {.count = 1, .reusable = true}}, SHIFT(105),
  [172] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [174] = {.entry = {.count = 1, .reusable = true}}, SHIFT(127),
  [176] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument, 3),
  [178] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__positional_argument_list_repeat1, 2),
  [180] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_last_match_arm, 3, .production_id = 4),
  [182] = {.entry = {.count = 1, .reusable = true}}, SHIFT(221),
  [184] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_field_access, 3),
  [186] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [188] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_field_access, 3),
  [190] = {.entry = {.count = 1, .reusable = true}}, SHIFT(293),
  [192] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_if, 7),
  [194] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comptime_if, 7),
  [196] = {.entry = {.count = 1, .reusable = true}}, SHIFT(394),
  [198] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_reg_statement, 10),
  [200] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_reg_statement, 7),
  [202] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_assert_statement, 2),
  [204] = {.entry = {.count = 1, .reusable = false}}, SHIFT(22),
  [206] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_bool_literal, 1),
  [208] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_bool_literal, 1),
  [210] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__simple_base_expression, 1),
  [212] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__scoped_or_raw_ident, 1),
  [214] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__simple_base_expression, 1),
  [216] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__path, 1),
  [218] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_reg_statement, 8),
  [220] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_if, 6),
  [222] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comptime_if, 6),
  [224] = {.entry = {.count = 1, .reusable = true}}, SHIFT(231),
  [226] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_reg_statement, 9),
  [228] = {.entry = {.count = 1, .reusable = true}}, SHIFT(415),
  [230] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_let_binding, 6),
  [232] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [234] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [236] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_set_statement, 4),
  [238] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_let_binding, 4),
  [240] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_if, 8),
  [242] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comptime_if, 8),
  [244] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_else, 3),
  [246] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comptime_else, 3),
  [248] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_pipeline_stage_name, 2),
  [250] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_pipeline_stage_name, 2),
  [252] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__statement, 2),
  [254] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__statement, 2),
  [256] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_else, 4),
  [258] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_comptime_else, 4),
  [260] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument_list, 5),
  [262] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_argument_list, 5),
  [264] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_literal, 6),
  [266] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_literal, 6),
  [268] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument_list, 2),
  [270] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_argument_list, 2),
  [272] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_argument_list, 2),
  [274] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_argument_list, 2),
  [276] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_literal, 5),
  [278] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_literal, 5),
  [280] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_entity_instance, 3),
  [282] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_entity_instance, 3),
  [284] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_match_expression, 3),
  [286] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_match_expression, 3),
  [288] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_stage_reference, 6),
  [290] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_stage_reference, 6),
  [292] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_argument_list, 5),
  [294] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_argument_list, 5),
  [296] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument_list, 3),
  [298] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_argument_list, 3),
  [300] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_pipeline_instance, 6),
  [302] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_pipeline_instance, 6),
  [304] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_literal, 2),
  [306] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_literal, 2),
  [308] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_paren_expression, 3),
  [310] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_paren_expression, 3),
  [312] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_literal, 3),
  [314] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_literal, 3),
  [316] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_literal, 4),
  [318] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_literal, 4),
  [320] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_argument_list, 3),
  [322] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_argument_list, 3),
  [324] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_literal, 5),
  [326] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_literal, 5),
  [328] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument_list, 4),
  [330] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_argument_list, 4),
  [332] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_literal, 7),
  [334] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_literal, 7),
  [336] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_argument_list, 4),
  [338] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_argument_list, 4),
  [340] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__base_expression, 1),
  [342] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__base_expression, 1),
  [344] = {.entry = {.count = 1, .reusable = true}}, SHIFT(379),
  [346] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_if_expression, 5),
  [348] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_if_expression, 5),
  [350] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_match_block, 3),
  [352] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_match_block, 3),
  [354] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_literal, 4),
  [356] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_literal, 4),
  [358] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_match_block, 2),
  [360] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_match_block, 2),
  [362] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_argument_list, 1),
  [364] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_argument_list, 1),
  [366] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_stage_reference, 5),
  [368] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_stage_reference, 5),
  [370] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_stage_reference, 5, .production_id = 3),
  [372] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_stage_reference, 5, .production_id = 3),
  [374] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_function_call, 2),
  [376] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_function_call, 2),
  [378] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_match_block, 4),
  [380] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_match_block, 4),
  [382] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_method_call, 4),
  [384] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_method_call, 4),
  [386] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_method_call, 5),
  [388] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_method_call, 5),
  [390] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [392] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [394] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(405),
  [397] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(444),
  [400] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(438),
  [403] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(378),
  [406] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(436),
  [409] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(270),
  [412] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(281),
  [415] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(429),
  [418] = {.entry = {.count = 1, .reusable = true}}, SHIFT(154),
  [420] = {.entry = {.count = 1, .reusable = true}}, SHIFT(117),
  [422] = {.entry = {.count = 1, .reusable = false}}, SHIFT(215),
  [424] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(154),
  [427] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(75),
  [430] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(215),
  [433] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(421),
  [436] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(43),
  [439] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_match_block_repeat1, 2), SHIFT_REPEAT(43),
  [442] = {.entry = {.count = 1, .reusable = true}}, SHIFT(153),
  [444] = {.entry = {.count = 1, .reusable = true}}, SHIFT(135),
  [446] = {.entry = {.count = 1, .reusable = true}}, SHIFT(219),
  [448] = {.entry = {.count = 1, .reusable = true}}, SHIFT(134),
  [450] = {.entry = {.count = 1, .reusable = false}}, SHIFT(186),
  [452] = {.entry = {.count = 1, .reusable = false}}, SHIFT(179),
  [454] = {.entry = {.count = 1, .reusable = true}}, SHIFT(131),
  [456] = {.entry = {.count = 1, .reusable = false}}, SHIFT(160),
  [458] = {.entry = {.count = 1, .reusable = false}}, SHIFT(289),
  [460] = {.entry = {.count = 1, .reusable = true}}, SHIFT(289),
  [462] = {.entry = {.count = 1, .reusable = true}}, SHIFT(210),
  [464] = {.entry = {.count = 1, .reusable = true}}, SHIFT(202),
  [466] = {.entry = {.count = 1, .reusable = false}}, SHIFT(147),
  [468] = {.entry = {.count = 1, .reusable = true}}, SHIFT(196),
  [470] = {.entry = {.count = 1, .reusable = true}}, SHIFT(213),
  [472] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(174),
  [475] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(351),
  [478] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(39),
  [481] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(387),
  [484] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(432),
  [487] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(445),
  [490] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(13),
  [493] = {.entry = {.count = 1, .reusable = true}}, SHIFT(78),
  [495] = {.entry = {.count = 1, .reusable = true}}, SHIFT(174),
  [497] = {.entry = {.count = 1, .reusable = true}}, SHIFT(351),
  [499] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [501] = {.entry = {.count = 1, .reusable = true}}, SHIFT(387),
  [503] = {.entry = {.count = 1, .reusable = true}}, SHIFT(432),
  [505] = {.entry = {.count = 1, .reusable = true}}, SHIFT(445),
  [507] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [509] = {.entry = {.count = 1, .reusable = true}}, SHIFT(254),
  [511] = {.entry = {.count = 1, .reusable = true}}, SHIFT(226),
  [513] = {.entry = {.count = 1, .reusable = true}}, SHIFT(228),
  [515] = {.entry = {.count = 1, .reusable = true}}, SHIFT(256),
  [517] = {.entry = {.count = 1, .reusable = true}}, SHIFT(197),
  [519] = {.entry = {.count = 1, .reusable = true}}, SHIFT(207),
  [521] = {.entry = {.count = 1, .reusable = true}}, SHIFT(250),
  [523] = {.entry = {.count = 1, .reusable = true}}, SHIFT(92),
  [525] = {.entry = {.count = 1, .reusable = true}}, SHIFT(252),
  [527] = {.entry = {.count = 1, .reusable = true}}, SHIFT(89),
  [529] = {.entry = {.count = 1, .reusable = true}}, SHIFT(263),
  [531] = {.entry = {.count = 1, .reusable = true}}, SHIFT(70),
  [533] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_sub, 1),
  [535] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_sub, 1),
  [537] = {.entry = {.count = 1, .reusable = true}}, SHIFT(262),
  [539] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_mul, 1),
  [541] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_mul, 1),
  [543] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_add, 1),
  [545] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_add, 1),
  [547] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_custom_infix, 3),
  [549] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_custom_infix, 3),
  [551] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_lt, 1),
  [553] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_lt, 1),
  [555] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_gt, 1),
  [557] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_gt, 1),
  [559] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_op_bitwise_and, 1),
  [561] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_op_bitwise_and, 1),
  [563] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_braced_parameter_list, 4),
  [565] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_braced_parameter_list, 5),
  [567] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_type, 1),
  [569] = {.entry = {.count = 1, .reusable = true}}, SHIFT(150),
  [571] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_braced_parameter_list, 2),
  [573] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_braced_parameter_list, 3),
  [575] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unit_definition, 5),
  [577] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unit_definition, 6),
  [579] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_scoped_identifier, 3, .production_id = 2),
  [581] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_scoped_identifier, 3, .production_id = 2),
  [583] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unit_definition, 7),
  [585] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_builtin_type, 1),
  [587] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_scoped_identifier, 2, .production_id = 1),
  [589] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_scoped_identifier, 2, .production_id = 1),
  [591] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_builtin_marker, 1),
  [593] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__item_repeat1, 2), SHIFT_REPEAT(405),
  [596] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__item_repeat1, 2),
  [598] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unit_definition, 4),
  [600] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__generic_list, 3),
  [602] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_type, 5),
  [604] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_comptime_config, 4),
  [606] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl, 5),
  [608] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_body, 3),
  [610] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_type, 2),
  [612] = {.entry = {.count = 1, .reusable = true}}, SHIFT(441),
  [614] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_body, 4),
  [616] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_wire, 2),
  [618] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_type, 2),
  [620] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl, 4),
  [622] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__generic_list, 4),
  [624] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__item, 2),
  [626] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_struct_definition, 5),
  [628] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_struct_definition, 4),
  [630] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_type, 3),
  [632] = {.entry = {.count = 1, .reusable = true}}, SHIFT(158),
  [634] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__pattern, 1),
  [636] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__pattern, 1),
  [638] = {.entry = {.count = 1, .reusable = true}}, SHIFT(277),
  [640] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_mut_wire, 3),
  [642] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__generic_list, 5),
  [644] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_body, 2),
  [646] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_use, 3),
  [648] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_type, 5),
  [650] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_struct_definition, 3),
  [652] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__scoped_or_raw_ident, 1),
  [654] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_definition, 3),
  [656] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_body, 5),
  [658] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_type, 4),
  [660] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_definition, 4),
  [662] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_match_arm, 4),
  [664] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_last_match_arm, 4, .production_id = 4),
  [666] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_match_arm, 4),
  [668] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute, 4),
  [670] = {.entry = {.count = 1, .reusable = true}}, SHIFT(275),
  [672] = {.entry = {.count = 1, .reusable = true}}, SHIFT(442),
  [674] = {.entry = {.count = 1, .reusable = true}}, SHIFT(345),
  [676] = {.entry = {.count = 1, .reusable = false}}, SHIFT(230),
  [678] = {.entry = {.count = 1, .reusable = false}}, SHIFT(318),
  [680] = {.entry = {.count = 1, .reusable = true}}, SHIFT(274),
  [682] = {.entry = {.count = 1, .reusable = true}}, SHIFT(333),
  [684] = {.entry = {.count = 1, .reusable = true}}, SHIFT(245),
  [686] = {.entry = {.count = 1, .reusable = true}}, SHIFT(318),
  [688] = {.entry = {.count = 1, .reusable = true}}, SHIFT(276),
  [690] = {.entry = {.count = 1, .reusable = true}}, SHIFT(437),
  [692] = {.entry = {.count = 1, .reusable = true}}, SHIFT(300),
  [694] = {.entry = {.count = 1, .reusable = true}}, SHIFT(177),
  [696] = {.entry = {.count = 1, .reusable = true}}, SHIFT(178),
  [698] = {.entry = {.count = 1, .reusable = true}}, SHIFT(201),
  [700] = {.entry = {.count = 1, .reusable = true}}, SHIFT(180),
  [702] = {.entry = {.count = 1, .reusable = true}}, SHIFT(316),
  [704] = {.entry = {.count = 1, .reusable = true}}, SHIFT(302),
  [706] = {.entry = {.count = 1, .reusable = true}}, SHIFT(194),
  [708] = {.entry = {.count = 1, .reusable = true}}, SHIFT(341),
  [710] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_impl_repeat1, 2), SHIFT_REPEAT(444),
  [713] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_impl_repeat1, 2), SHIFT_REPEAT(438),
  [716] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_impl_repeat1, 2),
  [718] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_positional_unpack, 2),
  [720] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_positional_unpack, 2),
  [722] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_named_unpack, 2),
  [724] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_named_unpack, 2),
  [726] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_pattern_list, 5),
  [728] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_pattern_list, 5),
  [730] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_pattern_list, 5),
  [732] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_pattern_list, 5),
  [734] = {.entry = {.count = 1, .reusable = true}}, SHIFT(163),
  [736] = {.entry = {.count = 1, .reusable = true}}, SHIFT(188),
  [738] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_pattern, 2),
  [740] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_pattern, 2),
  [742] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_pattern_list, 4),
  [744] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_pattern_list, 4),
  [746] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_pattern_list, 4),
  [748] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_pattern_list, 4),
  [750] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_pattern, 3),
  [752] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_pattern, 3),
  [754] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_pattern, 5),
  [756] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_pattern, 5),
  [758] = {.entry = {.count = 1, .reusable = true}}, SHIFT(295),
  [760] = {.entry = {.count = 1, .reusable = true}}, SHIFT(215),
  [762] = {.entry = {.count = 1, .reusable = true}}, SHIFT(166),
  [764] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_pattern_list, 2),
  [766] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_pattern_list, 2),
  [768] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_pattern_list, 3),
  [770] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__named_pattern_list, 3),
  [772] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_pattern_list, 3),
  [774] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_pattern_list, 3),
  [776] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple_pattern, 4),
  [778] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple_pattern, 4),
  [780] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__positional_pattern_list, 2),
  [782] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__positional_pattern_list, 2),
  [784] = {.entry = {.count = 1, .reusable = true}}, SHIFT(259),
  [786] = {.entry = {.count = 1, .reusable = true}}, SHIFT(239),
  [788] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_member, 1),
  [790] = {.entry = {.count = 1, .reusable = true}}, SHIFT(393),
  [792] = {.entry = {.count = 1, .reusable = true}}, SHIFT(373),
  [794] = {.entry = {.count = 1, .reusable = true}}, SHIFT(375),
  [796] = {.entry = {.count = 1, .reusable = true}}, SHIFT(249),
  [798] = {.entry = {.count = 1, .reusable = true}}, SHIFT(420),
  [800] = {.entry = {.count = 1, .reusable = true}}, SHIFT(225),
  [802] = {.entry = {.count = 1, .reusable = true}}, SHIFT(310),
  [804] = {.entry = {.count = 1, .reusable = true}}, SHIFT(93),
  [806] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_array_literal_repeat1, 2), SHIFT_REPEAT(15),
  [809] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [811] = {.entry = {.count = 1, .reusable = true}}, SHIFT(148),
  [813] = {.entry = {.count = 1, .reusable = true}}, SHIFT(395),
  [815] = {.entry = {.count = 1, .reusable = true}}, SHIFT(253),
  [817] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_parameter_list_repeat1, 2),
  [819] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_parameter_list_repeat1, 2), SHIFT_REPEAT(248),
  [822] = {.entry = {.count = 1, .reusable = true}}, SHIFT(337),
  [824] = {.entry = {.count = 1, .reusable = true}}, SHIFT(95),
  [826] = {.entry = {.count = 1, .reusable = true}}, SHIFT(366),
  [828] = {.entry = {.count = 1, .reusable = true}}, SHIFT(110),
  [830] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__item_repeat1, 2), SHIFT_REPEAT(442),
  [833] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [835] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 6),
  [837] = {.entry = {.count = 1, .reusable = true}}, SHIFT(195),
  [839] = {.entry = {.count = 1, .reusable = true}}, SHIFT(346),
  [841] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 5),
  [843] = {.entry = {.count = 1, .reusable = true}}, SHIFT(243),
  [845] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_tuple_type_repeat1, 2),
  [847] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_decl_statement_repeat1, 2), SHIFT_REPEAT(406),
  [850] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_decl_statement_repeat1, 2),
  [852] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_enum_body_repeat1, 2),
  [854] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_enum_body_repeat1, 2), SHIFT_REPEAT(371),
  [857] = {.entry = {.count = 1, .reusable = true}}, SHIFT(383),
  [859] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_decl_statement, 3),
  [861] = {.entry = {.count = 1, .reusable = true}}, SHIFT(29),
  [863] = {.entry = {.count = 1, .reusable = true}}, SHIFT(218),
  [865] = {.entry = {.count = 1, .reusable = true}}, SHIFT(278),
  [867] = {.entry = {.count = 1, .reusable = true}}, SHIFT(311),
  [869] = {.entry = {.count = 1, .reusable = true}}, SHIFT(279),
  [871] = {.entry = {.count = 1, .reusable = true}}, SHIFT(368),
  [873] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_tuple_type_repeat1, 2), SHIFT_REPEAT(157),
  [876] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [878] = {.entry = {.count = 1, .reusable = true}}, SHIFT(283),
  [880] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 7),
  [882] = {.entry = {.count = 1, .reusable = true}}, SHIFT(364),
  [884] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_decl_statement, 2),
  [886] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter, 1),
  [888] = {.entry = {.count = 1, .reusable = true}}, SHIFT(240),
  [890] = {.entry = {.count = 1, .reusable = true}}, SHIFT(206),
  [892] = {.entry = {.count = 1, .reusable = true}}, SHIFT(130),
  [894] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_tuple_type_repeat1, 2), SHIFT_REPEAT(155),
  [897] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_generic_parameters_repeat1, 2), SHIFT_REPEAT(349),
  [900] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_generic_parameters_repeat1, 2),
  [902] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [904] = {.entry = {.count = 1, .reusable = true}}, SHIFT(181),
  [906] = {.entry = {.count = 1, .reusable = true}}, SHIFT(236),
  [908] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_tuple_pattern_repeat1, 2),
  [910] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_tuple_pattern_repeat1, 2), SHIFT_REPEAT(175),
  [913] = {.entry = {.count = 1, .reusable = true}}, SHIFT(255),
  [915] = {.entry = {.count = 1, .reusable = true}}, SHIFT(165),
  [917] = {.entry = {.count = 1, .reusable = true}}, SHIFT(241),
  [919] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_typed_parameter, 3),
  [921] = {.entry = {.count = 1, .reusable = true}}, SHIFT(149),
  [923] = {.entry = {.count = 1, .reusable = true}}, SHIFT(260),
  [925] = {.entry = {.count = 1, .reusable = true}}, SHIFT(290),
  [927] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 3),
  [929] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_named_pattern_param, 1),
  [931] = {.entry = {.count = 1, .reusable = true}}, SHIFT(176),
  [933] = {.entry = {.count = 1, .reusable = true}}, SHIFT(280),
  [935] = {.entry = {.count = 1, .reusable = true}}, SHIFT(261),
  [937] = {.entry = {.count = 1, .reusable = true}}, SHIFT(140),
  [939] = {.entry = {.count = 1, .reusable = true}}, SHIFT(211),
  [941] = {.entry = {.count = 1, .reusable = true}}, SHIFT(235),
  [943] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 4),
  [945] = {.entry = {.count = 1, .reusable = true}}, SHIFT(143),
  [947] = {.entry = {.count = 1, .reusable = true}}, SHIFT(198),
  [949] = {.entry = {.count = 1, .reusable = true}}, SHIFT(309),
  [951] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_typed_parameter, 4),
  [953] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parameter_list, 2),
  [955] = {.entry = {.count = 1, .reusable = true}}, SHIFT(237),
  [957] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__positional_argument_list_repeat1, 2), SHIFT_REPEAT(18),
  [960] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__named_argument_list_repeat1, 2),
  [962] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__named_argument_list_repeat1, 2), SHIFT_REPEAT(325),
  [965] = {.entry = {.count = 1, .reusable = true}}, SHIFT(426),
  [967] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_pipeline_reg_marker, 1),
  [969] = {.entry = {.count = 1, .reusable = true}}, SHIFT(273),
  [971] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__named_argument, 1),
  [973] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [975] = {.entry = {.count = 1, .reusable = true}}, SHIFT(102),
  [977] = {.entry = {.count = 1, .reusable = true}}, SHIFT(297),
  [979] = {.entry = {.count = 1, .reusable = true}}, SHIFT(294),
  [981] = {.entry = {.count = 1, .reusable = true}}, SHIFT(136),
  [983] = {.entry = {.count = 1, .reusable = true}}, SHIFT(133),
  [985] = {.entry = {.count = 1, .reusable = true}}, SHIFT(191),
  [987] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [989] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__named_pattern_list_repeat1, 2),
  [991] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__named_pattern_list_repeat1, 2), SHIFT_REPEAT(312),
  [994] = {.entry = {.count = 1, .reusable = true}}, SHIFT(132),
  [996] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [998] = {.entry = {.count = 1, .reusable = true}}, SHIFT(159),
  [1000] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_named_pattern_param, 3),
  [1002] = {.entry = {.count = 1, .reusable = true}}, SHIFT(369),
  [1004] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_enum_member, 2),
  [1006] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_generic_parameters, 5),
  [1008] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__generic_param, 2),
  [1010] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_generic_parameters, 3),
  [1012] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_generic_parameters, 4),
  [1014] = {.entry = {.count = 1, .reusable = false}}, SHIFT(424),
  [1016] = {.entry = {.count = 1, .reusable = false}}, SHIFT(286),
  [1018] = {.entry = {.count = 1, .reusable = false}}, SHIFT(430),
  [1020] = {.entry = {.count = 1, .reusable = false}}, SHIFT(68),
  [1022] = {.entry = {.count = 1, .reusable = true}}, SHIFT(212),
  [1024] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_decl_statement, 4),
  [1026] = {.entry = {.count = 1, .reusable = true}}, SHIFT(115),
  [1028] = {.entry = {.count = 1, .reusable = true}}, SHIFT(137),
  [1030] = {.entry = {.count = 1, .reusable = true}}, SHIFT(90),
  [1032] = {.entry = {.count = 1, .reusable = true}}, SHIFT(317),
  [1034] = {.entry = {.count = 1, .reusable = true}}, SHIFT(234),
  [1036] = {.entry = {.count = 1, .reusable = true}}, SHIFT(26),
  [1038] = {.entry = {.count = 1, .reusable = true}}, SHIFT(91),
  [1040] = {.entry = {.count = 1, .reusable = true}}, SHIFT(227),
  [1042] = {.entry = {.count = 1, .reusable = true}}, SHIFT(401),
  [1044] = {.entry = {.count = 1, .reusable = true}}, SHIFT(367),
  [1046] = {.entry = {.count = 1, .reusable = true}}, SHIFT(156),
  [1048] = {.entry = {.count = 1, .reusable = true}}, SHIFT(392),
  [1050] = {.entry = {.count = 1, .reusable = true}}, SHIFT(146),
  [1052] = {.entry = {.count = 1, .reusable = true}}, SHIFT(21),
  [1054] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__pipeline_start, 4),
  [1056] = {.entry = {.count = 1, .reusable = true}}, SHIFT(299),
  [1058] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [1060] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [1062] = {.entry = {.count = 1, .reusable = true}}, SHIFT(100),
  [1064] = {.entry = {.count = 1, .reusable = true}}, SHIFT(184),
  [1066] = {.entry = {.count = 1, .reusable = true}}, SHIFT(122),
  [1068] = {.entry = {.count = 1, .reusable = true}}, SHIFT(427),
  [1070] = {.entry = {.count = 1, .reusable = true}}, SHIFT(164),
  [1072] = {.entry = {.count = 1, .reusable = true}}, SHIFT(398),
  [1074] = {.entry = {.count = 1, .reusable = true}}, SHIFT(434),
  [1076] = {.entry = {.count = 1, .reusable = true}}, SHIFT(119),
  [1078] = {.entry = {.count = 1, .reusable = true}}, SHIFT(120),
  [1080] = {.entry = {.count = 1, .reusable = true}}, SHIFT(222),
  [1082] = {.entry = {.count = 1, .reusable = true}}, SHIFT(287),
  [1084] = {.entry = {.count = 1, .reusable = true}}, SHIFT(402),
  [1086] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_reg_reset, 6),
  [1088] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [1090] = {.entry = {.count = 1, .reusable = true}}, SHIFT(139),
  [1092] = {.entry = {.count = 1, .reusable = true}}, SHIFT(33),
  [1094] = {.entry = {.count = 1, .reusable = true}}, SHIFT(238),
  [1096] = {.entry = {.count = 1, .reusable = true}}, SHIFT(403),
  [1098] = {.entry = {.count = 1, .reusable = true}}, SHIFT(187),
  [1100] = {.entry = {.count = 1, .reusable = true}}, SHIFT(192),
  [1102] = {.entry = {.count = 1, .reusable = true}}, SHIFT(173),
  [1104] = {.entry = {.count = 1, .reusable = true}}, SHIFT(296),
  [1106] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_pipeline_reg_marker, 3),
  [1108] = {.entry = {.count = 1, .reusable = true}}, SHIFT(423),
  [1110] = {.entry = {.count = 1, .reusable = true}}, SHIFT(412),
  [1112] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [1114] = {.entry = {.count = 1, .reusable = true}}, SHIFT(413),
  [1116] = {.entry = {.count = 1, .reusable = true}}, SHIFT(264),
  [1118] = {.entry = {.count = 1, .reusable = true}}, SHIFT(372),
  [1120] = {.entry = {.count = 1, .reusable = true}}, SHIFT(233),
  [1122] = {.entry = {.count = 1, .reusable = true}}, SHIFT(232),
  [1124] = {.entry = {.count = 1, .reusable = true}}, SHIFT(169),
  [1126] = {.entry = {.count = 1, .reusable = true}}, SHIFT(410),
  [1128] = {.entry = {.count = 1, .reusable = true}}, SHIFT(292),
  [1130] = {.entry = {.count = 1, .reusable = true}}, SHIFT(411),
  [1132] = {.entry = {.count = 1, .reusable = true}}, SHIFT(285),
  [1134] = {.entry = {.count = 1, .reusable = true}}, SHIFT(431),
  [1136] = {.entry = {.count = 1, .reusable = true}}, SHIFT(145),
  [1138] = {.entry = {.count = 1, .reusable = true}}, SHIFT(142),
  [1140] = {.entry = {.count = 1, .reusable = true}}, SHIFT(439),
  [1142] = {.entry = {.count = 1, .reusable = true}}, SHIFT(265),
  [1144] = {.entry = {.count = 1, .reusable = true}}, SHIFT(282),
  [1146] = {.entry = {.count = 1, .reusable = true}}, SHIFT(223),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_spade(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
