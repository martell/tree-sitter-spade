// Borrowed from
// https://github.com/Quaqqer/tree-sitter-sylt/blob/52959acb374576f7e738257e1cf849878fd22e33/grammar.js

function sep1(rule, separator, trailing) {
  return seq(
    rule,
    repeat(seq(separator, rule)),
    ...(trailing ? [optional(separator)] : [])
  );
}

function sep(rule, separator, trailing) {
  return optional(sep1(rule, separator, trailing));
}

module.exports = grammar({
    name: "spade",

    extras: $ => [/\s/, $.line_comment],

    rules: {
        source_file: $ => repeat($._item),

        attribute: $ => seq('#', '[', $.identifier, ']'),

        _item: $ => seq(
            optional(repeat($.attribute)),
            choice(
                $.unit_definition,
                $.struct_definition,
                $.enum_definition,
                $.use,
                $.impl,
                $.comptime_config,
            )
        ),

        unit_definition: $ => seq(
            choice(
                'fn',
                'entity',
                $._pipeline_start
            ),
            $.identifier,
            optional($.generic_parameters),
            $.parameter_list,
            optional(
                seq(
                    '->',
                    $.type,
                )
            ),
            $._body_or_builtin
        ),

        _fn_start: $ => 'fn',
        _entity_start: $ => 'entity',
        _pipeline_start: $ => seq(
            'pipeline',
            '(',
            $.int_literal,
            ')',
        ),

        struct_definition: $ => seq(
            'struct',
            optional('port'),
            $.identifier,
            optional($.generic_parameters),
            $.braced_parameter_list,
        ),


        enum_definition: $ => seq(
            'enum',
            $.identifier,
            optional($.generic_parameters),
            $.enum_body,
        ),

        impl: $ => seq(
            'impl',
            $._scoped_or_raw_ident,
            '{',
            repeat($.unit_definition),
            '}'
        ),

        enum_body: $ => seq('{', sep($.enum_member, ',', true), '}'),

        enum_member: $ => seq($.identifier, optional($.braced_parameter_list)),

        use: $ => seq(
            'use',
            $.scoped_identifier,
            ';'
        ),

        comptime_config: $ => seq('$config', $.identifier, '=', $.int_literal),

        _body_or_builtin: $ => choice(
            $.builtin_marker,
            $.block
        ),

        block: $ => seq (
            '{',
            repeat($._statement),
            $._expression,
            '}'
        ),

        _statement: $ => choice (
            seq($.let_binding, ';'),
            seq($.set_statement, ';'),
            seq($.reg_statement, ';'),
            seq($.pipeline_reg_marker, ';'),
            seq($.assert_statement, ';'),
            seq($.decl_statement, ';'),
            seq($.pipeline_stage_name),
            seq($.comptime_if),
        ),

        let_binding: $ => seq (
            'let',
            $._pattern,
            optional(seq(':', $.type)),
            '=',
            $._expression,
        ),

        reg_statement: $ => seq (
            'reg',
            '(',
            $.identifier,
            ')',
            $._pattern,
            optional(seq(':', $.type)),
            optional($.reg_reset),
            '=',
            $._expression,
        ),

        set_statement: $ => seq (
            'set',
            $._expression,
            '=',
            $._expression
        ),

        decl_statement: $ => seq('decl', sep1($.identifier, ',', true)),

        pipeline_reg_marker: $ => seq('reg', optional(seq('*', $.int_literal))),
        pipeline_stage_name: $ => seq('\'', $.identifier),

        _comptime_operator: $ => choice($.op_lt, $.op_gt, $.op_le, $.op_ge, $.op_equals),
        comptime_if: $ => seq(
            '$if',
            $.identifier,
            $._comptime_operator,
            $.int_literal,
            '{',
            repeat(
                $._statement,
            ),
            '}',
            optional($.comptime_else)
        ),
        comptime_else: $ => seq(
            '$else',
            '{',
            repeat($._statement),
            '}'
        ),

        assert_statement: $ => seq('assert', $._expression),

        reg_reset: $ => seq (
            'reset',
            '(',
            $.identifier,
            ':',
            $._expression,
            ')',
        ),

        _expression: $ => choice (
            $._base_expression,
            $.binary_expression,
            $.unary_expression,
        ),

        op_add: $ => '+',
        op_sub: $ => '-',
        op_mul: $ => '*',
        op_equals: $ => '==',
        op_lt: $ => '<',
        op_gt: $ => '>',
        op_le: $ => '<=',
        op_ge: $ => '>=',
        op_lshift: $ => '<<',
        op_rshift: $ => '>>',
        op_bitwise_and: $ => '&',
        op_bitwise_xor: $ => '^',
        op_bitwise_or: $ => '|',
        op_logical_and: $ => '&&',
        op_logical_or: $ => '||',

        // Combined parsers for operators which have the same precedence
        _op_add_like: $ => choice($.op_add, $.op_sub),
        _op_relational: $ => choice($.op_lt, $.op_gt, $.op_le, $.op_ge),
        _op_shifty: $ => choice($.op_lshift, $.op_rshift),
        _op_shifty: $ => choice($.op_lshift, $.op_rshift),

        op_custom_infix: $ => seq('`', $.identifier, '`'),

        binary_expression: $ => choice (
            prec.left(-1, seq($._expression, $.op_mul, $._expression)),
            prec.left(-2, seq($._expression, $._op_add_like, $._expression)),
            prec.left(-3, seq($._expression, $._op_shifty, $._expression)),
            prec.left(-4, seq($._expression, $.op_equals, $._expression)),
            prec.left(-5, seq($._expression, $._op_relational, $._expression)),
            prec.left(-6, seq($._expression, $.op_bitwise_and, $._expression)),
            prec.left(-7, seq($._expression, $.op_bitwise_xor, $._expression)),
            prec.left(-8, seq($._expression, $.op_bitwise_or, $._expression)),
            prec.left(-9, seq($._expression, $.op_logical_and, $._expression)),
            prec.left(-10, seq($._expression, $.op_logical_or, $._expression)),
            prec.left(-11, seq($._expression, $.op_custom_infix, $._expression)),
        ),

        unary_expression: $ => prec(
            0,
            choice (
                seq($.op_sub, $._expression),
                seq('!', $._expression),
                seq('*', $._expression)
            )
        ),

        if_expression: $ => seq(
            'if',
            $._expression,
            $.block,
            'else',
            choice($.if_expression, $.block)
        ),

        match_expression: $ => seq(
            'match',
            $._expression,
            $.match_block
        ),

        match_block: $ => seq(
            '{',
            optional(seq(
              repeat($.match_arm),
              alias($.last_match_arm, $.match_arm)
            )),
            '}'
        ),

        match_arm: $ => seq(
            $._pattern,
            '=>',
            choice(
                seq($._expression, ','),
            )
        ),

        last_match_arm: $ => seq(
            field('pattern', $._pattern),
            '=>',
            field('value', $._expression),
            optional(',')
        ),

        _base_expression: $ => choice(
            $.block,
            $._simple_base_expression,
            $.field_access,
            $.method_call
        ),


        _simple_base_expression: $ => choice(
            $.bool_literal,
            $.int_literal,
            $.array_literal,
            $.tuple_literal,
            $.identifier,
            $.match_expression,
            $.function_call,
            $.entity_instance,
            $.pipeline_instance,
            $.stage_reference,
            $.if_expression,
            $.paren_expression,
            $.self
        ),

        field_access: $ => seq($._simple_base_expression, '.', $.identifier),
        method_call: $ => seq(
            $._simple_base_expression,
            '.',
            optional('inst'),
            $.identifier,
            $.argument_list
        ),

        array_literal: $ => seq('[', sep($._expression, ',', true), ']'),
        tuple_literal: $ => seq('(', $._expression, ',', sep($._expression, ',', true), ')'),

        paren_expression: $ => seq('(', $._expression, ')'),

        function_call: $ => seq($._scoped_or_raw_ident, $.argument_list),
        entity_instance: $ => seq('inst', $._scoped_or_raw_ident, $.argument_list),
        pipeline_instance: $ => seq('inst', '(', $.int_literal ,')', $._scoped_or_raw_ident, $.argument_list),

        stage_reference: $ => seq(
            'stage',
            '(',
            choice(field('stage', $.identifier), seq(optional(choice('-', '+')), $.int_literal)),
            ').',
            $.identifier
        ),

        bool_literal: $ => choice(
            'true',
            'false'
        ),

        builtin_marker: $ => choice('__builtin__'),

        argument_list: $ => choice(
            $._positional_argument_list,
            $._named_argument_list
        ),

        _named_argument_list: $ => seq(
            '$(',
            sep($._named_argument, ',', true),
            ')'
        ),

        _named_argument: $ => choice (
            $.parameter,
            seq($.parameter, ':', $._expression)
        ),

        _positional_argument_list: $ => seq(
            '(',
            sep(seq($._expression), ',', true),
            ')'
        ),

        parameter_list: $ => seq(
            '(',
            optional(seq($.self, optional(','))),
            sep($.typed_parameter, ',', true),
            ')',
        ),

        braced_parameter_list: $ => seq(
            '{',
            sep($.typed_parameter, ',', true),
            '}',
        ),

        typed_parameter: $ => seq(
            optional(repeat($.attribute)),
            $.parameter,
            ':',
            $.type
        ),

        parameter: $ => $.identifier,
        self: $ => 'self',

        _pattern: $ => choice(
            $._scoped_or_raw_ident,
            $.int_literal,
            $.bool_literal,
            $.tuple_pattern,
            $.named_unpack,
            $.positional_unpack
        ),

        tuple_pattern: $ => seq('(', sep($._pattern, ',', true) ,')'),
        named_unpack: $ => seq($._scoped_or_raw_ident, $._named_pattern_list),
        positional_unpack: $ => seq($._scoped_or_raw_ident, $._positional_pattern_list),

        _named_pattern_list: $ => seq('$(', sep($.named_pattern_param, ',', true), ')'),
        _positional_pattern_list: $ => seq('(', sep($._pattern, ',', true), ')'),

        named_pattern_param: $ => choice(
            $.parameter,
            seq($.parameter, ':', $._pattern)
        ),

        type: $ => choice(
            // Base type with generics
            seq(
                $._base_type,
                optional($._generic_list),
            ),
            $.tuple_type,
            $.array_type,
            $.mut_wire,
            $.wire
        ),

        tuple_type: $ => seq('(', sep($.type, ',', true), ')'),
        array_type: $ => seq('[', $.type, ';', $.type, ']'),
        mut_wire: $ => seq('&', 'mut', $.type),
        wire: $ => seq('&', $.type),

        _generic_list: $ => seq (
            '<',
            sep1($.type, ',', true),
            '>'
        ),

        generic_parameters: $ => seq (
            '<',
            sep1($._generic_param, ',', true),
            '>'
        ),

        _generic_param: $ => seq(
            optional('#'),
            $.identifier
        ),

        _base_type: $ => choice (
            $.builtin_type,
            $.identifier,
            $.int_literal
        ),

        builtin_type: $ => choice (
            'int',
            'bool',
            'clock'
        ),

        // TODO: Same regex as spade
        identifier: $ => /[\p{XID_Start}_]\p{XID_Continue}*/u,

        scoped_identifier: $ => seq(
          field('path', optional(choice(
            $._path,
          ))),
          '::',
          field('name', $.identifier)
        ),

        _path: $ => choice($.scoped_identifier, $.identifier),
        _scoped_or_raw_ident: $ => choice($.scoped_identifier, $.identifier),

        int_literal: $ => choice(
            /[0-9][0-9_]*/,
            /0x[0-9A-Fa-f][0-9_A-Fa-f]*/,
            /0b[0-1][0-1_]*/
        ),

        comment: $ => choice(
          $.line_comment,
        ),

        line_comment: $ => token(seq(
          '//', /.*/
        )),
    }
})
