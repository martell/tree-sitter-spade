======
empty match block
======

fn a() {
  match x {
    
  }
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (match_expression
        (identifier)
        (match_block)))))


======
match block
======

fn a() {
  match x {
    a => b,
    0 => d
  }
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (match_expression
        (identifier)
        (match_block
          (match_arm
            (identifier)
            (identifier))
          (match_arm
            (int_literal)
            (identifier)))))))


======
match with block
======

fn a() {
  match x {
    a => {b},
    0 => d
  }
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (match_expression
        (identifier)
        (match_block
          (match_arm
            (identifier)
            (block (identifier)))
          (match_arm
            (int_literal)
            (identifier)))))))
