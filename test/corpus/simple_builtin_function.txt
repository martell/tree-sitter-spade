================
Simple builtin function
================

fn x() -> bool __builtin__

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (type (builtin_type))
    (builtin_marker)))

