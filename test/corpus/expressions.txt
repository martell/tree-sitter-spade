===
Array literals
===

fn a() {
  [1, 2]
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (array_literal
        (int_literal)
        (int_literal)))))

===
Tuple literals
===

fn a() {
  (1, 2)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (tuple_literal
        (int_literal)
        (int_literal)))))

===
Function calls
===

fn a() {
  test(a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (function_call
        (identifier)
        (argument_list (identifier))))))



===
Function with named arguments
===

fn a() {
  test$(a: a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (function_call
        (identifier)
        (argument_list (parameter (identifier)) (identifier))))))


===
Function with named shorthand arguments
===

fn a() {
  test$(a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (function_call
        (identifier)
        (argument_list (parameter (identifier)))))))


===
Entity calls
===

fn a() {
  inst test(a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (entity_instance
        (identifier)
        (argument_list (identifier))))))



===
Entity with named arguments
===

fn a() {
  inst test$(a: a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (entity_instance
        (identifier)
        (argument_list (parameter (identifier)) (identifier))))))


===
Pipeline instance instance
===

fn a() {
  inst(10) test$(a: a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (pipeline_instance
        (int_literal)
        (identifier)
        (argument_list (parameter (identifier)) (identifier))))))


===
Parenthesized expression
===

fn a() {
  (a)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (paren_expression (identifier)))))


===
Dereference operator
===

fn a() {
  *a
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (unary_expression (identifier)))))


===
Field access
===

fn a() {
  a.b
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (field_access (identifier) (identifier)))))


===
Method call (positional)
===

fn a() {
  a.b(x)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (method_call
        (identifier)
        (identifier)
        (argument_list (identifier))))))

===
Method call (positional)
===

fn a() {
  a.b$(x: y)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (method_call
        (identifier)
        (identifier)
        (argument_list
          (parameter (identifier))
          (identifier))))))

===
Entity method call (positional)
===

fn a() {
  a.inst b(x)
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (method_call
        (identifier)
        (identifier)
        (argument_list (identifier))))))

===
Self
===

fn a() {
  self
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (self))))
