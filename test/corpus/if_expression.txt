===
If expression
===

fn a() {
  if x {
    true
  }
  else {
    false
  }
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (if_expression
        (identifier)
        (block (bool_literal))
        (block (bool_literal))))))


===
If with else if
===

fn a() {
  if x {
    true
  }
  else if x {
    true
  }
  else {
    false
  }
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (if_expression
        (identifier)
        (block (bool_literal))
        (if_expression
          (identifier)
          (block (bool_literal))
          (block (bool_literal)))))))
