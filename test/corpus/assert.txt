===
assert
===

fn a() {
  assert x;
  x
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (assert_statement (identifier))
      (identifier))))

