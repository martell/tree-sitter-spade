================
Decl statement single
================

fn x() {
  decl x;
  true
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (decl_statement
        (identifier))
      (bool_literal))))

================
Decl statement multi
================

fn x() {
  decl x,y;
  true
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (decl_statement
        (identifier)
        (identifier))
      (bool_literal))))

===
Set statement
===


fn a() {
  set x = y;
  a
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (set_statement
        (identifier)
        (identifier))
    (identifier))))
