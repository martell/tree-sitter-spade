================
fn bodies
================

fn x() {
  true
}

fn x() {
  0
}

fn x() {
  let x = 0;
  0
}

fn x() {
  let x: bool = 0;
  0
}

---

(source_file
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (bool_literal)))
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (int_literal)))
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (let_binding (identifier) (int_literal))
      (int_literal)))
  (unit_definition
    (identifier)
    (parameter_list)
    (block
      (let_binding (identifier) (type (builtin_type)) (int_literal))
      (int_literal))))
